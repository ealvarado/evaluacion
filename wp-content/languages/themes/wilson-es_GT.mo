��    J      l  e   �      P  
   Q     \     s  #   �      �  (   �     �  
     	          1   *     \     j     r     �     �     �     �     �     �     �     �               #     4     A  #   F     j  	   o  M   y     �     �     �  	   �  �   �     �	     �	     �	     �	  #   �	     
      )
  
   J
  $   U
  (   z
     �
     �
  	   �
     �
     �
     �
     �
  	   
               #     /     6     :     @     L  
   R     ]  M   `     �  E   �  H   	  I   R  6   �     �     �     �    �     �            0   8  -   i  '   �     �     �     �     �  9         :     H     P     e     ~     �     �     �     �     �     �  
   �      �          1     ?  %   E     k  	   r  Q   |     �     �     �       �        �     �     �     �  0     %   8  *   ^     �  &   �  )   �  !   �  %     	   5     ?  	   P  	   Z     d     |     �     �     �     �     �  
   �     �     �     �     �  f   �     X  b   u  S   �  Y   ,  <   �     �     �     �     !            +          E      *   '      ,       ;   A       J   9   5       <                                     	   -       H   )   G   I            0       >   F          .             /   ?                  3   7      B   1   
      :   &   6                 8       "   #               4       D   @   =   2   (          C       $                        %           % Comments %s Comment %s Comments %s Pingback %s Pingbacks &laquo; Older<span> Comments</span> &laquo; Older<span> posts</span> &laquo; Previous<span> attachment</span> (Post author) 0 Comments 1 Comment Accent Color Allows you to customize theme options for Wilson. Anders Norén Archive Archive template Archives by Categories Archives by Day Archives by Month Archives by Tags Archives by Year Author Awaiting moderation Categories: Category Comments are closed. Continue reading Contributors Date Displays your latest Flickr photos. Edit Error 404 Flickr ID (use <a target="_blank" href="http://www.idgettr.com">idGettr</a>): Flickr Widget Footer A Footer B Hide menu It seems like you have tried to open a page that doesn't exist. It could have been deleted, moved, or it never existed at all. You are welcome to search for what you are looking for with the form below. Last 30 Posts Logo Month More on Flickr &raquo; Newer<span> Comments</span> &raquo; Newer<span> posts</span> Newer<span> posts</span> &raquo; Next post: Next<span> attachment</span> &raquo; No results. Try again, would you kindly? Number of images to display Older<span> posts</span> Pingback: Previous post: Reply Search Search form Show menu Sidebar Sticky Sticky post Submit Tag Tags: Theme by %s Title To the top Up Upload a logo to replace the default site name and description in the sidebar View all posts in %s We couldn't find any posts that matched your query. Please try again. Widgets in this area will be displayed in the left column in the footer. Widgets in this area will be displayed in the right column in the footer. Widgets in this area will be displayed in the sidebar. Wilson Wilson Options Year PO-Revision-Date: 2016-12-07 11:35:48+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.4.0-alpha
Language: es_GT
Project-Id-Version: Themes - Wilson
 % Comentarios %s Comentario %s Comentarios %s Pingback %s Pingbacks &laquo; <span> Comentarios </span> más antiguos &laquo; <span> Mensajes </span> más antiguos &laquo; <span> Adjunto </span> anterior (Autor del mensaje) 0 Comentarios 1 Comentario Acentuar el color Le permite personalizar las opciones de tema para Wilson. Anders Norén Archivo Archivo de plantilla Archivos por categorías Archivos por día Archivos por mes Archivos por etiquetas Archivos por año Autor Esperando moderación Categorías: Categoría Los comentarios están cerrados. Continuar leyendo Colaboradores Fecha Mostrar sus últimas fotos de Flickr. Editar Error 404 ID de Flickr (usar <a target="_blank" href="http://www.idgettr.com">idGettr</a>): Widget para Flickr Pié de página A Pié de página B Esconder menú Parece que has intentado abrir una página que no existe. Podría haber sido eliminado, movido, o nunca existió en absoluto. Le invitamos a buscar lo que busca con el siguiente formulario. Últimos 30 mensajes Logo Mes Más en Flickr &raquo; <span>Comentarios </span> más recientes &raquo; <span> Mensajes</span> más recientes <span> Mensajes</span> más nuevos &raquo; Siguiente mensaje: Siguiente<span> adjunto</span> &raquo; Sin resultados. ¿Quiere tratar de nuevo? Número de imágenes para mostrar <span> Mensajes </span> más antiguos Pingback: Mensaje anterior Responder Búsqueda Formulario de búsqueda Mostrar menú Barra lateral Pegado Mensaje pegado Enviar Etiqueta Etiquetas: Tema por %s Título Hacia arriba Arriba Subir un logo para reemplazar el nombre del sitio predeterminado y la descripción en la barra lateral Ver todos los mensajes en %s No hemos podido encontrar ningún mensaje que coincida con su consulta. Por favor, trate de nuevo. Los widgets en esta área se mostrarán en la columna izquierda del pie de página. Los widgets de esta área se mostrarán en la columna de la derecha en el pie de página. Los widgets en esta área se mostrarán en la barra lateral. Wilson Opciones de Wilson Año 