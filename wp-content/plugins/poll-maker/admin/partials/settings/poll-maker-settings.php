<?php
$actions = $this->settings_obj;

if (isset($_REQUEST['ays_submit'])) {
	$actions->store_data($_REQUEST);
}
if (isset($_GET['ays_poll_tab'])) {
	$ays_poll_tab = sanitize_text_field($_GET['ays_poll_tab']);
} else {
	$ays_poll_tab = 'tab1';
}
$db_data = $actions->get_db_data();

global $wp_roles;
$ays_users_roles = $wp_roles->role_names;

$mailchimp_res      = ($actions->ays_get_setting('mailchimp') === false) ? json_encode(array()) : $actions->ays_get_setting('mailchimp');
$mailchimp          = json_decode($mailchimp_res, true);
$mailchimp_username = isset($mailchimp['username']) ? $mailchimp['username'] : '';
$mailchimp_api_key  = isset($mailchimp['apiKey']) ? $mailchimp['apiKey'] : '';

$option_res         = ($actions->ays_get_setting('options') === false) ? json_encode(array()) : $actions->ays_get_setting('options');
$options            = json_decode($option_res, true);

$answers_sound = isset($options['answers_sound']) ? $options['answers_sound'] : '';

?>
<div class="wrap" style="position:relative;">
    <div class="container-fluid">
        <form method="post">
            <input type="hidden" name="ays_poll_tab" value="<?php echo htmlentities($ays_poll_tab); ?>">
            <h1 class="wp-heading-inline">
				<?php
				echo __('Settings', $this->plugin_name);
				?>
            </h1>
			<?php
			if (isset($_REQUEST['status'])) {
				$actions->poll_settings_notices($_REQUEST['status']);
			}
			?>
            <hr/>
            <div class="ays-settings-wrapper">
                <div>
                    <div class="nav-tab-wrapper" style="position:sticky; top:35px;">
                        <a href="#tab1" data-tab="tab1"
                           class="nav-tab <?php echo ($ays_poll_tab == 'tab1') ? 'nav-tab-active' : ''; ?>">
							<?php echo __("General", $this->plugin_name); ?>
                        </a>
                        <a href="#tab2" data-tab="tab2"
                           class="nav-tab <?php echo ($ays_poll_tab == 'tab2') ? 'nav-tab-active' : ''; ?>">
							<?php echo __("Integrations", $this->plugin_name); ?>
                        </a>
                        <a href="#tab3" data-tab="tab3"
                           class="nav-tab <?php echo ($ays_poll_tab == 'tab3') ? 'nav-tab-active' : ''; ?>">
                            <?php echo __("Shortcodes", $this->plugin_name); ?>
                        </a>
                    </div>
                </div>
                <div class="ays-poll-tabs-wrapper">
                    <div id="tab1"
                         class="ays-poll-tab-content <?php echo ($ays_poll_tab == 'tab1') ? 'ays-poll-tab-content-active' : ''; ?>">
                        <p class="ays-subtitle"><?php echo __('General Settings', $this->plugin_name) ?></p>
                        <hr/>
                        <fieldset>
                            <legend>
                                <strong style="font-size:30px;"><i class="ays_fa ays_poll_fa_question_circle"></i></strong>
                                <h5><?php echo __('Default parameters for Poll',$this->plugin_name)?></h5>
                            </legend>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="ays_disable_ip_storing">
                                        <?= __('Disable IP Storing', $this->plugin_name); ?>
                                        <a class="ays_help" data-toggle="tooltip"
                                           data-placement="top"
                                           title="<?= __("After enabling this option, IP address of the users will not be stored in database.  Note: If this option is enabled, then the Limit users to rate only once option will not work.", $this->plugin_name); ?>">
                                            <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                                        </a>
                                    </label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="checkbox"
                                           name="ays_disable_ip_storing"
                                           id="ays_disable_ip_storing"
                                           value="on" <?php echo (isset($options['disable_ip_storing']) && $options['disable_ip_storing'] == 'on') ? 'checked' : ''; ?>
                                    >
                                </div>
                            </div>
                        </fieldset>
                        <hr/>
                        <fieldset>
                            <legend>
                                <strong style="font-size:30px;"><i class="ays_poll_fa ays_poll_fa_music"></i></strong>
                                <h5><?php echo __('Poll answers sound',$this->plugin_name)?></h5>
                            </legend>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="ays_questions_default_type">
                                        <?php echo __( "Sound for answers", $this->plugin_name ); ?>
                                        <a class="ays_help" data-toggle="tooltip" title="<?php echo __('This option will work with Enable answers sound option.',$this->plugin_name); ?>">
                                            <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                                        </a>
                                    </label>
                                </div>
                                <div class="col-sm-8">
                                    <div class="ays-bg-music-container">
                                        <a class="add-poll-bg-music" href="javascript:void(0);"><?php echo __("Select sound", $this->plugin_name); ?></a>
                                        <audio controls src="<?php echo $answers_sound; ?>"></audio>
                                        <input type="hidden" name="ays_poll_answers_sound" class="ays_poll_bg_music" value="<?php echo $answers_sound; ?>">
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <hr/>
                                                  
                        <fieldset>
                            <legend>
                                <strong style="font-size:30px;"><i class="ays_poll_fa ays_poll_fa_globe"></i></strong>
                                <h5><?php echo __('Who will have permission to Poll menu',$this->plugin_name)?></h5>
                            </legend>
                            <div class="col-sm-12" style="padding:20px;">
                                <div class="pro_features" style="">
                                    <div style="margin-right:20px;">
                                        <p style="font-size:20px;">
                                            <?php echo __("This feature is available only in ", $this->plugin_name); ?>
                                            <a href="https://ays-pro.com/wordpress/poll-maker/" target="_blank" title="Developer package"><?php echo __("Developer package!!!", $this->plugin_name); ?></a>
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-4">
                                        <label for="ays_user_roles">
                                            <?php echo __( "Select user role", $this->plugin_name ); ?>
                                            <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Ability to manage Poll Maker plugin only for selected user roles.',$this->plugin_name)?>">
                                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                                            </a>
                                        </label>
                                    </div>
                                    <div class="col-sm-8">
                                        <select disabled name="ays_user_roles_poll[]" id="ays_user_roles_poll" multiple>
                                            <option selected value="admin">Administrator</option>
                                            <option selected value="author">Author</option>
                                        </select>
                                    </div>
                                    <blockquote>
                                        <?php echo __( "Ability to manage Poll Maker plugin only for selected user roles.", $this->plugin_name ); ?>
                                    </blockquote>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div id="tab2"
                         class="ays-poll-tab-content <?php echo ($ays_poll_tab == 'tab2') ? 'ays-poll-tab-content-active' : ''; ?>">
                        <p class="ays-subtitle"><?php echo __('Integrations', $this->plugin_name) ?></p>
                        <hr/>
                        <fieldset>
                            <legend>
                                <img class="ays_integration_logo" src="<?php echo POLL_MAKER_AYS_ADMIN_URL; ?>/images/integrations/mailchimp_logo.png" alt="">
                                <h5><?php echo __('MailChimp',$this->plugin_name)?></h5>
                            </legend>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <div class="form-group row" aria-describedby="aaa">
                                        <div class="col-sm-3">
                                            <label for="ays_mailchimp_username">
                                                <?php echo __('MailChimp Username',$this->plugin_name)?>
                                            </label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="text"
                                                   class="ays-text-input"
                                                   id="ays_mailchimp_username"
                                                   name="ays_mailchimp_username"
                                                   value="<?php echo $mailchimp_username; ?>"
                                            />
                                        </div>
                                    </div>
                                    <hr/>
                                    <div class="form-group row" aria-describedby="aaa">
                                        <div class="col-sm-3">
                                            <label for="ays_mailchimp_api_key">
                                                <?php echo __('MailChimp API Key',$this->plugin_name)?>
                                            </label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="text"
                                                   class="ays-text-input"
                                                   id="ays_mailchimp_api_key"
                                                   name="ays_mailchimp_api_key"
                                                   value="<?php echo $mailchimp_api_key; ?>"
                                            />
                                        </div>
                                    </div>
                                    <blockquote>
                                        <?php echo sprintf( __( "You can get your API key from your ", $this->plugin_name ) . "<a href='%s' target='_blank'> %s.</a>", "https://us20.admin.mailchimp.com/account/api/", __( "Account Extras menu", $this->plugin_name ) ); ?>
                                    </blockquote>
                                </div>
                            </div>
                        </fieldset>
                        <hr/>
                        <fieldset>
                            <legend>
                                <img class="ays_integration_logo" src="<?php echo POLL_MAKER_AYS_ADMIN_URL; ?>/images/integrations/campaignmonitor_logo.png" alt="">
                                <h5><?php echo __('Campaign Monitor',$this->plugin_name)?></h5>
                            </legend>
                            <div class="form-group row" style="padding:0;margin:0;">
                                <div class="col-sm-12" style="padding:20px;">
                                    <div class="pro_features" style="justify-content:flex-end;">
                                        <div style="margin-right:20px;">
                                            <p style="font-size:20px;">
                                                <?php echo __("This feature is available only in ", $this->plugin_name); ?>
                                                <a href="https://ays-pro.com/wordpress/poll-maker" target="_blank" title="DEVELOPER feature"><?php echo __("DEVELOPER packadge!!!", $this->plugin_name); ?></a>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <div class="form-group row" aria-describedby="aaa">
                                                <div class="col-sm-3">
                                                    <label for="ays_monitor_client">
                                                        Campaign Monitor <?= __('Client ID', $this->plugin_name) ?>
                                                    </label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <input type="text"
                                                           class="ays-text-input"
                                                           id="ays_monitor_client"
                                                           name="ays_monitor_client"
                                                           value=""
                                                    >
                                                </div>
                                            </div>
                                            <hr/>
                                            <div class="form-group row" aria-describedby="aaa">
                                                <div class="col-sm-3">
                                                    <label for="ays_monitor_api_key">
                                                        Campaign Monitor <?= __('API Key', $this->plugin_name) ?>
                                                    </label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <input type="text"
                                                           class="ays-text-input"
                                                           id="ays_monitor_api_key"
                                                           name="ays_monitor_api_key"
                                                           value=""
                                                    >
                                                </div>
                                            </div>
                                            <blockquote>
                                                <?= __("You can get your API key and Client ID from your Account Settings page", $this->plugin_name); ?>
                                            </blockquote>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <hr/>
                        <fieldset>
                            <legend>
                                <img class="ays_integration_logo" src="<?php echo POLL_MAKER_AYS_ADMIN_URL; ?>/images/integrations/zapier_logo.png" alt="">
                                <h5><?php echo __('Zapier',$this->plugin_name)?></h5>
                            </legend>
                            <div class="form-group row" style="padding:0;margin:0;">
                                <div class="col-sm-12" style="padding:20px;">
                                    <div class="pro_features" style="justify-content:flex-end;">
                                        <div style="margin-right:20px;">
                                            <p style="font-size:20px;">
                                                <?php echo __("This feature is available only in ", $this->plugin_name); ?>
                                                <a href="https://ays-pro.com/wordpress/poll-maker" target="_blank" title="DEVELOPER feature"><?php echo __("DEVELOPER packadge!!!", $this->plugin_name); ?></a>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <div class="form-group row" aria-describedby="aaa">
                                                <div class="col-sm-3">
                                                    <label for="ays_zapier_hook">
                                                        <?= __('Zapier Webhook URL', $this->plugin_name) ?>
                                                    </label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <input type="text"
                                                           class="ays-text-input"
                                                           id="ays_zapier_hook"
                                                           name="ays_zapier_hook"
                                                           value=""
                                                    >
                                                </div>
                                            </div>
                                            <blockquote>
                                                <?php echo sprintf(__("If you do not have any ZAP created, go " . "<a href='%s' target='_blank'>%s</a>" . ". Remember to choose Webhooks by Zapier as Trigger App.", $this->plugin_name), "https://zapier.com/app/editor/", "here"); ?>
                                            </blockquote>
                                            <blockquote>
                                                <?= __("We will send you all data from poll information form with \"AysPoll\" key by POST method", $this->plugin_name); ?>
                                            </blockquote>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <hr/>
                        <fieldset>
                            <legend>
                                <img class="ays_integration_logo" src="<?php echo POLL_MAKER_AYS_ADMIN_URL; ?>/images/integrations/activecampaign_logo.png" alt="">
                                <h5><?php echo __('ActiveCampaign',$this->plugin_name)?></h5>
                            </legend>
                            <div class="form-group row" style="padding:0;margin:0;">
                                <div class="col-sm-12" style="padding:20px;">
                                    <div class="pro_features" style="justify-content:flex-end;">
                                        <div style="margin-right:20px;">
                                            <p style="font-size:20px;">
                                                <?php echo __("This feature is available only in ", $this->plugin_name); ?>
                                                <a href="https://ays-pro.com/wordpress/poll-maker" target="_blank" title="DEVELOPER feature"><?php echo __("DEVELOPER packadge!!!", $this->plugin_name); ?></a>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <div class="form-group row" aria-describedby="aaa">
                                                <div class="col-sm-3">
                                                    <label for="ays_active_camp_url">
                                                        <?= __('API Access URL', $this->plugin_name) ?>
                                                    </label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <input type="text"
                                                           class="ays-text-input"
                                                           id="ays_active_camp_url"
                                                           name="ays_active_camp_url"
                                                           value=""
                                                    >
                                                </div>
                                            </div>
                                            <hr/>
                                            <div class="form-group row" aria-describedby="aaa">
                                                <div class="col-sm-3">
                                                    <label for="ays_active_camp_api_key">
                                                        <?= __('API Access Key', $this->plugin_name) ?>
                                                    </label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <input type="text"
                                                           class="ays-text-input"
                                                           id="ays_active_camp_api_key"
                                                           name="ays_active_camp_api_key"
                                                           value=""
                                                    >
                                                </div>
                                            </div>
                                            <blockquote>
                                                <?= __("Your API URL and Key can be found in your account on the My Settings page under the \"Developer\" tab", $this->plugin_name); ?>
                                            </blockquote>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <hr/>
                        <fieldset>
                            <legend>
                                <img class="ays_integration_logo" src="<?php echo POLL_MAKER_AYS_ADMIN_URL; ?>/images/integrations/slack_logo.png" alt="">
                                <h5><?php echo __('Slack',$this->plugin_name)?></h5>
                            </legend>
                            <div class="form-group row" style="padding:0;margin:0;">
                                <div class="col-sm-12" style="padding:20px;">
                                    <div class="pro_features" style="justify-content:flex-end;">
                                        <div style="margin-right:20px;">
                                            <p style="font-size:20px;">
                                                <?php echo __("This feature is available only in ", $this->plugin_name); ?>
                                                <a href="https://ays-pro.com/wordpress/poll-maker" target="_blank" title="DEVELOPER feature"><?php echo __("DEVELOPER packadge!!!", $this->plugin_name); ?></a>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <div class="form-group row" aria-describedby="aaa">
                                                <div class="col-sm-3">
                                                    <button id="slackInstructionsPopOver" type="button" class="btn btn-info"
                                                            data-toggle="popover"
                                                            title="<?= __("Slack Integration Setup Instructions", $this->plugin_name) ?>"><?= __("Instructions", $this->plugin_name) ?></button>
                                                    
                                                </div>
                                            </div>
                                            <div class="form-group row" aria-describedby="aaa">
                                                <div class="col-sm-3">
                                                    <label for="ays_slack_client">
                                                        <?= __('App Client ID', $this->plugin_name) ?>
                                                    </label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <input type="text"
                                                           class="ays-text-input"
                                                           id="ays_slack_client"
                                                           name="ays_slack_client"
                                                           value=""
                                                    >
                                                </div>
                                            </div>
                                            <hr/>
                                            <div class="form-group row" aria-describedby="aaa">
                                                <div class="col-sm-3">
                                                    <label for="ays_slack_oauth">
                                                        <?= __('Slack Authorization', $this->plugin_name) ?>
                                                    </label>
                                                </div>
                                                <div class="col-sm-9">
                                                    
                                                        <span class="btn btn-success pointer-events-none">
                                                        <?= __("Authorized", $this->plugin_name) ?></span>
                                                </div>
                                            </div>
                                            <hr/>
                                            <div class="form-group row" aria-describedby="aaa">
                                                <div class="col-sm-3">
                                                    <label for="ays_slack_secret">
                                                        <?= __('App Client Secret', $this->plugin_name) ?>
                                                    </label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <input type="text"
                                                           class="ays-text-input"
                                                           id="ays_slack_secret"
                                                           name="ays_slack_secret"
                                                           value="" >
                                                </div>
                                            </div>
                                            <hr/>
                                            <div class="form-group row" aria-describedby="aaa">
                                                <div class="col-sm-3">
                                                    <label for="ays_slack_oauth">
                                                        <?= __('App Access Token', $this->plugin_name) ?>
                                                    </label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <button type="button"
                                                            class="btn btn-outline-secondary disabled"><?= __("Need Authorization", $this->plugin_name) ?>
                                                    </button>
                                                    <input type="hidden" id="ays_slack_token" name="ays_slack_token" value="">
                                                </div>
                                            </div>
                                            <blockquote>
                                                <?= __("You can get your App Client ID and Client Secret from your App's the Basic Information page", $this->plugin_name); ?>
                                            </blockquote>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div id="tab3"
                         class="ays-poll-tab-content <?php echo ($ays_poll_tab == 'tab3') ? 'ays-poll-tab-content-active' : ''; ?>">
                        <p class="ays-subtitle"><?php echo __('Shortcodes', $this->plugin_name) ?></p>
                        <hr/>
                        <div class="form-group row">
                            <div class="col-sm-3">
                                <label for="all_ays_poll_shortcodes">
                                    <?php echo __("Shortcode for all polls ", $this->plugin_name); ?>
                                </label>
                            </div>
                            <div class="col-sm-9">
                                <input type="text" name="all_ays_poll_shortcodes" id="all_ays_poll_shortcodes" onclick="this.setSelectionRange(0, this.value.length)"readonly="" class="ays-text-input" value="[ays_poll_all]">
                                    
                            </div>
                        </div>
                        <blockquote>
                            <?php echo __("Ability to show all Polls.", $this->plugin_name); ?>
                        </blockquote>
                        <hr/>
                        <div class="form-group row">
                            <div class="col-sm-3">
                                <label for="ays_poll_shortcodes_by_id">
                                    <?php echo __("Shortcode result by ID ", $this->plugin_name); ?>
                                </label>
                            </div>
                            <div class="col-sm-9">
                                <input type="text" name="ays_poll_shortcodes_by_id" id="ays_poll_shortcodes_by_id" onclick="this.setSelectionRange(0, this.value.length)"readonly="" class="ays-text-input" value='[ayspoll_results id="Your Poll ID"]'>
                                    
                            </div>
                        </div>
                        <blockquote>
                            <?php echo __("Ability to show Polls results by ID.", $this->plugin_name); ?>
                        </blockquote>
                    </div>
                </div>
            </div>
            <hr/>
			<?php
			wp_nonce_field('settings_action', 'settings_action');
			$other_attributes = array();
			submit_button(__('Save changes', $this->plugin_name), 'primary ays-button', 'ays_submit', true, $other_attributes);
			?>
        </form>
    </div>
</div>