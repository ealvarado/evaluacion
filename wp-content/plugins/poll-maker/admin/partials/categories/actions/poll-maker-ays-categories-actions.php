<?php
$action   = (isset($_GET['action'])) ? sanitize_text_field($_GET['action']) : '';
$heading  = '';
$id       = (isset($_GET['poll_category'])) ? absint(intval($_GET['poll_category'])) : null;
$category = array(
	'id'          => '',
	'title'       => '',
	'description' => '',
	'options'     => json_encode(array(
		'allow_skip' => 'allow',
		'next_text'  => 'Next',
	)),
);
switch ( $action ) {
	case 'add':
		$heading = __('Add new category', $this->plugin_name);
		break;
	case 'edit':
		$heading  = __('Edit category', $this->plugin_name);
		$category = $this->cats_obj->get_poll_category($id);
		break;
    default:
        break;
}
$cat_opt = json_decode($category['options'], true);
if (isset($_POST['ays_submit'])) {
	$this->cats_obj->add_edit_poll_category($_POST, $id);
} elseif (isset($_POST['ays_apply'])) {
	$this->cats_obj->add_edit_poll_category($_POST, $id, 'apply');
}
?>
<div class="wrap">
    <div class="container-fluid">
        <h1><?= $heading; ?></h1>
        <hr/>
        <form class="ays-poll-category-form" id="ays-poll-category-form" method="post">
            <div class="form-group row">
                <div class="col-sm-3">
                    <label for='ays-title'>
						<?= __('Title', $this->plugin_name); ?>
                        <a class="ays_help"
                           data-toggle="tooltip"
                           data-placement="top"
                           title="<?= __('The name of category', $this->plugin_name); ?>">
                            <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                        </a>
                    </label>
                </div>
                <div class="col-sm-9">
                    <input class='ays-text-input ays-text-input-short' id='ays-title' name='ays_title' required
                           type='text' value='<?= stripslashes($category['title']); ?>'>
                </div>
            </div>
            <hr>
            <div class='ays-field'>
                <label for='ays-description'>
					<?= __('Description', $this->plugin_name); ?>
                    <a class="ays_help"
                       data-toggle="tooltip"
                       data-placement="top"
                       title="<?= __('The description of category', $this->plugin_name); ?>">
                        <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                    </a>
                </label>
				<?php
				$content   = stripslashes($category['description']);
				$editor_id = 'ays-description';
				$settings  = array(
					'editor_height' => '5',
					'textarea_name' => 'ays_description',
					'editor_class'  => 'ays-textarea',
					'media_buttons' => false
				);
				wp_editor($content, $editor_id, $settings);
				?>
            </div>
            <hr>
            <div class="form-group row">
                <div class="col-sm-3">
                    <label for='ays-poll-skip'><?= __('Allow to skip polls', $this->plugin_name); ?>
                        <a class="ays_help" data-toggle="tooltip" data-placement="top"
                           title="<?= __('If you allow, the "Next" button will always be enabled, and if not, only after polling', $this->plugin_name); ?>">
                            <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                        </a>
                    </label>
                </div>
                <div class="col-sm-9">
                    <input type="checkbox" name="ays_poll_allow_skip" id="ays-poll-skip"
                           value="allow" <?= isset($cat_opt['allow_skip']) && $cat_opt['allow_skip'] == 'allow' ? 'checked' : ''; ?>>
                </div>
            </div>
            <hr>
            <div class="form-group row">
                <div class="col-sm-3">
                    <label for='ays_poll_next_text'><?= __('Next button text', $this->plugin_name); ?><a
                                class="ays_help" data-toggle="tooltip" data-placement="top"
                                title="<?= __('Next poll button text', $this->plugin_name); ?>">
                            <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                        </a></label>
                </div>
                <div class="col-sm-9">
                    <input class='ays-text-input ays-text-input-short' id='ays_poll_next_text' name='ays_poll_next_text'
                           required type='text'
                           value='<?= empty($cat_opt['next_text']) ? 'Next' : $cat_opt['next_text']; ?>'>
                </div>
            </div>
            <hr>
			<?php
			wp_nonce_field('poll_category_action', 'poll_category_action');
			$other_attributes = array('id' => 'ays-button');
			submit_button(__('Save Category', $this->plugin_name), 'primary', 'ays_submit', false, $other_attributes);
			if (null != $id) {
				submit_button(__('Apply Category', $this->plugin_name), '', 'ays_apply', false, $other_attributes);
			}
			?>
        </form>
    </div>
</div>