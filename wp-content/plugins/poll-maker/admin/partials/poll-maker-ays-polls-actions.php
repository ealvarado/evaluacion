<?php
$action        = (isset($_GET['action'])) ? sanitize_text_field($_GET['action']) : '';
$active_tab    = (!empty($_GET['active-tab'])) ? sanitize_text_field($_GET['active-tab']) : 'General';
$heading       = '';
$image_text    = __('Add Image', $this->plugin_name);
$image_text_bg = __('Add Image', $this->plugin_name);

$id              = (isset($_GET['poll'])) ? absint(intval($_GET['poll'])) : null;

if ($action == 'edit') {
    if ($id === null || $id === 0) {
        $url = esc_url_raw(remove_query_arg(array('action', 'poll')));
        wp_safe_redirect($url);
    }
}

$user_id = get_current_user_id();
$user = get_userdata($user_id);
$author = array(
    'id' => $user->ID,
    'name' => $user->data->display_name
);

$poll            = array(
	'title'       => 'Default title',
	'description' => '',
	'categories'  => array(),
	'image'       => '',
	'question'    => '',
	'type'        => 'choosing',
	'view_type'   => '',
	'answers'     => array(),
	'show_title'  => 1,
	'styles'      => '',
	'custom_css'  => '',
	'theme_id'    => 1,
);
$default_colors  = array(
	"main_color"       => "#0C6291",
	"text_color"       => "#0C6291",
	"icon_color"       => "#0C6291",
	"box_shadow_color" => "#000000",
	"bg_color"         => "#FBFEF9",
	"answer_bg_color"  => "#FBFEF9",
	"title_bg_color"   => "#FBFEF9",
);
$default_options = array(
	'randomize_answers'           => 'off',
	"icon_size"                   => 24,
	"width"                       => 600,
	"btn_text"                    => __('Vote', $this->plugin_name),
	"see_res_btn_text"            => __('See Results', $this->plugin_name),
	"border_style"                => "ridge",
	"border_radius"               => 0,
	"border_width"                => 2,
	"enable_box_shadow"           => "",
	"bg_image"                    => "",
	"hide_results"                => 0,
	"hide_results_text"           => "<p style='text-align: center'>" . __("Thanks for your answer!", $this->plugin_name) . "</p>",
	"allow_not_vote"              => 0,
	"show_social"                 => 0,
	"load_effect"                 => "load_gif",
	"load_gif"                    => "plg_default",
	'limit_users'                 => 0,
	"limitation_message"          => "<p style='text-align: center'>" . __("You have already voted", $this->plugin_name) . "</p>",
	'redirect_url'                => '',
	'redirection_delay'           => '',
	'users_role'                  => '',
	'enable_restriction_pass'     => 0,
	'restriction_pass_message'    => "<p style='text-align: center'>" . __("You don't have permissions for passing the poll", $this->plugin_name) . "</p>",
	'enable_logged_users'         => 0,
	'enable_logged_users_message' => "<p style='text-align: center'>" . __('You must sign in for passing the poll', $this->plugin_name) . "</p>",
	'notify_email_on'             => 0,
	'notify_email'                => '',
	'result_sort_type'            => 'none',
    'redirect_after_submit'       => 0,
	'redirect_users'              => 0,
	'redirect_after_vote_url'     => '',
	'redirect_after_vote_delay'   => '',
	'published'                   => 1,
	'enable_pass_count'           => 'on',
    'activeInterval'              => '',
    'create_date' 				  => current_time( 'mysql' ),
    'author' 					  => $author,
    'deactiveInterval'            => '',
    'enable_background_gradient' => 'off',
    'background_gradient_color_1' => '#103251',
    'background_gradient_color_2' => '#607593',
    'poll_gradient_direction' => 'vertical',
	'activeIntervalSec'           => '',
	'deactiveIntervalSec'         => '',
	'active_date_check'           => '',
	'active_date_message_soon'    => "<p style='text-align: center'>" . __("The poll will be available soon!", $this->plugin_name) . "</p>",
	'active_date_message'         => "<p style='text-align: center'>" . __("The poll has expired!", $this->plugin_name) . "</p>",
	'enable_restart_button'       => 0,
    'enable_vote_btn'             => 1,
	'show_votes_count'            => 1,
    'show_res_percent'            => 1,
	'poll_direction'              => 'ltr',
	'info_form'                   => 0,
	'fields'                      => 'apm_name,apm_email,apm_phone',
	'required_fields'             => 'apm_email',
	'info_form_title'             => "<h5>" . __("Please fill out the form:", $this->plugin_name) . "</h5>",
	'disable_answer_hover'        => 0,
	'enable_asnwers_sound'        => 'off'

);
switch ( $action ) {
	case 'add':
		$heading = __('Add new poll', $this->plugin_name);
		$options = array_merge($default_options, $default_colors);
		break;
	case 'edit':
		$heading = __('Edit poll', $this->plugin_name);
		$poll    = $this->polls_obj->get_poll_by_id($id);
        
        if (empty($poll)) {
            $url = esc_url_raw(remove_query_arg(array('action', 'poll')));
            wp_safe_redirect($url);
        }

		$options = array_merge($default_options, $poll['styles']);
		break;
	default:
		$url = esc_url_raw(remove_query_arg(array('action', 'poll')));
        wp_safe_redirect($url);
		break;
}
$enable_pass_count = $options['enable_pass_count'];
$categories        = $this->polls_obj->get_categories();
global $wp_roles;
$ays_users_roles = $wp_roles->roles;
if (isset($_POST['ays_submit']) || isset($_POST['ays_submit_top'])) {
	$this->polls_obj->add_or_edit_polls($_POST, $id);
}
if (isset($_POST['ays_apply_top']) || isset($_POST['ays_apply'])) {
	$this->polls_obj->add_or_edit_polls($_POST, $id, "apply");
}
$style    = "display: none;";
$style_bg = "display: none;";
if (isset($poll['image']) && !empty($poll['image'])) {
	$style      = "display: block;";
	$image_text = __('Edit Image', $this->plugin_name);
}
if (isset($options['bg_image']) && !empty($options['bg_image'])) {
	$style_bg      = "display: block;";
	$image_text_bg = __('Edit Image', $this->plugin_name);
}

$published = $options['published'];


if (!empty($options['activeInterval']) && !empty($options['deactiveInterval'])) {
	$activateTime   = strtotime($options['activeInterval']);
	$activePoll     = date('Y-m-d', $activateTime);
	$deactivateTime = strtotime($options['deactiveInterval']);
	$deactivePoll   = date('Y-m-d', $deactivateTime);
} else {
	$activePoll     = date('Y-m-d');
	$deactivePoll   = date('Y-m-d');
}

$activePollSec      = isset($options['activeIntervalSec']) && !empty($options['activeIntervalSec']) ? $options['activeIntervalSec'] : '';
$deactivePollSec    = isset($options['deactiveIntervalSec']) && !empty($options['deactiveIntervalSec']) ? $options['deactiveIntervalSec'] : '';

$activePoll = $activePoll . " " . $activePollSec;
$deactivePoll = $deactivePoll . " " . $deactivePollSec;

$randomize_answers = (isset($options['randomize_answers']) && $options['randomize_answers'] == 'on') ? true : false;

$all_fields      = array(
	array(
		"name" => __("Name", $this->plugin_name),
		"slug" => "apm_name",
	),
	array(
		"name" => __("Email", $this->plugin_name),
		"slug" => "apm_email",
	),
	array(
		"name" => __("Phone", $this->plugin_name),
		"slug" => "apm_phone",
	),
);


//INTEGRATIONS
$poll_settings = $this->settings_obj;

$settings_options = $this->settings_obj->ays_get_setting('options');
if($settings_options){
    $settings_options = json_decode($settings_options, true);
}else{
    $settings_options = array();
}
$asnwers_sound = (isset($settings_options['answers_sound']) && $settings_options['answers_sound'] != '') ? true : false;

$answers_sound_status = false;
if($asnwers_sound){
    $answers_sound_status = true;
}

// Answers sound option
$options['enable_asnwers_sound'] = isset($options['enable_asnwers_sound']) ? $options['enable_asnwers_sound'] : 'off';
$enable_asnwers_sound = (isset($options['enable_asnwers_sound']) && $options['enable_asnwers_sound'] == "on") ? true : false;

$mailchimp_res      = ($poll_settings->ays_get_setting('mailchimp') === false) ? json_encode(array()) : $poll_settings->ays_get_setting('mailchimp');
$mailchimp          = json_decode($mailchimp_res, true);
$mailchimp_username = isset($mailchimp['username']) ? $mailchimp['username'] : '';
$mailchimp_api_key  = isset($mailchimp['apiKey']) ? $mailchimp['apiKey'] : '';
$mailchimp_lists    = $mailchimp_api_key ? $this->ays_get_mailchimp_lists($mailchimp_username, $mailchimp_api_key) : array();
$mailchimp_select   = array();
if (!empty($mailchimp_lists) && $mailchimp_lists['total_items'] > 0) {
    foreach ( $mailchimp_lists['lists'] as $list ) {
        $mailchimp_select[] = array(
            'listId'   => $list['id'],
            'listName' => $list['name']
        );
    }
} else {
    $mailchimp_select = __("There are no lists", $this->plugin_name);
}

// MailChimp
$enable_mailchimp = (isset($options['enable_mailchimp']) && $options['enable_mailchimp'] == 'on') ? true : false;
$mailchimp_list = (isset($options['mailchimp_list'])) ? $options['mailchimp_list'] : '';


$fields          = !empty($options['fields']) ? explode(",", $options['fields']) : array();
$required_fields = !empty($options['required_fields']) ? explode(",", $options['required_fields']) : array();

// Show votes count
$options['show_votes_count'] = isset($options['show_votes_count']) ? $options['show_votes_count'] : 1;
$showvotescount = isset($options['show_votes_count']) && intval($options['show_votes_count']) == 1 ? true : false;

// Show result percent
$options['show_res_percent'] = isset($options['show_res_percent']) ? $options['show_res_percent'] : 1;
$show_res_percent = isset($options['show_res_percent']) && intval($options['show_res_percent']) == 1 ? true : false;

// Show result button after schedule
$options['show_result_btn_schedule'] = isset($options['show_result_btn_schedule']) ? $options['show_result_btn_schedule'] : 0;
$showresbtnschedule = isset($options['show_result_btn_schedule']) && intval($options['show_result_btn_schedule']) == 1 ? true : false;

$schedule_show_timer = isset($options['ays_poll_show_timer']) && intval($options['ays_poll_show_timer']) == 1 ? true : false;

$show_timer_type = isset($options['ays_show_timer_type']) && !empty($options['ays_show_timer_type'])? $options['ays_show_timer_type'] : 'countdown';

// Show login form for not logged in users
$options['show_login_form'] = isset($options['show_login_form']) ? $options['show_login_form'] : 'off';
$show_login_form = (isset($options['show_login_form'] ) && $options['show_login_form'] == "on") ? true : false;

// Background gradient
$options['enable_background_gradient'] = (!isset($options['enable_background_gradient'])) ? 'off' : $options['enable_background_gradient'];
$enable_background_gradient = (isset($options['enable_background_gradient']) && $options['enable_background_gradient'] == 'on') ? true : false;

$background_gradient_color_1 = (isset($options['background_gradient_color_1']) && $options['background_gradient_color_1'] != '' && $enable_background_gradient) ? $options['background_gradient_color_1'] : '#103251';
$background_gradient_color_2 = (isset($options['background_gradient_color_2']) && $options['background_gradient_color_2'] != '' && $enable_background_gradient) ? $options['background_gradient_color_2'] : '#607593';
$poll_gradient_direction = (isset($options['poll_gradient_direction']) && $options['poll_gradient_direction'] != '') ? $options['poll_gradient_direction'] : 'vertical';


// Redirect after submit
$options['redirect_after_submit'] = (!isset($options['redirect_after_submit'])) ? 0 : $options['redirect_after_submit'];
$redirect_after_submit = (isset($options['redirect_after_submit']) && $options['redirect_after_submit'] == 1) ? true : false;
$submit_redirect_url = isset($options['ays_submit_redirect_url']) ? $options['ays_submit_redirect_url'] : '';
// $submit_redirect_delay = isset($options['submit_redirect_delay']) ? $options['submit_redirect_delay'] : '';

$users_role   = (isset($options['users_role']) && $options['users_role'] != "") ? json_decode($options['users_role'], true) : array();

$options['enable_answer_style'] = isset($options['enable_answer_style']) ? $options['enable_answer_style'] : 'on';

$poll_create_date = (isset($options['create_date']) && $options['create_date'] != '') ? $options['create_date'] : "0000-00-00 00:00:00";

if(isset($options['author']) && $options['author'] != 'null'){
    $poll_author = $options['author'];
} else {
    $poll_author = array('name' => 'Unknown');
}

$show_create_date = (isset($options['show_create_date']) && $options['show_create_date'] == 1) ? true : false;
$show_author = (isset($options['show_author']) && $options['show_author'] == 1) ? true : false;

$custom_class = (isset($options['custom_class']) && $options['custom_class'] != "") ? $options['custom_class'] : '';
?>
<!--LIVE PREVIEW STYLES-->
<?php
$emoji = array(
	"<i class='ays_poll_far ays_poll_fa-dizzy'></i>",
	"<i class='ays_poll_far ays_poll_fa-smile'></i>",
	"<i class='ays_poll_far ays_poll_fa-meh'></i>",
	"<i class='ays_poll_far ays_poll_fa-frown'></i>",
	"<i class='ays_poll_far ays_poll_fa-tired'></i>",
); ?>
<style>
    /*save changing properties of poll in the css-variables*/
    :root {
        /*colors*/
        --theme-main-color: <?=$options['main_color']?>;
        --theme-bg-color: <?=$options['bg_color']?>;
        --theme-answer-bg-color: <?= isset($options['answer_bg_color']) && !empty($options['answer_bg_color']) ? $options['answer_bg_color'] : $options['bg_color'] ?>;
        --theme-title-bg-color: <?= isset($options['title_bg_color']) && !empty($options['title_bg_color']) ? $options['title_bg_color'] : $options['bg_color'] ?>;
        --theme-text-color: <?=$options['text_color']?>;
        --theme-icon-color: <?=$options['icon_color']?>;
        /*options*/
        --poll-width: <?= (int) $options['width'] > 0 ? (int) $options['width'] . "px" : "100%" ?>;
        --poll-border-style: <?=$options['border_style']?>;
        --poll-border-radius: <?= absint($options['border_radius'])?>px;
        --poll-border-width: <?= absint($options['border_width'])?>px;
        --poll-box-shadow: <?= (isset($options['box_shadow_color']) && !empty($options['box_shadow_color'])) ? $options['box_shadow_color'] . ' 0px 0px 10px 0px' : '' ?>;
        --poll-bagckround-image: <?= !empty($options['bg_image']) ? "url({$options['bg_image']})" : "unset" ?>;
        --poll-icons-size: <?= absint($options['icon_size']) >= 10 ? absint($options['icon_size']) : 24 ?>px;
        --poll-display-title: <?= $poll['show_title'] ? "block" : "none"?>;
        --poll-display-image-box: <?= !empty($poll['image']) ? "block" : "none" ?>;

    }
</style>
<!--LIVE PREVIEW STYLES END-->
<div class="wrap">
    <div class="container-fluid">
        <form class="ays-poll-form" id="ays-poll-form" method="post">
            <input type="hidden" name="ays_poll_active_tab" id="ays_poll_active_tab"
                   value="<?php echo htmlentities($active_tab); ?>"/>
           	<input type="hidden" name="ays_poll_ctrate_date" value="<?php echo $poll_create_date; ?>">
           	<input type="hidden" name="ays_poll_author" value="<?php echo htmlentities(json_encode($poll_author)); ?>">
            <h1 class="wp-heading-inline">
				<?php
				echo "$heading";
				$other_attributes = array('id' => 'ays-button-top');
				submit_button(__('Save and close', $this->plugin_name), 'primary', 'ays_submit_top', false, $other_attributes);
                submit_button(__('Save', $this->plugin_name), '', 'ays_apply_top', false, $other_attributes);
				?>
            </h1>
            <div>
                <p class="ays-subtitle">
                    <strong class="ays_poll_title_in_top">
						<?php echo stripslashes(htmlentities($poll['title'])); ?>
                    </strong>
                </p>
                <?php if($id !== null): ?>
                <div class="row">
                    <div class="col-sm-3">
                        <label> <?php echo __( "Shortcode text for editor", $this->plugin_name ); ?> </label>
                    </div>
                    <div class="col-sm-9">
                        <p style="font-size:14px; font-style:italic;">
                            <?php echo __("To insert the Poll into a page, post or text widget, copy shortcode", $this->plugin_name); ?>
                            <strong style="font-size:16px; font-style:normal;"><?php echo "[ays_poll id=".$id."]"; ?></strong>
                            <?php echo " " . __( "and paste it at the desired place in the editor.", $this->plugin_name); ?>
                        </p>
                    </div>
                </div>
                <?php endif;?>
            </div>
            <hr>
            <div class="nav-tab-wrapper">
                <a href="#tab1" data-title="General"
                   class="nav-tab <?= $active_tab == 'General' ? 'nav-tab-active' : ''; ?>">
					<?= __('General', $this->plugin_name); ?>
                </a>
                <a href="#tab2" data-title="Styles"
                   class="nav-tab <?= $active_tab == 'Styles' ? 'nav-tab-active' : ''; ?>">
                    <?= __('Styles', $this->plugin_name); ?>
                </a>
                <a href="#tab3" data-title="Settings"
                   class="nav-tab <?= $active_tab == 'Settings' ? 'nav-tab-active' : ''; ?>">
					<?= __('Settings', $this->plugin_name); ?>
                </a>
                <a href="#tab4" data-title="Limitations"
                   class="nav-tab <?= $active_tab == 'Limitations' ? 'nav-tab-active' : ''; ?>">
					<?= __("Limitations", $this->plugin_name); ?>
                </a>                
                <a href="#tab5" data-title="Userdata"
                   class="nav-tab <?= $active_tab == 'Userdata' ? 'nav-tab-active' : ''; ?>">
					<?= __('User Data', $this->plugin_name); ?>
                </a>
                <a href="#tab6" data-title="Email"
                   class="nav-tab <?= $active_tab == 'Email' ? 'nav-tab-active' : ''; ?>">
					<?= __('Email', $this->plugin_name); ?>
                </a>
                <a href="#tab7" data-title="Integrations"
                   class="nav-tab <?= $active_tab == 'Integrations' ? 'nav-tab-active' : ''; ?>">
					<?= __('Integrations', $this->plugin_name); ?>
                </a>
            </div>
            <div id="tab1"
                 class="ays-poll-tab-content <?= $active_tab == 'General' ? 'ays-poll-tab-content-active' : ''; ?>">
                <p class="ays-subtitle">
					<?= __('General options', $this->plugin_name); ?>
                </p>
                <hr>
                <div class="form-group row">
                    <div class="col-sm-3">
                        <label>
							<?= __('Status', $this->plugin_name); ?>
                            <a class="ays_help" data-toggle="tooltip" data-placement="top"
                               title="<?= __('Status of the poll', $this->plugin_name) ?>">
                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                            </a>
                        </label>
                    </div>
                    <div class="col-sm-9">
                        <div class="d-flex">
                            <div class="form-check form-check-inline">
                                <input type="radio" id="ays-publish" name="ays_publish"
                                       value="1" <?= ($published == 1) ? 'checked' : ''; ?> />
                                <label class="form-check-label"
                                       for="ays-publish"> <?= __('Published', $this->plugin_name); ?> </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input type="radio" id="ays-unpublish" name="ays_publish"
                                       value="0" <?= ($published == 0) ? 'checked' : ''; ?> />
                                <label class="form-check-label"
                                       for="ays-unpublish"> <?= __('Unpublished', $this->plugin_name); ?> </label>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="form-group row">
                    <div class="col-sm-3">
                        <label for='ays-poll-title'>
							<?= __('Title', $this->plugin_name); ?>
                            <a class="ays_help" data-toggle="tooltip"
                               data-placement="top"
                               title="<?= __("The name of the poll", $this->plugin_name); ?>">
                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                            </a>
                        </label>
                    </div>
                    <div class="col-sm-9">
                        <input type="text" class="ays-text-input" id='ays-poll-title' name='ays-poll-title'
                               data-required="true" value="<?= stripslashes(htmlentities($poll['title'])); ?>"/>
                    </div>
                </div>
                <hr>
                <div class='form-group row'>
                    <div class="col-sm-3">
                        <label for='ays-poll-category'>
							<?= __('Categories', $this->plugin_name); ?>
                            <a class="ays_help" data-toggle="tooltip"
                               data-placement="top"
                               title="<?= __("The categories of the poll", $this->plugin_name); ?>">
                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                            </a>
                        </label>
                    </div>
                    <div class="col-sm-9">
						<?php if (!empty($categories)): ?>
                            <select id="ays-poll-category" class="apm-cat-select2" name="ays-poll-categories[]" multiple
                                    data-placeholder='<?= __("Select category", $this->plugin_name) ?>'>
								<?php
								foreach ( $categories as $cat ) {
									?>
                                    <option value="<?= $cat['id']; ?>" <?= in_array($cat['id'], $poll['categories']) ? 'selected' : ''; ?>>
										<?= $cat['title']; ?>
                                    </option>
								<?php }
								?>
                            </select>
						<?php else: ?>
                            <a href="?page=poll-maker-ays-cats&action=add"><?= __("Create category", $this->plugin_name) ?></a>
						<?php endif; ?>
                    </div>
                </div>
                <hr>
                <div class="ays-field form-group">
                    <label for='ays-poll-question'>
						<?= __('Question', $this->plugin_name); ?><a class="ays_help" data-toggle="tooltip"
                                                                     data-placement="top"
                                                                     title="<?= __("The question of the poll", $this->plugin_name); ?>">
                            <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                        </a>
                        <a href="javascript:void(0)" class="add-question-image button">
							<?= $image_text; ?><a class="ays_help" data-toggle="tooltip" data-placement="top"
                                                  title="<?= __("Add image on the question", $this->plugin_name); ?>">
                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                            </a></a>
                    </label>
                    <div class="ays-poll-question-image-container" style="<?= $style; ?>">
                        <span class="ays-remove-question-img"></span>
                        <img src="<?= $poll['image']; ?>" id="ays-poll-img"/>
                        <input type="hidden" name="ays_poll_image" id="ays-poll-image" value="<?= $poll['image']; ?>"/>
                    </div>
					<?php
					$content   = stripslashes($poll["question"]);
					$editor_id = 'ays-poll-question';
					$settings  = array(
						'editor_height' => '15',
						'textarea_name' => 'ays_poll_question',
						'editor_class'  => 'ays-textarea',
						'media_buttons' => true,
						'tinymce'       => array(
							"init_instance_callback" => "function(editor) {
                                editor.on('Change', function(e) {
                                    document.querySelector('.box-apm .ays_question').innerHTML = e.level.content;
                                });
                            }",
						)
					);
					wp_editor($content, $editor_id, $settings);
					?>
                </div>
                <hr>
                <div class="form-group row">
                    <div class="col-sm-3">
                        <label for="ays-poll-type">
							<?= __('Type', $this->plugin_name); ?><a class="ays_help" data-toggle="tooltip" data-placement="top" title="<?= __("The type of the poll", $this->plugin_name); ?>">
                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                            </a></label>
                    </div>
                    <div class="col-sm-9 apm-types-row d-flex apm-pro-feature-block">
                        <div class="ays_poll_type_image_div col">
                            <label for="type_choosing"
                                   class="<?= $poll['type'] == 'choosing' ? "apm_active_type" : ""; ?>">
                                <p><?= __('Choosing', $this->plugin_name) ?></p>
                                <img src="<?= POLL_MAKER_AYS_ADMIN_URL . '/images/polls/choosing.png' ?>"
                                     alt="<?= __('Choosing', $this->plugin_name) ?>">
                            </label>
                            <input type="radio" name="ays-poll-type" id="type_choosing"
                                   value="choosing" <?= $poll['type'] == 'choosing' ? "checked" : ""; ?>>
                        </div>
                        <div class="ays_poll_type_image_div col">
                            <label for="type_rating" class="<?= $poll['type'] == 'rating' ? "apm_active_type" : ""; ?>">
                                <p><?= __('Rating', $this->plugin_name) ?></p>
                                <img src="<?= POLL_MAKER_AYS_ADMIN_URL . '/images/polls/rating.png' ?>"
                                     alt="<?= __('Rating', $this->plugin_name) ?>">
                            </label>
                            <input type="radio" name="ays-poll-type" id="type_rating"
                                   value="rating" <?= $poll['type'] == 'rating' ? "checked" : ""; ?>>
                        </div>
                        <div class="ays_poll_type_image_div col">
                            <label for="type_voting" class="<?= $poll['type'] == 'voting' ? "apm_active_type" : ""; ?>">
                                <p><?= __('Voting', $this->plugin_name) ?></p>
                                <img src="<?= POLL_MAKER_AYS_ADMIN_URL . '/images/polls/voting.png' ?>"
                                     alt="<?= __('Voting', $this->plugin_name) ?>">
                            </label>
                            <input type="radio" name="ays-poll-type" id="type_voting"
                                   value="voting" <?= $poll['type'] == 'voting' ? "checked" : ""; ?>>
                        </div>
                        <div class="ays_poll_type_image_div col">
	                        <div class="col-sm-12" style="padding:20px;">
			                    <div class="pro_features" style="justify-content:flex-end;">
			                        <div>
			                            <p style="font-size:12px; margin-right: 0;">
			                                <?php echo __("This feature is available only in ", $this->plugin_name); ?>
			                                <a href="https://ays-pro.com/wordpress/poll-maker" target="_blank" title="Developer feature"><?php echo __("Developer package!!!", $this->plugin_name); ?></a>
			                            </p>
			                        </div>
			                    </div>
		                        <div class="ays_poll_type_image_div col">
		                            <label>
		                                <p><?= __('Versus', $this->plugin_name); ?></p>
		                                <img src="<?= POLL_MAKER_AYS_ADMIN_URL . '/images/polls/versus.png' ?>"
		                                     alt="<?= __('Versus', $this->plugin_name); ?>"
		                                     title="<?= __("It is PRO version feature", $this->plugin_name); ?>">
		                            </label>
		                        </div>
	                        </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-9">
                        <blockquote>
							<?= __('If you change the type, the number of counted answers will be annulled.', $this->plugin_name); ?>
                        </blockquote>
                    </div>
                </div>
                <hr>
                <div class="if-choosing poll-type-block form-group">
                    <div class="col-sm-3">
                        <div class="form-group row">
                            <label for="ays-poll-answer">
							<?= __('Answers', $this->plugin_name); ?><a class="ays_help" data-toggle="tooltip" data-placement="top" title="<?= __("Answers from which the choice is made", $this->plugin_name); ?>">
                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                            </a></label>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group row" style="height: 33px;">
                            <!-- empty -->
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12 answers-col">
                                <i class="ays_poll_far ays_poll_fa-plus-square" title="<?= __("Add answer", $this->plugin_name); ?>"
                                   id="add-answer"></i>
								<?php
								if (count($poll['answers']) > 0 && 'choosing' == $poll['type']) {
									$answer_id = 1;
									foreach ( $poll['answers'] as $answer ) { ?>
                                        <div>
                                            <input type="text" class="ays-text-input ays-text-input-short"
                                                   name='ays-poll-answers[]' data-id="<?= $answer_id; ?>"
                                                   value="<?= stripcslashes(esc_html($answer['answer'])); ?>">
                                            <input type="hidden" data-id="<?= $answer_id; ?>"
                                                   name="ays-poll-answers-ids[]" value="<?= $answer['id']; ?>">
											<?= $answer_id > 2 ? '<i class="remove-answer ays_poll_fas ays_poll_fa-minus-square" title="' . __("Remove answer", $this->plugin_name) . '" data-id="'.$answer_id.'"></i>' : ""; ?>
                                        </div>
										<?php $answer_id++;
									}
								} else {
									?>
                                    <div>
                                        <input type="text" class="ays-text-input ays-text-input-short"
                                               name='ays-poll-answers[]'
                                               value="<?= __("Answer 1", $this->plugin_name) ?>">
                                        <input type="hidden" name="ays-poll-answers-ids[]" value="0">
                                    </div>
                                    <div>
                                        <input type="text" class="ays-text-input ays-text-input-short"
                                               name='ays-poll-answers[]'
                                               value="<?= __("Answer 2", $this->plugin_name) ?>">
                                        <input type="hidden" name="ays-poll-answers-ids[]" value="0">
                                    </div>
								<?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="form-group row">
                            <label for="ays_redirect_after_submit" >
                                <?php echo __('Redirect',$this->plugin_name)?>
                                <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Redirect to custom URL after user submit the form.',$this->plugin_name)?>">
                                    <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                                </a>
                                <input type="checkbox" class="ays-enable-timer1 ays_toggle_checkbox" id="ays_redirect_after_submit"
                                   name="ays_redirect_after_submit"
                                   value="on" <?php echo $redirect_after_submit ? 'checked' : '' ?>/>
                            </label>
                        </div>
                         <div class="form-group row" id="ays_redirect_box" style='display: <?php echo $redirect_after_submit ? 'block' : 'none'; ?> '>
                            <?php 
                                if (count($poll['answers']) > 0 && 'choosing' == $poll['type']) {
                                    $answer_id = 1;
                                    foreach ( $poll['answers'] as $answer ) { ?>
                                        
                                        <div class="ays-redirect-parent-box">
                                            <div class="ays-redirect-content-left">
                                                <label for="ays_submit_redirect_url_<?php echo $answer_id; ?>">
                                                    <?php echo __('URL',$this->plugin_name)?>
                                                    <a class="ays_help" data-toggle="tooltip" title="<?php echo __('The URL for redirecting after the user submits the form.',$this->plugin_name)?>">
                                                        <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                                                    </a>
                                                </label>
                                            </div>
                                            <div>
                                                <input type="text" style='width: 370px;' class="ays-text-input ays-text-input-short ays_redirect_active" id="ays_submit_redirect_url_<?php echo $answer_id; ?>"
                                                    name="ays_submit_redirect_url[]"
                                                    value="<?php echo $answer['redirect']; ?>"/>
                                            </div>
                                        </div>
                                        <?php $answer_id++;
                                    }
                                } else {
                            ?>
                            <div class="ays-redirect-parent-box">
                                <div class="ays-redirect-content-left">
                                    <label for="ays_submit_redirect_url_1">
                                        <?php echo __('URL',$this->plugin_name)?>
                                        <a class="ays_help" data-toggle="tooltip" title="<?php echo __('The URL for redirecting after the user submits the form.',$this->plugin_name)?>">
                                            <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                                        </a>
                                    </label>
                                </div>
                                <div>
                                    <input type="url" style='width: 370px;' class="ays-text-input ays-text-input-short ays_redirect_active" id="ays_submit_redirect_url_1"
                                        name="ays_submit_redirect_url[]"
                                        value="<?php echo $submit_redirect_url; ?>"/>
                                </div>
                            </div>

                            <div class="ays-redirect-parent-box">
                                <div class="ays-redirect-content-left">
                                    <label for="ays_submit_redirect_url_2">
                                        <?php echo __('URL',$this->plugin_name)?>
                                        <a class="ays_help" data-toggle="tooltip" title="<?php echo __('The URL for redirecting after the user submits the form.',$this->plugin_name)?>">
                                            <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                                        </a>
                                    </label>
                                </div>
                                <div>
                                    <input type="url" style='width: 370px;' class="ays-text-input ays-text-input-short ays_redirect_active" id="ays_submit_redirect_url_2"
                                        name="ays_submit_redirect_url[]"
                                        value="<?php echo $submit_redirect_url; ?>"/>
                                </div>
                            </div>
                            <?php } ?> 
                        </div>
                    </div>
                </div>
                <hr class="if-choosing poll-type-block">
                <div class="if-choosing poll-type-block form-group row apm-pro-feature-block">
                    <div class="col-sm-3">
                        <label for="ays-poll-answer">
							<?= __('Answers view', $this->plugin_name); ?><a class="ays_help" data-toggle="tooltip" data-placement="top" title="<?= __("Answers from which the choice is made", $this->plugin_name); ?>">
                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                            </a></label>
                    </div>
                    <div class="col-sm-9">
                        <div class="row">
                            <div class="col-sm-3">
                                <input type="radio" id="ays-view-list" name="ays-poll-choose-type" value="list"
                                       checked/>
                                <label class="form-check-label"
                                       for="ays-view-list"> <?= __('List view (Default)', $this->plugin_name); ?> </label>
                            </div>
                            <div class="col-sm-9">
	                            <div class="col-sm-12" >
				                    <div class="pro_features" style="justify-content:flex-end;">
				                        <div>
				                            <p style="font-size:14px;">
				                                <?php echo __("This feature is available only in ", $this->plugin_name); ?>
				                                <a href="https://ays-pro.com/wordpress/poll-maker" target="_blank" title="Developer feature"><?php echo __("Developer package!!!", $this->plugin_name); ?></a>
				                            </p>
				                        </div>
				                    </div>
		                            <div class="form-group" style="height: 70px; margin-bottom: 0;">
		                            	<input type="radio" id="ays-view-grid" name="ays-poll-choose-type" value="grid"/>
										<label class="form-check-label"
		                                       for="ays-view-grid"> <?= __('Grid view', $this->plugin_name); ?> </label>
			                        </div>
		                        </div>
	                        </div>
                        </div>
                    </div>
                </div>
                <hr class="if-choosing poll-type-block">
                <div class="if-choosing poll-type-block row">
                	<div class="col-sm-12" style="padding: 20px;">
	                    <div class="pro_features" style="justify-content:flex-end; width: 98%; left: 10px;">
	                        <div>
	                            <p style="font-size:14px;">
	                                <?php echo __("This feature is available only in ", $this->plugin_name); ?>
	                                <a href="https://ays-pro.com/wordpress/poll-maker" target="_blank" title="Developer feature"><?php echo __("Developer package!!!", $this->plugin_name); ?></a>
	                            </p>
	                        </div>
	                    </div>
	                    <div class="form-group row">
		                    <div class="col-3">
		                        <label for="apm_allow_add_answers">
									<?= __('Allow add custom answer', $this->plugin_name); ?>
		                            <a class="ays_help" data-toggle="tooltip" data-placement="top"
		                               title="<?= __("Allow users to add their answer variant", $this->plugin_name); ?>">
		                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
		                            </a>
		                        </label>
		                    </div>
		                    <div class="col-9">
		                        <input type="checkbox" name="" id="apm_allow_add_answers" >
		                    </div>
	                    </div>
                    </div>        
                </div>
                <div class="if-voting poll-type-block  form-group row">
                    <div class="col-sm-3">
                        <label for="ays-poll-vote-type">
							<?= __('Answers', $this->plugin_name); ?><a class="ays_help" data-toggle="tooltip" data-placement="top" title="<?= __("The appearance of the voting type of the poll", $this->plugin_name); ?>">
                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                            </a></label>
                    </div>
                    <div class="col-sm-9 answers-col">
                        <select class="ays-select" id="ays-poll-vote-type" name="ays-poll-vote-type">
                            <option value='hand' <?= $poll['view_type'] == 'hand' ? "selected" : ""; ?>>
								<?= __('Hand', $this->plugin_name); ?>
                            </option>
                            <option value="emoji" <?= $poll['view_type'] == 'emoji' ? "selected" : ""; ?>>
								<?= __('Emoji', $this->plugin_name); ?>
                            </option>
                        </select>
                        <?php    
                        switch ($poll['view_type']) {
                            case 'hand':
                                $vote_res = 'ays_poll_far ays_poll_fa-thumbs-up';
                                $rate_res = 'ays_poll_fas ays_poll_fa-star';
                                break;

                            case 'emoji':
                                $vote_res = 'ays_poll_fas ays_poll_fa-smile';
                                $rate_res = 'ays_poll_fas ays_poll_fa-smile';
                                break;

                            case 'star':
                                $rate_res = 'ays_poll_fas ays_poll_fa-star';
                                $vote_res = 'ays_poll_far ays_poll_fa-thumbs-up';
                                break;
                            
                            default:
                                $vote_res = 'ays_poll_far ays_poll_fa-thumbs-up';
                                $rate_res = 'ays_poll_fas ays_poll_fa-star';
                                break;
                        }
?>
                        <i id="vote-res" class="<?php echo $vote_res; ?>"></i>
                    </div>
                </div>
                <div class="if-rating poll-type-block  form-group row">
                    <div class="col-sm-3">
                        <label for="ays-poll-rate-type">
							<?= __('Answers', $this->plugin_name); ?><a class="ays_help" data-toggle="tooltip"
                                                                        data-placement="top"
                                                                        title="<?= __("The appearance of the rating type of the poll and scaling system", $this->plugin_name); ?>">
                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                            </a></label>
                    </div>
                    <div class="col-sm-9 answers-col">
                        <select class="ays-select" id="ays-poll-rate-type" name="ays-poll-rate-type">
                            <option value='star' <?= $poll['view_type'] == 'star' ? "selected" : ""; ?>>
								<?= __('Stars', $this->plugin_name); ?>
                            </option>
                            <option value="emoji" <?= $poll['view_type'] == 'emoji' ? "selected" : ""; ?>>
								<?= __('Emoji', $this->plugin_name); ?>
                            </option>
                        </select>
                        <select class="ays-select" id="ays-poll-rate-value" name="ays-poll-rate-value">
                            <option value="<?= count($poll['answers']); ?>" selected>
								<?= count($poll['answers']); ?>
                            </option>
                        </select>
                        <i id="rate-res" class="<?php echo $rate_res; ?>"></i>
                    </div>
                </div>
                <hr>
            </div>
            <div id="tab2"
                 class="ays-poll-tab-content <?= $active_tab == 'Styles' ? 'ays-poll-tab-content-active' : ''; ?>">
                <p class="ays-subtitle">
					<?= __('Styling options', $this->plugin_name); ?>
                </p>
                <hr>
                <div class="form-group row">
                    <div class="col-sm-1">
                        <label for='ays-poll-theme' class="ays_label_flex">
							<?= __('Theme', $this->plugin_name); ?>
                            <a class="ays_help" data-toggle="tooltip" data-placement="top"
                               title="<?= __("The color theme of the poll", $this->plugin_name); ?>">
                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                            </a>
                        </label>
                    </div>
                    <div class="col-sm-11 apm-themes-row d-flex apm-pro-feature-block">
                        <div class="ays_poll_theme_image_div col">
                            <label for="theme_minimal" <?= ($poll['theme_id'] == 3) ? 'class="apm_active_theme"' : '' ?>>
                                <p><?= __('Minimal', $this->plugin_name) ?></p>
                                <img src="<?= POLL_MAKER_AYS_ADMIN_URL . '/images/themes/minimal.png' ?>"
                                     alt="<?= __('Minimal', $this->plugin_name) ?>">
                            </label>
                            <input type="radio" name="ays_poll_theme" id="theme_minimal"
                                   value="3" <?= ($poll['theme_id'] == 3) ? 'checked' : '' ?>>
                        </div>
                        <div class="ays_poll_theme_image_div col">
                            <label for="theme_classic_light" <?= ($poll['theme_id'] <= 1) ? 'class="apm_active_theme"' : '' ?>>
                                <p><?= __('Classic Light', $this->plugin_name) ?></p>
                                <img src="<?= POLL_MAKER_AYS_ADMIN_URL . '/images/themes/ClassicLight.png' ?>"
                                     alt="<?= __('Classic Light', $this->plugin_name) ?>">
                            </label>
                            <input type="radio" name="ays_poll_theme" id="theme_classic_light"
                                   value="1" <?= ($poll['theme_id'] <= 1) ? 'checked' : '' ?>>
                        </div>
                        <div class="ays_poll_theme_image_div col">
                            <label for="theme_classic_dark" <?= ($poll['theme_id'] == 2) ? 'class="apm_active_theme"' : '' ?>>
                                <p><?= __('Classic Dark', $this->plugin_name) ?></p>
                                <img src="<?= POLL_MAKER_AYS_ADMIN_URL . '/images/themes/ClassicDark.png' ?>"
                                     alt="<?= __('Classic Dark', $this->plugin_name) ?>">
                            </label>
                            <input type="radio" name="ays_poll_theme" id="theme_classic_dark"
                                   value="2" <?= ($poll['theme_id'] == 2) ? 'checked' : '' ?>>
                        </div>
                        <div class="ays_poll_theme_image_div col">
	                        <div class="col-sm-12" style="padding:20px;">
			                    <div class="pro_features" style="justify-content:flex-end;">
			                        <div style="margin-right:20px;">
			                            <p style="font-size:20px;">
			                                <?php echo __("This feature is available only in ", $this->plugin_name); ?>
			                                <a href="https://ays-pro.com/wordpress/poll-maker" target="_blank" title="Developer feature"><?php echo __("Developer package!!!", $this->plugin_name); ?></a>
			                            </p>
			                        </div>
			                    </div>
		                        <div class="ays_poll_theme_image_div_pro">
			                        <div class="ays_poll_theme_image_div col apm-pro-feature">
			                            <label>
			                                <p>Light Shape</p>
			                                <img src="<?= POLL_MAKER_AYS_ADMIN_URL . '/images/themes/LightShape.png' ?>"
			                                     alt="Light Shape"
			                                     title="<?= __("It is PRO version feature", $this->plugin_name); ?>">
			                            </label>
			                        </div>
			                        <div class="ays_poll_theme_image_div col apm-pro-feature">
			                            <label>
			                                <p>Dark Shape</p>
			                                <img src="<?= POLL_MAKER_AYS_ADMIN_URL . '/images/themes/DarkShape.png' ?>"
			                                     alt="Light Shape"
			                                     title="<?= __("It is PRO version feature", $this->plugin_name); ?>">
			                            </label>
			                        </div>
			                        <div class="ays_poll_theme_image_div col apm-pro-feature">
			                            <label>
			                                <p>Coffee Fluid</p>
			                                <img src="<?= POLL_MAKER_AYS_ADMIN_URL . '/images/themes/CoffeeFluid.png' ?>"
			                                     alt="Coffee Fluid"
			                                     title="<?= __("It is PRO version feature", $this->plugin_name); ?>">
			                            </label>
			                        </div>
			                        <div class="ays_poll_theme_image_div col apm-pro-feature">
			                            <label>
			                                <p><?= __("Aquamarine", $this->plugin_name) ?></p>
			                                <img src="<?= POLL_MAKER_AYS_ADMIN_URL . '/images/themes/Aquamarine.png' ?>"
			                                     alt="Aquamarine"
			                                     title="<?= __("It is PRO version feature", $this->plugin_name); ?>">
			                            </label>
			                        </div>	                        
		                        </div>
	                        </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="col">
                    <div class="row">
                        <div class="col-lg-6 col-sm-12">
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for='ays-poll-main-color'>
										<?= __('Main Color', $this->plugin_name); ?>
                                        <a class="ays_help"
                                           data-toggle="tooltip"
                                           data-placement="top"
                                           title="<?= __("The central color of the poll (border color, the color of the rate percentage and the background color of sending button)", $this->plugin_name); ?>">
                                            <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                                        </a>
                                    </label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" class="ays-text-input" data-alpha="true" id='ays-poll-main-color'
                                           name='ays_poll_main_color'
                                           value="<?= !empty($options['main_color']) ? $options['main_color'] : $default_colors['main_color']; ?>"/>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for='ays-poll-text-color'>
										<?= __('Text Color', $this->plugin_name); ?>
                                        <a class="ays_help"
                                           data-toggle="tooltip"
                                           data-placement="top"
                                           title="<?= __("The color of the text in the poll", $this->plugin_name); ?>">
                                            <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                                        </a>
                                    </label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" class="ays-text-input" data-alpha="true" id='ays-poll-text-color'
                                           name='ays_poll_text_color'
                                           value="<?= !empty($options['text_color']) ? $options['text_color'] : $default_colors['text_color']; ?>"/>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for='ays-poll-icon-color'>
										<?= __('Icons Color', $this->plugin_name); ?>
                                        <a class="ays_help"
                                           data-toggle="tooltip"
                                           data-placement="top"
                                           title="<?= __("The icons color in voting and rating types", $this->plugin_name); ?>">
                                            <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                                        </a>
                                    </label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" class="ays-text-input" data-alpha="true" id='ays-poll-icon-color'
                                           name='ays_poll_icon_color'
                                           value="<?= !empty($options['icon_color']) ? $options['icon_color'] : $default_colors['icon_color']; ?>"/>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for='ays-poll-bg-color'>
										<?= __('Background Color', $this->plugin_name); ?>
                                        <a class="ays_help" data-toggle="tooltip" data-placement="top" title="<?= __("
                                            Background color of the poll", $this->plugin_name); ?>">
                                            <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                                        </a>
                                    </label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" class="ays-text-input" data-alpha="true" id='ays-poll-bg-color'
                                           name='ays_poll_bg_color'
                                           value="<?= !empty($options['bg_color']) ? $options['bg_color'] : $default_colors['bg_color']; ?>"/>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for='ays-poll-bg-image'>
										<?= __('Background Image', $this->plugin_name); ?>
                                        <a class="ays_help" data-toggle="tooltip" data-placement="top"
                                           title="<?= __("Background image of the poll", $this->plugin_name); ?>">
                                            <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                                        </a>
                                    </label>
                                </div>
                                <div class="col-sm-8">
                                    <a href="javascript:void(0)" class="add-bg-image button">
										<?= $image_text_bg; ?>
                                        <a class="ays_help" data-toggle="tooltip" data-placement="top" title="<?= __("
                                            If you add background image, background color not be applied. Remove image or don't add it, if you want background color will be apply", $this->plugin_name); ?>">
                                            <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                                        </a>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-8" style="<?= $style_bg; ?>">
                                <div class="ays-poll-bg-image-container">
                                    <span class="ays-remove-bg-img"></span>
                                    <img src="<?= isset($options['bg_image']) ? $options['bg_image'] : ""; ?>"
                                         id="ays-poll-bg-img"/>
                                    <input type="hidden" name="ays_poll_bg_image" id="ays-poll-bg-image"
                                           value="<?= isset($options['bg_image']) ? $options['bg_image'] : ""; ?>"/>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for='ays-poll-box-shadow-color'><?= __('Answer styles', $this->plugin_name); ?>
                                        <a class="ays_help" data-toggle="tooltip" data-placement="top"
                                           title="<?= __("The styles of answer", $this->plugin_name); ?>">
                                            <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                                        </a>
                                    </label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="checkbox" class="ays_toggle ays_toggle_slide"
                                           id="ays_poll_enable_answer_style"
                                           name="ays_poll_enable_answer_style" <?= ($options['enable_answer_style'] == 'on') ? 'checked' : ''; ?>>
                                    <label for="ays_poll_enable_answer_style" class="ays_switch_toggle">Toggle</label>
                                    <div class="col-sm-12 ays_toggle_target ays_divider_top ays_answer_style"
                                         style="margin-top: 10px; padding-top: 10px; <?= ($options['enable_answer_style'] == 'on') ? '' : 'display:none;' ?>">
                                        <label for="ays-poll-box-shadow-color">
											<?= __('Answers Background Color', $this->plugin_name) ?>
                                            <a class="ays_help" data-toggle="tooltip"
                                               title="--><?= __('Background color of the answers', $this->plugin_name) ?>">
                                                <i class="ays_fa ays_fa_info_circle"></i>
                                            </a>
                                        </label>
                                        <input type="text" class="ays-text-input" data-alpha="true"
                                           id='ays-poll-answer-bg-color'
                                           name='ays_poll_answer_bg_color'
                                           value="<?= isset($options['answer_bg_color']) && !empty($options['answer_bg_color']) ? $options['answer_bg_color'] : 'rgba(255,255,255,0)' ?>"/>
                                    </div>
	                                <div class="col-sm-12 ays_toggle_target ays_divider_top ays_answer_style" style="margin-top: 10px; padding-top: 10px; <?= ($options['enable_answer_style'] == 'on') ? '' : 'display:none;' ?>">
	                                    <label for='ays-poll-border-side'>
											<?= __('Border side', $this->plugin_name); ?>
	                                        <a class="ays_help"
	                                           data-toggle="tooltip"
	                                           data-placement="top"
	                                           title="<?= __("The side of the border", $this->plugin_name); ?>">
	                                            <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
	                                        </a>
	                                    </label>
	                                    <select name="ays_poll_border_side" id="ays-poll-border-side"
	                                            class="ays-select ays-select-search">
	                                        <option value="all_sides" <?= isset($options['answer_border_side']) && $options['answer_border_side'] == "all_sides" ? 'selected' : ''; ?>>
												<?= __("All sides", $this->plugin_name); ?>
	                                        </option>
	                                        <option value="none" <?= isset($options['answer_border_side']) && $options['answer_border_side'] == "none" ? 'selected' : ''; ?>>
												<?= __("None", $this->plugin_name); ?>
	                                        </option>
	                                        <option value="top" <?= isset($options['answer_border_side']) && $options['answer_border_side'] == "top" ? 'selected' : ''; ?>>
												<?= __("Top", $this->plugin_name); ?>
	                                        </option>
	                                        <option value="bottom" <?= isset($options['answer_border_side']) && $options['answer_border_side'] == "bottom" ? 'selected' : ''; ?>>
												<?= __("Bottom", $this->plugin_name); ?>
	                                        </option>
	                                        <option value="left" <?= isset($options['answer_border_side']) && $options['answer_border_side'] == "left" ? 'selected' : ''; ?>>
												<?= __("Left", $this->plugin_name); ?>
	                                        </option>
	                                        <option value="right" <?= isset($options['answer_border_side']) && $options['answer_border_side'] == "right" ? 'selected' : ''; ?>>
												<?= __("Right", $this->plugin_name); ?>
	                                        </option>	                                        
	                                    </select>
	                                </div>                              
                                </div>
                            </div>                            
                            <hr>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for='ays-poll-title-bg-color'>
										<?= __('Title Background Color', $this->plugin_name); ?>
                                        <a class="ays_help"
                                           data-toggle="tooltip"
                                           data-placement="top"
                                           title="<?= __("The background of the title in the poll", $this->plugin_name); ?>">
                                            <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                                        </a>
                                    </label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" class="ays-text-input" data-alpha="true"
                                           id='ays-poll-title-bg-color'
                                           name='ays_poll_title_bg_color'
                                           value="<?= !empty($options['title_bg_color']) ? $options['title_bg_color'] : 'rgba(255,255,255,0)' ?>"/>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for='ays-poll-icon-size'>
										<?= __('Icon size (px)', $this->plugin_name); ?>
                                        <a class="ays_help"
                                           data-toggle="tooltip"
                                           data-placement="top"
                                           title="<?= __("The size of the icons in rating and voting types of the poll (it should be 10 and more)", $this->plugin_name); ?>">
                                            <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                                        </a>
                                    </label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="number" class="ays-text-input ays-text-input-short"
                                           id='ays-poll-icon-size' name='ays_poll_icon_size'
                                           value="<?= (isset($options['icon_size'])) ? $options['icon_size'] : '24'; ?>"/>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for='ays-poll-width'>
										<?= __('Width (px)', $this->plugin_name); ?>
                                        <a class="ays_help"
                                           data-toggle="tooltip"
                                           data-placement="top"
                                           title="<?= __("The width of the poll (in case of 0 it
                                            will be 100%)", $this->plugin_name); ?>">
                                            <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                                        </a>
                                    </label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="number" step="50" min="0" class="ays-text-input ays-text-input-short"
                                           id='ays-poll-width' name='ays_poll_width'
                                           value="<?= $options['width'] ?>"/>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for='ays-poll-border-style'>
										<?= __('Border style', $this->plugin_name); ?>
                                        <a class="ays_help"
                                           data-toggle="tooltip"
                                           data-placement="top"
                                           title="<?= __("The style of the border", $this->plugin_name); ?>">
                                            <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                                        </a>
                                    </label>
                                </div>
                                <div class="col-sm-8">
                                    <select name="ays_poll_border_style" id="ays-poll-border-style"
                                            class="ays-select ays-select-search">
                                        <option value="solid" <?= $options['border_style'] == "solid" ? 'selected' : ''; ?>>
											<?= __("Solid", $this->plugin_name); ?>
                                        </option>
                                        <option value="dashed" <?= $options['border_style'] == "dashed" ? 'selected' : ''; ?>>
											<?= __("Dashed", $this->plugin_name); ?>
                                        </option>
                                        <option value="dotted" <?= $options['border_style'] == "dotted" ? 'selected' : ''; ?>>
											<?= __("Dotted", $this->plugin_name); ?>
                                        </option>
                                        <option value="double" <?= $options['border_style'] == "double" ? 'selected' : ''; ?>>
											<?= __("Double", $this->plugin_name); ?>
                                        </option>
                                        <option value="groove" <?= $options['border_style'] == "groove" ? 'selected' : ''; ?>>
											<?= __("Groove", $this->plugin_name); ?>
                                        </option>
                                        <option value="ridge" <?= $options['border_style'] == "ridge" ? 'selected' : ''; ?>>
											<?= __("Ridge", $this->plugin_name); ?>
                                        </option>
                                        <option value="inset" <?= $options['border_style'] == "inset" ? 'selected' : ''; ?>>
											<?= __("Inset", $this->plugin_name); ?>
                                        </option>
                                        <option value="outset" <?= $options['border_style'] == "outset" ? 'selected' : ''; ?>>
											<?= __("Outset", $this->plugin_name); ?>
                                        </option>
                                        <option value="none" <?= $options['border_style'] == "none" ? 'selected' : ''; ?>>
											<?= __("None", $this->plugin_name); ?>
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for='ays-poll-border-radius'><?= __('Border radius', $this->plugin_name); ?>
                                        <a class="ays_help" data-toggle="tooltip" data-placement="top"
                                           title="<?= __("The radius of the border", $this->plugin_name); ?>">
                                            <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                                        </a>
                                    </label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="number" min="0"
                                           class="ays-text-input ays-text-input-short"
                                           id='ays-poll-border-radius' name='ays_poll_border_radius'
                                           value="<?= (isset($options['border_radius']) && $options['border_radius']) ? $options['border_radius'] : '0'; ?>"/>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for='ays-poll-border-width'><?= __('Border width', $this->plugin_name); ?>
                                        <a class="ays_help" data-toggle="tooltip" data-placement="top"
                                           title="<?= __("The width of the border", $this->plugin_name); ?>">
                                            <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                                        </a>
                                    </label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="number" min="0"
                                           class="ays-text-input ays-text-input-short"
                                           id='ays-poll-border-width' name='ays_poll_border_width'
                                           value="<?= isset($options['border_width']) &&  $options['border_width'] != '' ? $options['border_width'] : 2; ?>"/>
                                </div>
                            </div>                            
                            <hr>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for='ays-poll-box-shadow-color'><?= __('Box shadow', $this->plugin_name); ?>
                                        <a class="ays_help" data-toggle="tooltip" data-placement="top"
                                           title="<?= __("The style of box shadow", $this->plugin_name); ?>">
                                            <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                                        </a>
                                    </label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="checkbox" class="ays_toggle ays_toggle_slide"
                                           id="ays_poll_enable_box_shadow"
                                           name="ays_poll_enable_box_shadow" <?= (isset($options['enable_box_shadow']) && $options['enable_box_shadow'] == 'on') ? 'checked' : ''; ?>>
                                    <label for="ays_poll_enable_box_shadow" class="ays_switch_toggle">Toggle</label>
                                    <div class="col-sm-12 ays_toggle_target ays_divider_top"
                                         style="margin-top: 10px; padding-top: 10px; <?= (isset($options['enable_box_shadow']) && !empty($options['enable_box_shadow'])) ? '' : 'display:none;' ?>">
                                        <label for="ays-poll-box-shadow-color">
											<?= __('Box shadow color', $this->plugin_name) ?>
                                            <a class="ays_help" data-toggle="tooltip"
                                               title="--><?= __('The shadow color of Poll container', $this->plugin_name) ?>">
                                                <i class="ays_fa ays_fa_info_circle"></i>
                                            </a>
                                        </label>
                                        <input type="text" class="ays-shadow-input" data-alpha="true" id='ays-poll-box-shadow-color'
                                               name='ays_poll_box_shadow_color'
                                               value="<?= (isset($options['box_shadow_color']) && !empty($options['box_shadow_color'])) ? $options['box_shadow_color'] : '#000000'; ?>"/>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <!-- ---------Aro start gradient -->
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="ays-enable-background-gradient">
                                        <?php echo __('Background Gradient',$this->plugin_name)?>
                                        <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Color gradient of the poll background',$this->plugin_name)?>">
                                            <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                                        </a>
                                    </label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="checkbox" class="ays_toggle ays_toggle_slide"
                                           id="ays-enable-background-gradient"
                                           name="ays_enable_background_gradient"
                                            <?php echo ($enable_background_gradient) ? 'checked' : ''; ?>/>
                                    <label for="ays-enable-background-gradient" class="ays_switch_toggle">Toggle</label>
                                    <div class="row ays_toggle_target" style="margin: 10px 0 0 0; padding-top: 10px; <?php echo ($enable_background_gradient) ? '' : 'display:none;' ?>">
                                        <div class="col-sm-12 ays_divider_top" style="margin-top: 10px; padding-top: 10px;">
                                            <label for='ays-background-gradient-color-1'>
                                                <?php echo __('Color 1', $this->plugin_name); ?>
                                                <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Color 1 of the poll background gradient',$this->plugin_name)?>">
                                                    <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                                                </a>
                                            </label>
                                            <input type="text" class="ays-text-input" id='ays-background-gradient-color-1' name='ays_background_gradient_color_1' data-alpha="true" value="<?php echo $background_gradient_color_1; ?>"/>
                                        </div>
                                        <div class="col-sm-12 ays_divider_top" style="margin-top: 10px; padding-top: 10px;">
                                            <label for='ays-background-gradient-color-2'>
                                                <?php echo __('Color 2', $this->plugin_name); ?>
                                                <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Color 2 of the poll background gradient',$this->plugin_name)?>">
                                                    <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                                                </a>
                                            </label>
                                            <input type="text" class="ays-text-input" id='ays-background-gradient-color-2' name='ays_background_gradient_color_2' data-alpha="true" value="<?php echo $background_gradient_color_2; ?>"/>
                                        </div>
                                        <div class="col-sm-12 ays_divider_top" style="margin-top: 10px; padding-top: 10px;">
                                            <label for="ays_poll_gradient_direction">
                                                <?php echo __('Gradient direction',$this->plugin_name)?>
                                                <a class="ays_help" data-toggle="tooltip" title="<?php echo __('The direction of the color gradient.',$this->plugin_name)?>">
                                                    <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                                                </a>
                                            </label>
                                            <select id="ays_poll_gradient_direction" name="ays_poll_gradient_direction" class="ays-text-input">
                                                <option <?php echo ($poll_gradient_direction == 'vertical') ? 'selected' : ''; ?> value="vertical"><?php echo __( 'Vertical', $this->plugin_name); ?></option>
                                                <option <?php echo ($poll_gradient_direction == 'horizontal') ? 'selected' : ''; ?> value="horizontal"><?php echo __( 'Horizontal', $this->plugin_name); ?></option>
                                                <option <?php echo ($poll_gradient_direction == 'diagonal_left_to_right') ? 'selected' : ''; ?> value="diagonal_left_to_right"><?php echo __( 'Diagonal left to right', $this->plugin_name); ?></option>
                                                <option <?php echo ($poll_gradient_direction == 'diagonal_right_to_left') ? 'selected' : ''; ?> value="diagonal_right_to_left"><?php echo __( 'Diagonal right to left', $this->plugin_name); ?></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- -----------end -->
                            <hr/>
			                <div class="form-group row">
			                    <div class="col-sm-4">
			                        <label for="ays_disable_answer_hover">
										<?= __('Disable answers hover', $this->plugin_name); ?>
			                            <a class="ays_help" data-toggle="tooltip"
			                               data-placement="top"
			                               title="<?= __("This option disables hover effect for answers", $this->plugin_name); ?>">
			                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
			                            </a>
			                        </label>
			                    </div>
			                    <div class="col-sm-8">
			                        <input type="checkbox"
			                               name="ays_disable_answer_hover"
			                               id="ays_disable_answer_hover"
			                               value="on" <?php echo ($options['disable_answer_hover'] == 1) ? 'checked' : ''; ?>									
			                        >
			                    </div>
			                </div>
                            <hr/>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="ays_poll_custom_class">
                                        <?php echo __('Custom class for poll container',$this->plugin_name)?>
                                        <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Custom HTML class for poll container. You can use your class for adding your custom styles for poll container. Example: p{color:red !important}',$this->plugin_name)?>">
                                            <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                                        </a>
                                    </label>
                                </div>
                                <div class="col-sm-8 ">
                                    <input type="text" class="ays-text-input" name="ays_poll_custom_class" id="ays_poll_custom_class" placeholder="myClass myAnotherClass..." value="<?php echo $custom_class; ?>">
                                </div>
                            </div>
                            <hr/>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="ays_custom_css">
										<?= __("Custom CSS", $this->plugin_name); ?>
									</label>
									<a class="ays_help" 
									   data-toggle="tooltip" 
									   data-placement="top" 
									   title="<?= __("You can add your css", $this->plugin_name); ?>">
                            			<i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                        			</a><br>
                                </div>
                                <div class="col-sm-8">
                                    <textarea class="ays-textarea" id="ays_custom_css" name="ays_custom_css" cols="30"
                                              rows="10"><?= (isset($poll['custom_css'])) ? $poll['custom_css'] : ''; ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-12">
                            <style id='apm-custom-css'>
                                <?=$poll['custom_css']?>
                            </style>
							<?php
							$content = "
                            <div class='box-apm-scroll'>
                                <div class='box-apm {$poll['type']}-poll' id=''>
                                    <div class='apm-title-box'>
                                        <h5>{$poll['title']}</h5>
                                    </div>
                                    <div class='ays_question'>" . stripslashes($poll['question']) . "</div>
                                    <div class='apm-img-box'>";
							$content .= !empty($poll['image']) ? "<img class='ays-poll-img' src='{$poll['image']}'>" : "";
                            $checking_answer_hover = ($options['disable_answer_hover'] == 1) ? 'disable_hover' : 'ays_enable_hover';
							$content .= "</div>
                                    <div class='apm-answers'>";
                            $minimalTheme = ($poll['theme_id'] == 3) ? 'ays_poll_minimal_theme' : '' ;
                            $minimalThemeBtn = ($poll['theme_id'] == 3) ? 'ays_poll_minimal_theme_btn' : '' ;                            
							switch ( $poll['type'] ) {
								case 'choosing':
									foreach ( $poll['answers'] as $index => $answer ) {
										$content .= "<div class='apm-choosing answer- ".$minimalTheme." '>
                                                        <input type='radio' name='answer' id='radio-$index-' value='{$answer['id']}'>
                                                        <label class='ays_label_poll ".$checking_answer_hover."' for='radio-$index-' >" . stripcslashes($answer['answer']) . "</label>
                                                    </div>";
									}
									break;
								case 'voting':
									switch ( $poll['view_type'] ) {
										case 'hand':
											foreach ( $poll['answers'] as $index => $answer ) {
												$content .= "<div class='apm-voting answer-'><input type='radio' name='answer' id='radio-$index-' value='{$answer['id']}'>
                                                                <label for='radio-$index-'>";
												$content .= ((int) $answer['answer'] > 0 ? "<i class='ays_poll_far ays_poll_fa-thumbs-up'></i>" : "<i class='ays_poll_far ays_poll_fa-thumbs-down'></i>") . "</label></div>";
											}
											break;
										case 'emoji':
											foreach ( $poll['answers'] as $index => $answer ) {
												$content .= "<div class='apm-voting answer-'><input type='radio' name='answer' id='radio-$index-' value='{$answer['id']}'>
                                                                <label for='radio-$index-'>";
												$content .= ((int) $answer['answer'] > 0 ? $emoji[1] : $emoji[3]) . "</label></div>";
											}
											break;
										default:										
											break;
									}
									break;
								case 'rating':
									switch ( $poll['view_type'] ) {
										case 'star':
											foreach ( $poll['answers'] as $index => $answer ) {
												$content .= "<div class='apm-rating answer-'><input type='radio' name='answer' id='radio-$index-' value='{$answer['id']}'>
                                                                <label for='radio-$index-'><i class='ays_poll_far ays_poll_fa-star'></i></label></div>";
											}
											break;
										case 'emoji':
											foreach ( $poll['answers'] as $index => $answer ) {
												$content .= "<div class='apm-rating answer-'><input type='radio' name='answer' id='radio-$index-' value='{$answer['id']}'>
                                                                <label class='emoji' for='radio-$index-'>" . $emoji[(count($poll['answers']) / 2 - $index + 1.5)] . "</label></div>";
											}
											break;
										default:										
											break;
									}
									break;								
								default:										
									break;								
							}
							$content .= "</div>
                                    <div class='apm-button-box' " . (isset($options['enable_vote_btn']) && $options['enable_vote_btn'] == 0 ? "style='display:none'" : "") . ">
                                        <input type='button' name='ays_finish_poll' class='btn ays-poll-btn {$poll['type']}-btn ".$minimalThemeBtn." '" . 'value="' . ((isset($options['btn_text']) && '' != $options['btn_text']) ? stripslashes($options['btn_text']) : 'Vote') . '">
                                    </div>
                                </div>
                            </div>';
							print $content;
							?>
                        </div>
                    </div>
                </div>
                <hr>
            </div>
            <div id="tab3"
                 class="ays-poll-tab-content <?= $active_tab == 'Settings' ? 'ays-poll-tab-content-active' : ''; ?>">
                <p class="ays-subtitle">
					<?= __('Feature options', $this->plugin_name); ?>
                </p>
                <hr>
                <div class="col-sm-12" style="padding:20px 0;">
                    <div class="pro_features" style="justify-content:flex-end;">
                        <div style="margin-right:20px;">
                            <p style="font-size:20px;">
                                <?php echo __("This feature is available only in ", $this->plugin_name); ?>
                                <a href="https://ays-pro.com/wordpress/poll-maker" target="_blank" title="Developer feature"><?php echo __("Developer package!!!", $this->plugin_name); ?></a>
                            </p>
                        </div>
                    </div>
	                <div class="form-group row">
	                    <div class="col-sm-3">
	                        <label for='ays_allow_multivote'>
								<?= __('Allow multivote', $this->plugin_name); ?>
								<a  class="ays_help" 
									data-toggle="tooltip"
									data-placement="top"
									title="<?= __("Allow to vote for more then one answer", $this->plugin_name); ?>">
	                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
	                            </a></label>
	                    </div>
	                    <div class="col-sm-9">
	                        <input type="checkbox" name="ays_allow_multivote" id="ays_allow_multivote"
	                               value="on" >
	                    </div>
	                </div>
                </div>
                <hr>
                <div class="form-group row">
                    <div class="col-sm-3">
                        <label for='show-title'>
							<?= __('Show Title', $this->plugin_name); ?>
                            <a class="ays_help" data-toggle="tooltip"
                               data-placement="top"
                               title="<?= __("Show or hide the name of the poll", $this->plugin_name); ?>">
                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                            </a>
                        </label>
                    </div>
                    <div class="col-sm-9">
                        <input type="checkbox" name="show_title" id="show-title"
                               value="show" <?= $poll['show_title'] ? 'checked' : ''; ?>>
                    </div>
                </div>
                <hr>
                <div class="form-group row">
                    <div class="col-sm-3">
                        <label>
							<?= __('Direction', $this->plugin_name); ?>
                            <a class="ays_help" data-toggle="tooltip" data-placement="top"
                               title="<?= __('Content direction of the poll', $this->plugin_name) ?>">
                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                            </a>
                        </label>
                    </div>
                    <div class="col-sm-9">
                        <div class="d-flex">
                            <div class="form-check form-check-inline">
                                <input type="radio" id="apm-dir-ltr" name="ays_poll_direction"
                                       value="ltr" <?= (isset($options['poll_direction']) && $options['poll_direction'] == 'ltr') ? 'checked' : ''; ?> />
                                <label class="form-check-label"
                                       for="apm-dir-ltr"> <?= __('Left to right', $this->plugin_name); ?> </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input type="radio" id="apm-dir-center" name="ays_poll_direction"
                                       value="center" <?= (isset($options['poll_direction']) && $options['poll_direction'] == 'center') ? 'checked' : ''; ?> />
                                <label class="form-check-label"
                                       for="apm-dir-center"> <?= __('Center', $this->plugin_name); ?> </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input type="radio" id="apm-dir-rtl" name="ays_poll_direction"
                                       value="rtl" <?= (isset($options['poll_direction']) && $options['poll_direction'] == 'rtl') ? 'checked' : ''; ?> />
                                <label class="form-check-label"
                                       for="apm-dir-rtl"> <?= __('Right to left', $this->plugin_name); ?> </label>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="form-group row">
                    <div class="col-sm-3">
                        <label for='show-votes-count'>
                            <?= __('Show votes count', $this->plugin_name); ?>
                            <a class="ays_help" data-toggle="tooltip"
                               data-placement="top"
                               title="<?= __("Show or hide finish page votes count", $this->plugin_name); ?>">
                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                            </a>
                        </label>
                    </div>
                    <div class="col-sm-9">
                        <input type="checkbox" name="show_votes_count" id="show-votes-count"
                               value="1" <?= $showvotescount ? 'checked' : '' ?> >
                    </div>
                </div>
                <hr>
                <div class="form-group row">
                    <div class="col-sm-3">
                        <label for='show-poll-create-dates'>
                            <?= __('Show creation date', $this->plugin_name); ?>
                            <a class="ays_help" data-toggle="tooltip"
                               data-placement="top"
                               title="<?= __("Show creation date in poll start page", $this->plugin_name); ?>">
                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                            </a>
                        </label>
                    </div>
                    <div class="col-sm-9">
                        <input type="checkbox" name="show_poll_creation_date" id="show-poll-create-dates"
                               value="1" <?= $show_create_date ? 'checked' : '' ?> >
                    </div>
                </div>
                <hr>
                <div class="form-group row">
                    <div class="col-sm-3">
                        <label for='show-poll-author'>
                            <?= __('Show author', $this->plugin_name); ?>
                            <a class="ays_help" data-toggle="tooltip"
                               data-placement="top"
                               title="<?= __("Show author in poll start page", $this->plugin_name); ?>">
                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                            </a>
                        </label>
                    </div>
                    <div class="col-sm-9">
                        <input type="checkbox" name="show_poll_author" id="show-poll-author"
                               value="1" <?= $show_author ? 'checked' : '' ?> >
                    </div>
                </div>
                <hr>
                <div class="form-group row">
                    <div class="col-sm-3">
                        <label for='show-res-percent'>
                            <?= __('Show answer percent', $this->plugin_name); ?>
                            <a class="ays_help" data-toggle="tooltip"
                               data-placement="top"
                               title="<?= __("Show or hide finish page answer percent", $this->plugin_name); ?>">
                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                            </a>
                        </label>
                    </div>
                    <div class="col-sm-9">
                        <input type="checkbox" name="show_res_percent" id="show-res-percent"
                               value="1" <?= $show_res_percent ? 'checked' : '' ?> >
                    </div>
                </div>
                <hr>
                <div class="form-group row">
                    <div class="col-sm-3">
                        <label for="schedule_the_poll">
							<?= __('Schedule', $this->plugin_name); ?>
                            <a class="ays_help" data-toggle="tooltip" data-placement="top"
                               title="<?= __('The period of time when poll will be active', $this->plugin_name) ?>">
                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                            </a>
                        </label>
                    </div>
                    <div class="col-sm-9">
                        <div class="row">
                            <div class="col-sm-1">
                                <input  type="checkbox"
                                        id="schedule_the_poll"   
                                        class="active_date_check"
                                        name="active_date_check" <?= (isset($options['active_date_check']) && !empty($options['active_date_check'])) ? 'checked' : '' ?>>
                            </div>
                            <div class="col-sm-11 active_date"
                                style="display:  <?php echo (isset($options['active_date_check']) && $options['active_date_check'] == 'on') ? 'block' : 'none' ?>">
                                <!-- -1- -->                                 
                                 <div class="form-group">
                                     <div class="row"> 
                                        <div class="col-sm-3">
                                            <label class="form-check-label" for="ays-active"> <?php echo __('Start date:', $this->plugin_name); ?> </label>
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="input-group mb-3">
                                                <input type="text" class="ays-text-input ays-text-input-short ays_actDect" id="ays-active" name="ays-active"
                                                   value="<?php echo $activePoll; ?>" placeholder="<?php echo current_time( 'mysql' ); ?>">
                                                <div class="input-group-append">
                                                    <label for="ays-active" class="input-group-text">
                                                        <span><i class="ays_fa ays_fa_calendar"></i></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- -2- -->
                                <div class="form-group">
                                     <div class="row"> 
                                        <div class="col-sm-3">
                                            <label class="form-check-label" for="ays-deactive"> <?php echo __('End date:', $this->plugin_name); ?> </label>
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="input-group mb-3">
                                                <input type="text" class="ays-text-input ays-text-input-short ays_actDect" id="ays-deactive" name="ays-deactive"
                                                   value="<?php echo $deactivePoll; ?>" placeholder="<?php echo current_time( 'mysql' ); ?>">
                                                <div class="input-group-append">
                                                    <label for="ays-deactive" class="input-group-text">
                                                        <span><i class="ays_fa ays_fa_calendar"></i></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                                <!-- ////////// -->
                                 <hr>
                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <label for='ays_poll_show_timer'>
                                            <?= __('Show timer', $this->plugin_name); ?>
                                            <a class="ays_help" data-toggle="tooltip"
                                               data-placement="top"
                                               title="<?= __("Show the countdown or end date time in the poll.", $this->plugin_name); ?>">
                                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                                            </a>
                                        </label>
                                    </div>
                                    <div class="col-sm-1">
                                        <input type="checkbox" name="ays_poll_show_timer" id="ays_poll_show_timer"
                                               value="1" <?= $schedule_show_timer ? 'checked' : '' ?> >
                                    </div>
                                    <div class="col-sm-8">
                                    	<div class="ays_show_time" style="display:  <?php echo $schedule_show_timer ? 'block;' : 'none;'; ?>">
	                                    	<div class="d-flex">
					                            <div class="form-check form-check-inline">
					                                <input type="radio" id="show_time_countdown" name="ays_show_timer_type"
					                                       value="countdown" <?= $show_timer_type == 'countdown' ? 'checked' : ''; ?> />
					                                <label class="form-check-label"
					                                       for="show_time_countdown"> <?= __('Show countdown', $this->plugin_name); ?> </label>
					                            </div>
					                            <div class="form-check form-check-inline">
					                                <input type="radio" id="show_time_enddate" name="ays_show_timer_type"
					                                       value="enddate" <?= $show_timer_type == 'enddate' ? 'checked' : ''; ?> />
					                                <label class="form-check-label"
					                                       for="show_time_enddate"> <?= __('Show end date', $this->plugin_name); ?> </label>
					                            </div>
					                        </div>
                                		</div>
                                	</div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <div class="row">  
                                        <div class="col-sm-3">
                                             <label class="form-check-label"
                                               for="active_date_message"><?= __("Pre start message:", $this->plugin_name) ?></label>
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="editor">
                                                <?php
                                                $content   = !empty($options['active_date_message_soon']) ? stripslashes($options['active_date_message_soon']) : stripslashes($default_options['active_date_message_soon']);
                                                $editor_id = 'active_date_message_soon';
                                                $settings  = array(
                                                    'editor_height'  => '4',
                                                    'textarea_name'  => 'active_date_message_soon',
                                                    'editor_class'   => 'ays-textarea',
                                                    'media_elements' => false
                                                );
                                                wp_editor($content, $editor_id, $settings);
                                                ?>
                                            </div>
                                        </div>                                        
                                    </div>
                                </div>
                         		<hr>
                                <!-- -3- -->
                                <div class="form-group">
                                    <div class="row">  
                                        <div class="col-sm-3">
                                             <label class="form-check-label"
                                               for="active_date_message"><?= __("Expiration message:", $this->plugin_name) ?></label>
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="editor">
                                                <?php
                                                $content   = !empty($options['active_date_message']) ? stripslashes($options['active_date_message']) : stripslashes($default_options['active_date_message']);
                                                $editor_id = 'active_date_message';
                                                $settings  = array(
                                                    'editor_height'  => '4',
                                                    'textarea_name'  => 'active_date_message',
                                                    'editor_class'   => 'ays-textarea',
                                                    'media_elements' => false
                                                );
                                                wp_editor($content, $editor_id, $settings);
                                                ?>
                                            </div>
                                        </div>                                        
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <label for='show_result_btn_schedule'>
                                            <?= __('Show result button', $this->plugin_name); ?>
                                            <a class="ays_help" data-toggle="tooltip"
                                               data-placement="top"
                                               title="<?= __("Show or hide the result button after the schedule", $this->plugin_name); ?>">
                                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                                            </a>
                                        </label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="checkbox" name="show_result_btn_schedule" id="show_result_btn_schedule"
                                               value="1" <?= $showresbtnschedule ? 'checked' : '' ?> >
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="form-group row">
                    <div class="col-sm-3">
                        <label for='ays-poll-hide-results'>
							<?= __('Hide results', $this->plugin_name); ?>
							<a 	class="ays_help" 
								data-toggle="tooltip"
								data-placement="top"
								title="<?= __("Do not show voting results after the poll", $this->plugin_name); ?>">
                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                            </a>
                        </label>
                    </div>
                    <div class="col-sm-9">
                        <input type="checkbox" name="ays-poll-hide-results" id="ays-poll-hide-results"
                               value="hide" <?= isset($options['hide_results']) && $options['hide_results'] ? 'checked' : ''; ?>>
                    </div>
                </div>
                <hr>
                <div class="form-group row if-ays-poll-hide-results">
                    <div class="col-sm-3">
                        <label for='ays-poll-hide-results-msg'>
							<?= __('Message instead of results', $this->plugin_name); ?>
							<a 	class="ays_help"
								data-toggle="tooltip"
								data-placement="top"
								title="<?= __("Message that will appear instead of the votting results after the poll", $this->plugin_name); ?>">
                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                            </a></label>
                    </div>
                    <div class="col-sm-9">
						<?php
						$content   = !empty($options['hide_results_text']) ? stripslashes($options['hide_results_text']) : stripslashes($default_options['hide_results_text']);
						$editor_id = 'ays-poll-hide-results-text';
						$settings  = array(
							'editor_height'  => '4',
							'textarea_name'  => 'ays-poll-hide-results-text',
							'editor_class'   => 'ays-textarea',
							'media_elements' => false
						);
						wp_editor($content, $editor_id, $settings);
						?>
                    </div>
                </div>
                <hr class="if-ays-poll-hide-results">
                <div class="form-group row">
                    <div class="col-sm-3">
                        <label for='ays-poll-allow-not-vote'>
							<?= __('Allow not to vote', $this->plugin_name); ?>
							<a 	class="ays_help" 
								data-toggle="tooltip" 
								data-placement="top" 
								title="<?= __("Allow to abstain from voting and go directly to the results. If checked, it is impossible to hide the results.", $this->plugin_name); ?>">
                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                            </a></label>
                    </div>
                    <div class="col-sm-2">
                        <input type="checkbox" name="ays-poll-allow-not-vote" id="ays-poll-allow-not-vote"
                               value="allow" <?= isset($options['allow_not_vote']) && $options['allow_not_vote'] ? 'checked' : ''; ?>>
                    </div>
                    <div class="col-sm-3">
                        <label for="ays-poll-btn-text">
							<?= __("Results button text", $this->plugin_name); ?>
                            <a class="ays_help"
                               data-toggle="tooltip"
                               data-placement="top"
                               title="<?= __("The text of the button, that shows results", $this->plugin_name); ?>">
                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                            </a>
                        </label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="ays-text-input ays-text-input-short"
                               id="ays-poll-btn-text" name="ays_poll_res_btn_text"
                               value="<?= (isset($options['see_res_btn_text']) && '' != $options['see_res_btn_text']) ? stripslashes($options['see_res_btn_text']) : __("See Results", $this->plugin_name); ?>"/>
                    </div>
                </div>
                <hr>
                <div class="form-group row">
                    <div class="col-sm-3">
                        <label for="ays_poll_result_message">
                            <?php echo __('Result Message',$this->plugin_name)?>
                            <a class="ays_help" data-toggle="tooltip" title="<?php echo __('The message will be displayed after submitting the poll.',$this->plugin_name)?>">
                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                            </a>
                        </label>
                    </div>
                    <div class="col-sm-9">
	                    <div class="form-group row">
	                    	<div class="col-sm-12">
	                        	<input type="checkbox" name="ays_poll_result_message" id="ays_poll_result_message"
	                               value="hide" <?= isset($options['hide_result_message']) && $options['hide_result_message'] ? 'checked' : ''; ?>>
	                    	</div>
	                    	<div class="col-sm-12 if_poll_hide_result_message"  style="margin-top: 15px;">
		                        <?php
		                        $content = wpautop(stripslashes((isset($options['result_message'])) ? $options['result_message'] : ''));
		                        $editor_id = 'ays_result_message';
		                        $settings = array('editor_height' => '10', 'textarea_name' => 'ays_result_message', 'editor_class' => 'ays-textarea', 'media_elements' => false);
		                        wp_editor($content, $editor_id, $settings);
		                        ?>
		                    </div>
	                    </div>
                    </div>                    
                </div>
                <hr>
                <div class="form-group row">
                    <div class="col-sm-3">
                        <label for='ays-poll-show-social'>
							<?= __('Social share buttons', $this->plugin_name); ?>
							<a 	class="ays_help"
								data-toggle="tooltip"
								data-placement="top"
								title='<?= __("Show \"Share\" buttons of social networks after polling.", $this->plugin_name); ?>'>
                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                            </a></label>
                    </div>
                    <div class="col-sm-9">
                        <input type="checkbox" name="ays-poll-show-social" id="ays-poll-show-social"
                               value="show" <?= isset($options['show_social']) && $options['show_social'] ? 'checked' : ''; ?>>
                    </div>
                </div>
                <hr>
                <div class="form-group row">
                    <div class="col-sm-3">
                        <label for='ays-poll-load-effect'>
							<?php echo __('Results sorting', $this->plugin_name); ?>
							<a class="ays_help" data-toggle="tooltip" data-html="true" data-placement="top" title="<?php
								echo __("Select the way of arrangement of the results on the finishing page of the poll.", $this->plugin_name) .
									"<ul style='list-style-type: circle;padding-left: 20px;'>".
                                        "<li>". __('Ascending – the smallest to largest',$this->plugin_name) ."</li>".
                                        "<li>". __('Descending – the largest to smallest',$this->plugin_name) ."</li>".
                                        "<li>". __('None',$this->plugin_name) ."</li>".
                                    "</ul>";
							?>">
                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                            </a></label>
                    </div>
                    <div class="col-sm-9">
                        <select name="ays-poll-result-sort-type" id="ays-poll-result-sort-type" class="ays-select">
                            <option value="none" <?= isset($options['result_sort_type']) && $options['result_sort_type'] == "none" ? 'selected' : ''; ?>>
								<?= __("None", $this->plugin_name); ?>
                            </option>
                            <option value="ASC" <?= isset($options['result_sort_type']) && $options['result_sort_type'] == "ASC" ? 'selected' : ''; ?>>
								<?= __("Ascending", $this->plugin_name); ?>
                            </option>
                            <option value="DESC" <?= isset($options['result_sort_type']) && $options['result_sort_type'] == "DESC" ? 'selected' : ''; ?>>
								<?= __("Descending", $this->plugin_name); ?>
                            </option>
                        </select>
                    </div>
                </div>
                <hr>
                <div class="form-group row">
                    <div class="col-sm-3" style="padding-right: 0px;">
                        <label for="ays_enable_pass_count">
							<?= __('Show passed users count', $this->plugin_name) ?>
                            <a class="ays_help" data-toggle="tooltip" data-placement="top"
                               title="<?= __('Show the number of users who passed the poll.', $this->plugin_name) ?>">
                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                            </a>
                        </label>
                    </div>
                    <div class="col-sm-9">
                        <input type="checkbox" id="ays_enable_pass_count" name="ays_enable_pass_count"
                               value="on" <?= ($enable_pass_count == 'on') ? 'checked' : ''; ?> />
                    </div>
                </div>
                <hr>
                <div class="form-group row">
                    <div class="col-sm-3">
                        <label for='ays-poll-load-effect'>
							<?= __('Loading effect', $this->plugin_name); ?>
                            <a class="ays_help" data-toggle="tooltip"
                               data-placement="top"
                               title="<?= __("Loading effect before displaying poll results.", $this->plugin_name); ?>">
                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                            </a>
                        </label>
                    </div>
                    <div class="col-sm-3">
                        <select name="ays-poll-load-effect" id="ays-poll-load-effect" class="ays-select">
                            <option value="load_gif" <?= isset($options['load_effect']) && $options['load_effect'] == "load_gif" ? 'selected' : ''; ?>>
								<?= __("Loading GIF", $this->plugin_name); ?>
                            </option>
                            <option value="opacity" <?= isset($options['load_effect']) && $options['load_effect'] == "opacity" ? 'selected' : ''; ?>>
								<?= __("Opacity", $this->plugin_name); ?>
                            </option>
                            <option value="blur" <?= isset($options['load_effect']) && $options['load_effect'] == "blur" ? 'selected' : ''; ?>>
								<?= __("Blur", $this->plugin_name); ?>
                            </option>
                            <option class="apm-pro-feature" disabled
                                    title="<?= __("It is PRO version feature", $this->plugin_name); ?>" value="pro">
								<?= __("Custom GIF", $this->plugin_name); ?>
                            </option>
                        </select>
                    </div>
                    <div class="if-loading-gif col-sm-6 row">
                        <div class="apm-loader d-flex justify-content-between align-items-center">
                            <input type="radio"
                                   name="ays-poll-load-gif" <?= isset($options['load_effect']) && $options['load_effect'] == "load_gif" && isset($options['load_gif']) && $options['load_gif'] == 'plg_default' ? 'checked' : ''; ?>
                                   id="plg_default" value="plg_default">
                            <label for="plg_default" class="apm-loading-gif">
                                <div class="loader loader--style3">
                                    <svg version="1.1" id="loader-1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%"
                                         height="100%" viewBox="0 0 50 50" style="enable-background:new 0 0 50  50;"
                                         xml:space="preserve">
                                        <path fill="#000"
                                              d="M43.935,25.145c0-10.318-8.364-18.683-18.683-18.683c-10.318,       0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615c8.072,0,  14.615,   6.543,14.615,14.615H43.935z">
                                            <animateTransform attributeType="xml" attributeName="transform"
                                                              type="rotate" from="0 25 25" to="360 25 25" dur="0.7s"
                                                              repeatCount="indefinite"/>
                                        </path>
                                    </svg>
                                </div>
                            </label>
                        </div>
                        <div class="apm-loader d-flex justify-content-between align-items-center">
                            <input type="radio"
                                   name="ays-poll-load-gif" <?= isset($options['load_effect']) && $options['load_effect'] == "load_gif" && isset($options['load_gif']) && $options['load_gif'] == 'plg_1' ? 'checked' : ''; ?>
                                   id="plg_1" value="plg_1">
                            <label for="plg_1" class="apm-loading-gif">
                                <div class="loader loader--style5">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%"
                                         height="100%" viewBox="0 0 24 30" style="enable-background:new 0 0 50 50;"
                                         xml:space="preserve">
                                        <rect x="0" y="0" width="4" height="10" fill="#333">
                                            <animateTransform attributeType="xml" attributeName="transform"
                                                              type="translate" values="0 0; 0 20; 0 0" begin="0"
                                                              dur="0.8s" repeatCount="indefinite"/>
                                        </rect>
                                        <rect x="10" y="0" width="4" height="10" fill="#333">
                                            <animateTransform attributeType="xml" attributeName="transform"
                                                              type="translate" values="0 0; 0 20; 0 0" begin="0.2s"
                                                              dur="0.8s" repeatCount="indefinite"/>
                                        </rect>
                                        <rect x="20" y="0" width="4" height="10" fill="#333">
                                            <animateTransform attributeType="xml" attributeName="transform"
                                                              type="translate" values="0 0; 0 20; 0 0" begin="0.4s"
                                                              dur="0.8s" repeatCount="indefinite"/>
                                        </rect>
                                    </svg>
                                </div>
                            </label>
                        </div>
                        <div class="apm-loader d-flex justify-content-between align-items-center">
                            <input type="radio"
                                   name="ays-poll-load-gif" <?= isset($options['load_effect']) && $options['load_effect'] == "load_gif" && isset($options['load_gif']) && $options['load_gif'] == 'plg_2' ? 'checked' : ''; ?>
                                   id="plg_2" value="plg_2">
                            <label for="plg_2" class="apm-loading-gif">
                                <div class="loader loader--style8">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%"
                                         height="100%" viewBox="0 0 24 30" style="enable-background:new 0 0 50 50;"
                                         xml:space="preserve">
                                        <rect x="0" y="10" width="4" height="10" fill="#333" opacity="0.2">
                                            <animate attributeName="opacity" attributeType="XML" values="0.2; 1; .2"
                                                     begin="0s" dur="0.7s" repeatCount="indefinite"/>
                                            <animate attributeName="height" attributeType="XML" values="10; 20; 10"
                                                     begin="0s" dur="0.7s" repeatCount="indefinite"/>
                                            <animate attributeName="y" attributeType="XML" values="10; 5; 10" begin="0s"
                                                     dur="0.7s" repeatCount="indefinite"/>
                                        </rect>
                                        <rect x="8" y="10" width="4" height="10" fill="#333" opacity="0.2">
                                            <animate attributeName="opacity" attributeType="XML" values="0.2; 1; .2"
                                                     begin="0.15s" dur="0.7s" repeatCount="indefinite"/>
                                            <animate attributeName="height" attributeType="XML" values="10; 20; 10"
                                                     begin="0.15s" dur="0.7s" repeatCount="indefinite"/>
                                            <animate attributeName="y" attributeType="XML" values="10; 5; 10"
                                                     begin="0.15s" dur="0.7s" repeatCount="indefinite"/>
                                        </rect>
                                        <rect x="16" y="10" width="4" height="10" fill="#333" opacity="0.2">
                                            <animate attributeName="opacity" attributeType="XML" values="0.2; 1; .2"
                                                     begin="0.3s" dur="0.7s" repeatCount="indefinite"/>
                                            <animate attributeName="height" attributeType="XML" values="10; 20; 10"
                                                     begin="0.3s" dur="0.7s" repeatCount="indefinite"/>
                                            <animate attributeName="y" attributeType="XML" values="10; 5; 10"
                                                     begin="0.3s" dur="0.7s" repeatCount="indefinite"/>
                                        </rect>
                                    </svg>
                                </div>
                            </label>
                        </div>
                        <div class="apm-loader d-flex justify-content-between align-items-center">
                            <input type="radio"
                                   name="ays-poll-load-gif" <?= isset($options['load_effect']) && $options['load_effect'] == "load_gif" && isset($options['load_gif']) && $options['load_gif'] == 'plg_3' ? 'checked' : ''; ?>
                                   id="plg_3" value="plg_3">
                            <label for="plg_3" class="apm-loading-gif">
                                <div class="loader loader--style5">
                                    <svg width="100%" height="100%" viewBox="0 0 105 105"
                                         xmlns="http://www.w3.org/2000/svg" fill="#000">
                                        <circle cx="12.5" cy="12.5" r="12.5">
                                            <animate attributeName="fill-opacity" begin="0s" dur="0.9s" values="1;.2;1"
                                                     calcMode="linear" repeatCount="indefinite"/>
                                        </circle>
                                        <circle cx="12.5" cy="52.5" r="12.5" fill-opacity=".5">
                                            <animate attributeName="fill-opacity" begin="100ms" dur="0.9s"
                                                     values="1;.2;1" calcMode="linear" repeatCount="indefinite"/>
                                        </circle>
                                        <circle cx="52.5" cy="12.5" r="12.5">
                                            <animate attributeName="fill-opacity" begin="300ms" dur="0.9s"
                                                     values="1;.2;1" calcMode="linear" repeatCount="indefinite"/>
                                        </circle>
                                        <circle cx="52.5" cy="52.5" r="12.5">
                                            <animate attributeName="fill-opacity" begin="600ms" dur="0.9s"
                                                     values="1;.2;1" calcMode="linear" repeatCount="indefinite"/>
                                        </circle>
                                        <circle cx="92.5" cy="12.5" r="12.5">
                                            <animate attributeName="fill-opacity" begin="800ms" dur="0.9s"
                                                     values="1;.2;1" calcMode="linear" repeatCount="indefinite"/>
                                        </circle>
                                        <circle cx="92.5" cy="52.5" r="12.5">
                                            <animate attributeName="fill-opacity" begin="400ms" dur="0.9s"
                                                     values="1;.2;1" calcMode="linear" repeatCount="indefinite"/>
                                        </circle>
                                        <circle cx="12.5" cy="92.5" r="12.5">
                                            <animate attributeName="fill-opacity" begin="700ms" dur="0.9s"
                                                     values="1;.2;1" calcMode="linear" repeatCount="indefinite"/>
                                        </circle>
                                        <circle cx="52.5" cy="92.5" r="12.5">
                                            <animate attributeName="fill-opacity" begin="500ms" dur="0.9s"
                                                     values="1;.2;1" calcMode="linear" repeatCount="indefinite"/>
                                        </circle>
                                        <circle cx="92.5" cy="92.5" r="12.5">
                                            <animate attributeName="fill-opacity" begin="200ms" dur="0.9s"
                                                     values="1;.2;1" calcMode="linear" repeatCount="indefinite"/>
                                        </circle>
                                    </svg>
                                </div>
                            </label>
                        </div>
                        <div class="apm-loader d-flex justify-content-between align-items-center">
                            <input type="radio"
                                   name="ays-poll-load-gif" <?= isset($options['load_effect']) && $options['load_effect'] == "load_gif" && isset($options['load_gif']) && $options['load_gif'] == 'plg_4' ? 'checked' : ''; ?>
                                   id="plg_4" value="plg_4">
                            <label for="plg_4" class="apm-loading-gif">
                                <div class="loader loader--style4">
                                    <svg width="100%" height="100%" viewBox="0 0 57 57"
                                         xmlns="http://www.w3.org/2000/svg" stroke="#000">
                                        <g fill="none" fill-rule="evenodd">
                                            <g transform="translate(1 1)" stroke-width="2">
                                                <circle cx="5" cy="50" r="5">
                                                    <animate attributeName="cy" begin="0s" dur="2.2s"
                                                             values="50;5;50;50" calcMode="linear"
                                                             repeatCount="indefinite"/>
                                                    <animate attributeName="cx" begin="0s" dur="2.2s" values="5;27;49;5"
                                                             calcMode="linear" repeatCount="indefinite"/>
                                                </circle>
                                                <circle cx="27" cy="5" r="5">
                                                    <animate attributeName="cy" begin="0s" dur="2.2s" from="5" to="5"
                                                             values="5;50;50;5" calcMode="linear"
                                                             repeatCount="indefinite"/>
                                                    <animate attributeName="cx" begin="0s" dur="2.2s" from="27" to="27"
                                                             values="27;49;5;27" calcMode="linear"
                                                             repeatCount="indefinite"/>
                                                </circle>
                                                <circle cx="49" cy="50" r="5">
                                                    <animate attributeName="cy" begin="0s" dur="2.2s"
                                                             values="50;50;5;50" calcMode="linear"
                                                             repeatCount="indefinite"/>
                                                    <animate attributeName="cx" from="49" to="49" begin="0s" dur="2.2s"
                                                             values="49;5;27;49" calcMode="linear"
                                                             repeatCount="indefinite"/>
                                                </circle>
                                            </g>
                                        </g>
                                    </svg>
                                </div>
                            </label>
                        </div>
                        <div class="apm-loader d-flex justify-content-between align-items-center apm-pro-feature">
                            <input type="radio" disabled id="loaderp" value="pro">
                            <label for="loaderp" class="apm-loading-gif"
                                   title="<?= __("It is PRO feature", $this->plugin_name) ?>">
                                <div class="loader loader--style4">
                                    <svg width="100%" height="100%" viewBox="0 0 135 140"
                                         xmlns="http://www.w3.org/2000/svg" fill="#000">
                                        <rect y="10" width="15" height="120" rx="6">
                                            <animate attributeName="height" begin="0.5s" dur="1s"
                                                     values="120;110;100;90;80;70;60;50;40;140;120" calcMode="linear"
                                                     repeatCount="indefinite"/>
                                            <animate attributeName="y" begin="0.5s" dur="1s"
                                                     values="10;15;20;25;30;35;40;45;50;0;10" calcMode="linear"
                                                     repeatCount="indefinite"/>
                                        </rect>
                                        <rect x="30" y="10" width="15" height="120" rx="6">
                                            <animate attributeName="height" begin="0.25s" dur="1s"
                                                     values="120;110;100;90;80;70;60;50;40;140;120" calcMode="linear"
                                                     repeatCount="indefinite"/>
                                            <animate attributeName="y" begin="0.25s" dur="1s"
                                                     values="10;15;20;25;30;35;40;45;50;0;10" calcMode="linear"
                                                     repeatCount="indefinite"/>
                                        </rect>
                                        <rect x="60" width="15" height="140" rx="6">
                                            <animate attributeName="height" begin="0s" dur="1s"
                                                     values="120;110;100;90;80;70;60;50;40;140;120" calcMode="linear"
                                                     repeatCount="indefinite"/>
                                            <animate attributeName="y" begin="0s" dur="1s"
                                                     values="10;15;20;25;30;35;40;45;50;0;10" calcMode="linear"
                                                     repeatCount="indefinite"/>
                                        </rect>
                                        <rect x="90" y="10" width="15" height="120" rx="6">
                                            <animate attributeName="height" begin="0.25s" dur="1s"
                                                     values="120;110;100;90;80;70;60;50;40;140;120" calcMode="linear"
                                                     repeatCount="indefinite"/>
                                            <animate attributeName="y" begin="0.25s" dur="1s"
                                                     values="10;15;20;25;30;35;40;45;50;0;10" calcMode="linear"
                                                     repeatCount="indefinite"/>
                                        </rect>
                                        <rect x="120" y="10" width="15" height="120" rx="6">
                                            <animate attributeName="height" begin="0.5s" dur="1s"
                                                     values="120;110;100;90;80;70;60;50;40;140;120" calcMode="linear"
                                                     repeatCount="indefinite"/>
                                            <animate attributeName="y" begin="0.5s" dur="1s"
                                                     values="10;15;20;25;30;35;40;45;50;0;10" calcMode="linear"
                                                     repeatCount="indefinite"/>
                                        </rect>
                                    </svg>
                                </div>
                            </label>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="form-group row">
                    <div class="col-sm-3">
                        <label for="ays_notify_by_email_on">
							<?= __('Results notification by e-mail', $this->plugin_name); ?>
                            <a class="ays_help" data-toggle="tooltip" data-placement="top"
                               title="<?= __('If this option is on you will receive e-mail notification about votes.', $this->plugin_name); ?>">
                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                            </a>
                        </label>
                    </div>
                    <div class="col-sm-3">
                        <input type="checkbox" id="ays_notify_by_email_on" name="ays_notify_by_email_on"
                               value="on" <?= ((isset($options['notify_email_on']) && $options['notify_email_on'] == 1)) ? 'checked' : ''; ?> />
                    </div>
                    <div class="if-notify-email col-sm-6">
                        <div class="row">
                            <div class="col-sm-4">
                                <label for="ays_notify_email">
									<?= __('E-mail', $this->plugin_name); ?>
                                    <a class="ays_help" data-toggle="tooltip" data-placement="top"
                                       title="<?= __('If you want set another e-mail, enter it here. Or leave blank for apply admin e-mail.', $this->plugin_name); ?>">
                                        <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                                    </a>
                                </label>
                            </div>
                            <div class="col-sm-8">
                                <input type="email" name="ays_notify_email" id="ays_notify_email"
                                       value="<?= isset($options['notify_email']) && !empty($options['notify_email']) ? $options['notify_email'] : get_option('admin_email'); ?>"
                                       size="30">
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="form-group row">
                    <div class="col-sm-3">
                        <label for="ays_redirect_after_vote">
							<?= __('Redirect after voting', $this->plugin_name); ?>
                            <a class="ays_help" data-toggle="tooltip" data-placement="top"
                               title="<?= __('If you want to redirect users after voting.', $this->plugin_name); ?>">
                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                            </a>
                        </label>
                    </div>
                    <div class="col-sm-1">
                        <input type="checkbox" id="ays_redirect_after_vote" name="ays_redirect_after_vote"
                               value="on" <?= ((isset($options['redirect_users']) && $options['redirect_users'] == 1)) ? 'checked' : ''; ?> />
                    </div>
                    <div class="if-redirect-after-vote col-sm-8">
                        <div class="row">
                            <div class="col-sm-2">
                                <label for="redirection_url">
									<?= __('URL', $this->plugin_name); ?>
                                    <a class="ays_help" data-toggle="tooltip" data-placement="top"
                                       title="<?= __('Redirection URL', $this->plugin_name); ?>">
                                        <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                                    </a>
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <input type="text" name="redirection_url" placeholder="https://www.google.com"
                                       id="redirection_url"
                                       value="<?= isset($options['redirect_after_vote_url']) && !empty($options['redirect_after_vote_url']) ? $options['redirect_after_vote_url'] : ""; ?>"
                                       size="25">
                            </div>
                            <div class="col-sm-2">
                                <label for="redirectio_delay">
									<?= __('Delay', $this->plugin_name); ?>
                                    <a class="ays_help" data-toggle="tooltip" data-placement="top"
                                       title="<?= __('Redirection delay (sec)', $this->plugin_name); ?>">
                                        <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                                    </a>
                                </label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="redirection_delay" id="redirectio_delay"
                                       value="<?= isset($options['redirect_after_vote_delay']) && !empty($options['redirect_after_vote_delay']) ? $options['redirect_after_vote_delay'] : ""; ?>"
                                       size="15">
                            </div>
                        </div>
                    </div>
                </div>
                <hr/>
                <div class="form-group row">
                    <div class="col-sm-3">
                        <label for="randomize-answers">
							<?= __('Randomize Answers', $this->plugin_name); ?>
                            <a class="ays_help" data-toggle="tooltip"
                               data-placement="top"
                               title="<?= __("Randomize answers of the poll", $this->plugin_name); ?>">
                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                            </a>
                        </label>
                    </div>
                    <div class="col-sm-9">
                        <input type="checkbox"
                               name="randomize_answers"
                               id="randomize-answers"
                               value="on"
							<?php echo $randomize_answers ? 'checked' : ''; ?>
                        />
                    </div>
                </div>
                <hr/>
                <div class="form-group row ays_toggle_parent">
                    <div class="col-sm-3" style="padding-right: 0px;">
                        <label for="ays_enable_asnwers_sound">
                            <?php echo __('Enable answers sound',$this->plugin_name)?>
                            <a class="ays_help" data-toggle="tooltip" title="<?php echo __('This option will work only when Enable sounds are selected from the General options page.',$this->plugin_name)?>">
                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                            </a>
                        </label>
                    </div>
                    <div class="col-sm-1">
                        <input type="checkbox" id="ays_enable_asnwers_sound"
                               name="ays_poll_enable_asnwers_sound" class="ays_toggle_checkbox"
                               value="on" <?php echo $enable_asnwers_sound ? 'checked' : ''; ?>/>
                    </div>
                    <div class="col-sm-8 if_answer_sound ays_toggle_target ays_divider_left" style="<?php echo $enable_asnwers_sound ? '' : 'display:none;' ?>">
                        <?php if($answers_sound_status): ?>
                        <blockquote class=""><?php echo __('Sound are selected. For change sounds go to', $this->plugin_name); ?> <a href="?page=poll-maker-ays-settings" target="_blank"><?php echo __('General options', $this->plugin_name); ?></a> <?php echo __('page', $this->plugin_name); ?></blockquote>
                        <?php else: ?>
                        <blockquote class=""><?php echo __('Sound are not selected. For selecting sounds go to', $this->plugin_name); ?> <a href="?page=poll-maker-ays-settings" target="_blank"><?php echo __('General options', $this->plugin_name); ?></a> <?php echo __('page', $this->plugin_name); ?></blockquote>
                        <?php endif; ?>
                    </div>
                </div>
                <hr/>
                <div class="col-sm-12" style="padding:20px 0;">
                    <div class="pro_features" style="justify-content:flex-end;">
                        <div style="margin-right:20px;">
                            <p style="font-size:20px;">
                                <?php echo __("This feature is available only in ", $this->plugin_name); ?>
                                <a href="https://ays-pro.com/wordpress/poll-maker" target="_blank" title="Developer feature"><?php echo __("Developer package!!!", $this->plugin_name); ?></a>
                            </p>
                        </div>
                    </div>
	                <div class="form-group row">
	                    <div class="col-3">
	                        <label for="apm_allow_add_answers">
								<?= __('Results bar in RGBA', $this->plugin_name); ?>
	                            <a class="ays_help" data-toggle="tooltip" data-placement="top"
	                               title="<?= __("The opacity of the result bar color will depend on the number of votes. Not work for 'Coffee Fluid' theme choosing list view poll.", $this->plugin_name); ?>">
	                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
	                            </a>
	                        </label>
	                    </div>
	                    <div class="col-9">
	                        <input type="checkbox" name="" id="apm_allow_add_answers" disabled>
	                    </div>	                    
	                </div>
	                <hr/>
	                <div class="form-group row">
	                    <div class="col-3">
	                        <label for="apm_allow_add_answers">
								<?= __('Show results by', $this->plugin_name); ?>
	                            <a class="ays_help" data-toggle="tooltip" data-placement="top"
	                               title="<?= __("Show the results by Standard or Pie Chart views.", $this->plugin_name); ?>">
	                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
	                            </a>
	                        </label>
	                    </div>
	                    <div class="col-sm-9">
	                       <div class="d-flex">
	                            <div class="form-check form-check-inline">
	                                <input type="radio" id="ays_poll_show_res_standart" name="ays_poll_show_result_view"
	                                       value="standart" checked/>
	                                <label class="form-check-label ays_poll_check_label"
	                                       for="ays_poll_show_res_standart"> <?= __('Standart', $this->plugin_name); ?> </label>
	                            </div>
	                            <div class="form-check form-check-inline">
	                                <input type="radio" id="ays_poll_show_res_pie_chart" name="ays_poll_show_result_view"
	                                       value="pie_chart" />
	                                <label class="form-check-label ays_poll_check_label"
	                                       for="ays_poll_show_res_pie_chart"> <?= __('Pie Chart', $this->plugin_name); ?> </label>
	                            </div>
	                        </div>
	                    </div>	                    
	                </div>
	                <hr/>
	                <div class="form-group row">
	                    <div class="col-3">
	                        <label for="apm_allow_add_answers">
								<?= __('Vote Reason', $this->plugin_name); ?>
	                            <a class="ays_help" data-toggle="tooltip" data-placement="top"
	                               title="<?= __("Allow users to add their vote reason", $this->plugin_name); ?>">
	                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
	                            </a>
	                        </label>
	                    </div>
	                    <div class="col-9">
	                        <input type="checkbox" name="" id="apm_allow_add_answers" disabled>
	                    </div>	                    
	                </div>
                </div>
                <hr/>
                <div class="form-group row">
                    <div class="col-sm-3">
                        <label for="ays_enable_restart_button">
							<?= __('Enable restart button', $this->plugin_name) ?>
                            <a class="ays_help" data-toggle="tooltip"
                               title="<?= __('For restarting poll and pass it again.', $this->plugin_name) ?>">
                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                            </a>
                        </label>
                    </div>
                    <div class="col-sm-1">
                        <input type="checkbox" class="ays-enable-timer1" id="ays_enable_restart_button"
                               name="ays_enable_restart_button"
                               value="on" <?= (isset($options['enable_restart_button']) && $options['enable_restart_button']) ? 'checked' : '' ?> />
                    </div>
                </div>
                <hr>
                <div class="form-group row">
                    <div class="col-sm-3">
                        <label for="ays_enable_vote_button">
							<?= __('Enable Vote button', $this->plugin_name) ?>
                            <a class="ays_help" data-toggle="tooltip"
                               title="<?= __('Poll submit button.', $this->plugin_name) ?>">
                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                            </a>
                        </label>
                    </div>
                    <div class="col-sm-2">
                        <input type="checkbox" id="ays_enable_vote_button" name="ays_enable_vote_button"
                               value="1" <?= (isset($options['enable_vote_btn']) && $options['enable_vote_btn'] == 0) ? '' : 'checked' ?> />
                    </div>
                    <div class="col-sm-3">
                        <label for="ays-poll-btn-text_vote">
							<?= __("Vote button text", $this->plugin_name); ?>
                            <a class="ays_help"
                               data-toggle="tooltip"
                               data-placement="top"
                               title="<?= __("The text of the vote button", $this->plugin_name); ?>">
                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                            </a>
                        </label>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="ays-text-input ays-text-input-short"
                               id="ays-poll-btn-text_vote" name="ays_poll_btn_text"
                               value="<?= stripslashes($options['btn_text']) ?>"/>
                    </div>
                </div>
                <hr>
            </div>
            <div id="tab4"
                 class="ays-poll-tab-content <?= $active_tab == 'Limitations' ? 'ays-poll-tab-content-active' : ''; ?>">
                <p class="ays-subtitle">
					<?= __('Limitation of Users', $this->plugin_name); ?>
                </p>
                <hr/>
                <div class="form-group row">
                    <div class="col-sm-3">
                        <label for="apm_limit_users">
							<?= __('Limit users to rate only once', $this->plugin_name); ?>
                            <a class="ays_help" data-toggle="tooltip" data-placement="top"
                               title="<?= __('This option allows to block the users who have already voted.', $this->plugin_name); ?>">
                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                            </a>
                        </label>
                    </div>
                    <div class="col-sm-2">
                        <input type="checkbox" id="apm_limit_users" name="apm_limit_users"
                               value="on" <?= $options['limit_users'] ? 'checked' : ''; ?> />
                    </div>
                    <div class="if-limit-users col-sm-7">
                        <div class="ays-limitation-options">
                            <!-- Limitation method -->
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label for="ays_redirect_method">
				                        <?= __('Method', $this->plugin_name); ?></label>
                                </div>
                                <div class="col-sm-9">
                                    <div class="ays-poll-sel-fields d-flex p-0">
                                        <div class="ays-poll-check-box mr-2">
                                            <input type="radio" id="ays_limit_method_ip" name="ays_limit_method"
                                                   value="ip" <?= (!empty($options['limit_users_method']) && $options['limit_users_method'] == 'ip') || empty($options['limit_users_method']) ? "checked" : "" ?> />
                                            <label class="form-check-label"
                                                   for="ays_limit_method_ip"><?= __('By IP', $this->plugin_name); ?> </label>
                                        </div>
                                        <div class="ays-poll-check-box mr-2">
                                            <input type="radio" id="ays_limit_method_user" name="ays_limit_method"
                                                   value="user" <?= !empty($options['limit_users_method']) && $options['limit_users_method'] == 'user' ? "checked" : "" ?> />
                                            <label class="form-check-label"
                                                   for="ays_limit_method_user"><?= __('By User ID', $this->plugin_name); ?> </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Limitation message -->
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label for="ays_limitation_message">
										<?= __('Message', $this->plugin_name); ?>
                                        <a class="ays_help" data-toggle="tooltip" data-placement="top"
                                           title="<?= __('Message for voted users.', $this->plugin_name); ?>">
                                            <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                                        </a>
                                    </label>
                                </div>
                                <div class="col-sm-9">
									<?php
									$content   = !empty($options['limitation_message']) ? stripslashes($options['limitation_message']) : stripslashes($default_options['limitation_message']);
									$editor_id = 'ays_limitation_message';
									$settings  = array(
										'editor_height'  => '4',
										'textarea_name'  => 'ays_limitation_message',
										'editor_class'   => 'ays-textarea',
										'media_elements' => false
									);
									wp_editor($content, $editor_id, $settings);
									?>
                                </div>
                            </div>
                            <!-- Limitation redirect url -->
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label for="ays_redirect_url">
										<?= __('Redirect URL', $this->plugin_name); ?></label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" name="ays_redirect_url" id="ays_redirect_url"
                                           class="ays-text-input"
                                           value="<?= $options['redirect_url'] ?>"/>
                                </div>
                            </div>
                            <!-- Limitation redirect delay -->
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label for="ays_redirection_delay">
										<?= __('Redirect delay (sec)', $this->plugin_name); ?></label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="number" name="ays_redirection_delay" id="ays_redirection_delay"
                                           class="ays-text-input"
                                           value="<?= $options['redirection_delay'] ?>"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr/>
                <div class="form-group row">
                    <div class="col-sm-3">
                        <label for="ays_enable_logged_users">
							<?= __('Only for logged in users', $this->plugin_name); ?>
                            <a class="ays_help" data-toggle="tooltip" data-placement="top"
                               title="<?= __('In case of switching this option the poll can be passed only by logged in users.', $this->plugin_name); ?>">
                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                            </a>
                        </label>
                    </div>
                    <div class="col-sm-1">
                        <input type="checkbox" id="ays_enable_logged_users" name="ays_enable_logged_users"
                               value="on" <?= $options['enable_logged_users'] == 1 ? 'checked' : ''; ?> />
                    </div>
                    <div class="if-logged-in col-sm-8">
                        <div class="form-group row">
                            <div class="col-sm-3">
                                <label for="ays_logged_in_message">
									<?= __('Message', $this->plugin_name); ?>
                                    <a class="ays_help" data-toggle="tooltip" data-placement="top"
                                       title="<?= __('Message for unauthorized users.', $this->plugin_name); ?>">
                                        <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                                    </a>
                                </label>
                            </div>
                            <div class="col-sm-9">
								<?php
								$content   = !empty($options['enable_logged_users_message']) ? stripslashes($options['enable_logged_users_message']) : $default_options['enable_logged_users_message'];
								$editor_id = 'ays_logged_in_message';
								$settings  = array(
									'editor_height'  => '4',
									'textarea_name'  => 'ays_enable_logged_users_message',
									'editor_class'   => 'ays-textarea',
									'media_elements' => false
								);
								wp_editor($content, $editor_id, $settings);
								?>
                            </div>
                        </div>
                        <hr/>                        
                        <div class="form-group row">
                            <div class="col-sm-3">
                                <label for="ays_show_login_form">
                                    <?php echo __('Show Login form',$this->plugin_name)?>
                                    <a class="ays_help" data-toggle="tooltip" title="<?php echo __('Show the Login form bottom of the message for not logged in users.',$this->plugin_name)?>">
                                        <i class="ays_fa ays_fa_info_circle"></i>
                                    </a>
                                </label>
                            </div>
                            <div class="col-sm-9">
                                <input type="checkbox" class="ays-enable-timer1" id="ays_show_login_form" name="ays_show_login_form" value="on" <?php echo ($show_login_form && $options['enable_logged_users'] == 1) ? 'checked' : ''; ?>/>
                            </div>
                        </div>
                    </div>
                </div>
                <hr/>
                <div class="form-group row">
                    <div class="col-sm-3">
                        <label for="ays_enable_restriction_pass">
							<?= __('Only for selected user role', $this->plugin_name); ?>
                            <a class="ays_help" data-toggle="tooltip" data-placement="top"
                               title="<?= __('In case of switching this option the poll can be passed only by users with selected role.', $this->plugin_name); ?>">
                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                            </a>
                        </label>
                    </div>
                    <div class="col-sm-1">
                        <input type="checkbox" id="ays_enable_restriction_pass" name="ays_enable_restriction_pass"
                               value="on" <?= (isset($options['enable_restriction_pass']) &&
						                       $options['enable_restriction_pass'] == 1) ? 'checked' : ''; ?> />
                    </div>
                    <div class="if-users-roles col-sm-8">
                        <div class="form-group row">
                            <div class="col-sm-3">
                                <label for="ays_users_roles">
									<?= __('User role', $this->plugin_name); ?></label>
                            </div>
                            <div class="col-sm-9">
                                <select name="ays_users_roles[]" id="ays_users_roles" class="ays-select" multiple>
									<?php
									foreach ($ays_users_roles as $key => $user_role) {
		                                $selected_role = "";
		                                if(is_array($users_role)){
		                                    if(in_array($user_role['name'], $users_role)){
		                                        $selected_role = 'selected';
		                                    }else{
		                                        $selected_role = '';
		                                    }
		                                }else{
		                                    if($users_role == $user_role['name']){
		                                        $selected_role = 'selected';
		                                    }else{
		                                        $selected_role = '';
		                                    }
		                                }
		                                echo "<option value='" . $user_role['name'] . "' " . $selected_role . ">" . $user_role['name'] . "</option>";
		                            }
									?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-3">
                                <label for="restriction_pass_message">
									<?= __('Message', $this->plugin_name); ?>
                                    <a class="ays_help" data-toggle="tooltip" data-placement="top"
                                       title="<?= __('Message for users with not allowed role.', $this->plugin_name); ?>">
                                        <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
                                    </a>
                                </label>
                            </div>
                            <div class="col-sm-9">
								<?php
								$content   = !empty($options['restriction_pass_message']) ? stripslashes($options['restriction_pass_message']) : stripslashes($default_options['restriction_pass_message']);
								$editor_id = 'restriction_pass_message';
								$settings  = array(
									'editor_height'  => '4',
									'textarea_name'  => 'restriction_pass_message',
									'editor_class'   => 'ays-textarea',
									'media_elements' => false
								);
								wp_editor($content, $editor_id, $settings);
								?>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
            </div>
            <div id="tab5"
                 class="ays-poll-tab-content <?= $active_tab == 'Userdata' ? 'ays-poll-tab-content-active' : ''; ?>">
                <p class="ays-subtitle">
					<?= __('User Data Settings', $this->plugin_name); ?>
                </p>
                <hr/>
                <div class="form-group row">
                    <div class="col-sm-3">
                        <label>
							<?= __('Information form title', $this->plugin_name); ?>
                            <a class="ays_help" data-toggle="tooltip" data-placement="top"
                               title="<?= __("The information form heading text", $this->plugin_name); ?>">
                                <i class="ays_poll_fas ays_fa-info-circle"></i>
                            </a>
                        </label>
                    </div>
                    <div class="col-sm-9">
						<?php
						$content   = stripslashes($options['info_form_title']);
						$editor_id = 'ays-poll-info-form-text';
						$settings  = array(
							'editor_height'  => '2',
							'textarea_name'  => 'ays-poll-info-form-text',
							'editor_class'   => 'ays-textarea',
							'media_elements' => false
						);
						wp_editor($content, $editor_id, $settings);
						?>
                    </div>
                </div>
                <hr>
                <div class="form-group row">
                    <div class="col-sm-3">
                        <label for='ays_poll_info_form'>
							<?= __('Enable Information Form', $this->plugin_name); ?>
                            <a class="ays_help" data-toggle="tooltip"
                               data-placement="top"
                               title="<?= __("If you enable information form then after the poll, the user will have to fill out the form", $this->plugin_name); ?>">
                                <i class="ays_poll_fas ays_fa-info-circle"></i>
                            </a>
                        </label>
                        <input type="checkbox" name="ays_poll_info_form" id="ays_poll_info_form"
                               value="on" <?= $options['info_form'] == 1 ? 'checked' : ''; ?>>
                    </div>
                    <div class="form-group col-sm-9"
                         style="border-left: 1px solid #ccc; <?php echo $options['info_form'] == 1 ? "display: block;" : "display: none;"; ?>">
                        <div>
                            <label>
	                            <?= __('Form Fields', $this->plugin_name); ?>
                                <a class="ays_help" data-toggle="tooltip" data-placement="top"
                                   title="<?= __("Select which fields the user should fill out.", $this->plugin_name); ?>">
                                    <i class="ays_poll_fas ays_fa-info-circle"></i>
                                </a>
                            </label>
                            <hr>
                        </div>
                        <div>
                            <div class="ays-poll-sel-fields d-flex">
	                            <?php foreach ( $all_fields as $field ): ?>
                                    <div class="ays-poll-check-box mr-2">
                                        <input type="checkbox" name="ays-poll-form-fields[]"
                                               value="<?= $field['slug']; ?>"
                                               id="ays-poll-form-field-<?= $field['slug']; ?>"
		                                    <?= (array_search($field['slug'], $fields) !== false) ? "checked" : ""; ?>>
                                        <label for="ays-poll-form-field-<?= $field['slug']; ?>">
	                                        <?= ucfirst($field['name']); ?></label>
                                    </div>
	                            <?php endforeach; ?>
                            </div>
                            <hr>
                        </div>
                        <div>
                            <label>
	                            <?= __('Required Fields', $this->plugin_name); ?>
                                <a class="ays_help" data-toggle="tooltip" data-placement="top"
                                   title="<?= __("Select which fields are required.", $this->plugin_name); ?>">
                                    <i class="ays_poll_fas ays_fa-info-circle"></i>
                                </a>
                            </label>
                            <hr>
                        </div>
                        <div>
                            <div class="ays-poll-req-fields d-flex">
	                            <?php foreach ( $all_fields as $field ): ?>
                                    <div class="ays-poll-check-box mr-2"
                                         id="ays-poll-box-rfield-<?= $field['slug']; ?>" <?= (array_search($field['slug'],
		                                    $fields) === false) ? "style='display:none'" : ""; ?>>
                                        <input type="checkbox" name="ays-poll-form-required-fields[]"
                                               value="<?= $field['slug']; ?>"
                                               id="ays-poll-form-rfield-<?= $field['slug']; ?>" <?= (array_search($field['slug'],
		                                        $required_fields) !== false) ? "checked" : ""; ?>>
                                        <label for="ays-poll-form-rfield-<?= $field['slug']; ?>">
	                                        <?= ucfirst($field['name']); ?></label>
                                    </div>
	                            <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>
                <hr>

                <div class="form-group row">
                	<div class="col-sm-12" style="padding:20px;">
	                    <div class="pro_features" style="justify-content:flex-end;">
	                        <div style="margin-right:20px;">
	                            <p style="font-size:20px;">
	                                <?php echo __("This feature is available only in ", $this->plugin_name); ?>
	                                <a href="https://ays-pro.com/wordpress/poll-maker" target="_blank" title="Developer feature"><?php echo __("Developer package!!!", $this->plugin_name); ?></a>
	                            </p>
	                        </div>
	                    </div>
	                    <div class="form-group row">
		                    <div class="col-sm-3">
		                        <label>
									<?= __('Add custom fields', $this->plugin_name); ?>
		                        </label>
		                    </div>
		                    <div class="col-sm-9">
		                        <blockquote>
									<?= __("For creating custom fields click ", $this->plugin_name); ?>
		                            <a href="?page=<?= $this->plugin_name; ?>-poll-attributes"><?= __("here", $this->plugin_name); ?></a>
		                        </blockquote>
		                    </div>
		                </div>
	                </div>
                </div>
                <hr>
            </div>
            <div id="tab6"
                 class="ays-poll-tab-content <?= $active_tab == 'Email' ? 'ays-poll-tab-content-active' : ''; ?>">                 
                <p class="ays-subtitle">
						<?= __('Email settings', $this->plugin_name); ?>
	                </p>
	                <hr>
                <div class="col-sm-12" style="padding:20px;">
                    <div class="pro_features" style="justify-content:flex-end;">
                        <div style="margin-right:20px;">
                            <p style="font-size:20px;">
                                <?php echo __("This feature is available only in ", $this->plugin_name); ?>
                                <a href="https://ays-pro.com/wordpress/poll-maker" target="_blank" title="Developer feature"><?php echo __("Developer package!!!", $this->plugin_name); ?></a>
                            </p>
                        </div>
                    </div>
	                
	                <hr/>
	                <div class="form-group row">
	                    <div class="col-sm-3">
	                        <label for="ays_notify_by_email_on">
								<?= __('Results notification by e-mail', $this->plugin_name); ?>
	                            <a class="ays_help" data-toggle="tooltip" data-placement="top"
	                               title="<?= __('If this option is on you will receive e-mail notification about votes.', $this->plugin_name); ?>">
	                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
	                            </a>
	                        </label>
	                    </div>
	                    <div class="col-sm-1">
	                        <input type="checkbox" id="ays_notify_by_email_on" name="ays_notify_by_email_on" value="on" checked />
	                    </div>
	                    <div class="if-notify-email col-sm-8" style="display: flex;">
	                        <div class="form-group row">
	                            <div class="col-sm-3">
	                                <label for="ays_notify_email">
										<?= __('E-mail', $this->plugin_name); ?>
	                                    <a class="ays_help" data-toggle="tooltip" data-placement="top"
	                                       title="<?= __('If you want set another e-mail, enter it here. Or leave blank for apply admin e-mail.', $this->plugin_name); ?>">
	                                        <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
	                                    </a>
	                                </label>
	                            </div>
	                            <div class="col-sm-9">
	                                <input type="email" name="ays_notify_email" id="ays_notify_email"
	                                       value="" size="30">
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <hr>
					<div class="form-group row" >
	                    <div class="col-sm-3">
	                        <label for="ays_enable_mail_user">
	                            <?= __('Send Mail to User', $this->plugin_name); ?>
	                            <a class="ays_help" data-toggle="tooltip" data-placement="top"
	                            title="<?= __('Send the message to the emails of the users after the submission of the poll.', $this->plugin_name); ?>">
	                                <i class="ays_poll_fas ays_poll_fa-info-circle"></i>
	                            </a>
	                        </label>
	                    </div>
	                    <div class="col-sm-1">                       
	                            <input type="checkbox" id="ays_enable_mail_user" name="ays_enable_mail_user" value="on" />
	                    </div>
	                    <div class="col-sm-8 if-enable-email_note">
	                       
	                    </div>
	                </div>
                </div>
                <hr>
            </div>
            <div id="tab7"
                 class="ays-poll-tab-content <?= $active_tab == 'Integrations' ? 'ays-poll-tab-content-active' : ''; ?>">
                <p class="ays-subtitle">
                    <?= __('Integrations settings', $this->plugin_name); ?>
                </p>
                <hr/>
                <fieldset>
                    <legend>
                        <img class="ays_integration_logo" src="<?php echo POLL_MAKER_AYS_ADMIN_URL; ?>/images/integrations/mailchimp_logo.png" alt="">
                        <h5><?php echo __('MailChimp Settings',$this->plugin_name)?></h5>
                    </legend>
                    <?php
                    if(count($mailchimp) > 0):
                        ?>
                        <?php
                        if($mailchimp_username == "" || $mailchimp_api_key == ""):
                            ?>
                            <blockquote class="error_message">
                                <?php echo __(
                                    sprintf(
                                        __("For enabling this option, please go to ",$this->plugin_name)." %s ". __("page and fill all options.",$this->plugin_name),
                                        "<a style='color:blue;text-decoration:underline;font-size:20px;' href='?page=$this->plugin_name-settings&ays_poll_tab=tab2'>". __("this",$this->plugin_name)."</a>"
                                    ),
                                    $this->plugin_name );
                                ?>
                            </blockquote>
                        <?php
                        else:
                            ?>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="ays_enable_mailchimp">
                                        <?php echo __('Enable MailChimp',$this->plugin_name)?>
                                    </label>
                                </div>
                                <div class="col-sm-1">
                                    <input type="checkbox" class="ays-enable-timer1" id="ays_enable_mailchimp"
                                           name="ays_enable_mailchimp"
                                           value="on"
                                        <?php
                                        if($mailchimp_username == "" || $mailchimp_api_key == ""){
                                            echo "disabled";
                                        }else{
                                            echo ($enable_mailchimp == 'on') ? 'checked' : '';
                                        }
                                        ?>/>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="ays_mailchimp_list">
                                        <?php echo __('MailChimp list',$this->plugin_name)?>
                                    </label>
                                </div>
                                <div class="col-sm-8">
                                    <?php if(is_array($mailchimp_select)): ?>
                                        <select name="ays_mailchimp_list" id="ays_mailchimp_list"
                                            <?php
                                            if($mailchimp_username == "" || $mailchimp_api_key == ""){
                                                echo 'disabled';
                                            }
                                            ?>>
                                            <option value="" disabled selected>Select list</option>
                                            <?php foreach($mailchimp_select as $mlist): ?>
                                                <option <?php echo ($mailchimp_list == $mlist['listId']) ? 'selected' : ''; ?>
                                                        value="<?php echo $mlist['listId']; ?>"><?php echo $mlist['listName']; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    <?php else: ?>
                                        <span><?php echo $mailchimp_select; ?></span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php
                        endif;
                        ?>
                    <?php
                    else:
                        ?>
                        <blockquote class="error_message">
                            <?php echo __(
                                sprintf(
                                    __("For enabling this option, please go to ",$this->plugin_name)." %s ". __("page and fill all options.",$this->plugin_name),
                                    "<a style='color:blue;text-decoration:underline;font-size:20px;' href='?page=$this->plugin_name-settings&ays_poll_tab=tab2'>". __("this",$this->plugin_name)."</a>"
                                ),
                                $this->plugin_name );
                            ?>
                        </blockquote>
                    <?php
                    endif;
                    ?>
                </fieldset>
                <hr/>
                <fieldset>
                    <legend>
                        <img class="ays_integration_logo" src="<?php echo POLL_MAKER_AYS_ADMIN_URL; ?>/images/integrations/campaignmonitor_logo.png" alt="">
                        <h5><?php echo __('Campaign Monitor Settings', $this->plugin_name) ?></h5>
                    </legend>
                    <div class="form-group row">
                        <div class="col-sm-12" style="padding:20px;">
                            <div class="pro_features pro_features_integrations" style="justify-content:flex-end;">
                                <div style="margin-right:20px;">
                                    <p style="font-size:20px;">
                                        <?php echo __("This feature is available only in ", $this->plugin_name); ?>
                                        <a href="https://ays-pro.com/wordpress/poll-maker" target="_blank" title="Developer feature"><?php echo __("Developer package!!!", $this->plugin_name); ?></a>
                                    </p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="ays_enable_monitor">
                                        <?php echo __('Enable Campaign Monitor', $this->plugin_name) ?>
                                    </label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="checkbox" class="ays-enable-timer1" id="ays_enable_monitor" name="ays_enable_monitor" value="on"/>
                                </div>
                            </div>	                            
                            <hr>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="ays_monitor_list">
                                        <?php echo __('Campaign Monitor list', $this->plugin_name) ?>
                                    </label>
                                </div>
                                <div class="col-sm-8">                                    
                                    <select name="ays_monitor_list" id="ays_monitor_list">
                                        <option value="" disabled selected><?= __("Select List", $this->plugin_name) ?></option>
                                    </select>                                    
                                </div>
                        	</div>
                        </div>
                    </div>
                </fieldset>
                <hr/>
                <fieldset>
                    <legend>
                        <img class="ays_integration_logo" src="<?php echo POLL_MAKER_AYS_ADMIN_URL; ?>/images/integrations/zapier_logo.png" alt="">
                        <h5><?php echo __('Zapier Integration Settings', $this->plugin_name) ?></h5>
                    </legend>
                    <div class="form-group row">
                        <div class="col-sm-12" style="padding:20px;">
                            <div class="pro_features pro_features_integrations" style="justify-content:flex-end;">
                                <div style="margin-right:20px;">
                                    <p style="font-size:20px;">
                                        <?php echo __("This feature is available only in ", $this->plugin_name); ?>
                                        <a href="https://ays-pro.com/wordpress/poll-maker" target="_blank" title="Developer feature"><?php echo __("Developer package!!!", $this->plugin_name); ?></a>
                                    </p>
                                </div>
                            </div> 
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="ays_enable_zapier">
                                        <?php echo __('Enable Zapier Integration', $this->plugin_name) ?>
                                    </label>
                                </div>
                                <div class="col-sm-1">
                                    <input type="checkbox" class="ays-enable-timer1" id="ays_enable_zapier" name="ays_enable_zapier" value="on"/>
                                </div>
                                <div class="col-sm-3">
                                    <button type="button"                                           
                                            id="testZapier"
                                            class="btn btn-outline-secondary">
                                        <?= __("Send test data", $this->plugin_name) ?>
                                    </button>
                                    <a class="ays_help" data-toggle="tooltip" style="font-size: 16px;"
                                       title="<?= __('We will send you a test data, and you can catch it in your ZAP for configure it.', $this->plugin_name) ?>">
                                        <i class="ays_fa ays_fa_info_circle"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <hr/>
                <fieldset>
                    <legend>
                        <img class="ays_integration_logo" src="<?php echo POLL_MAKER_AYS_ADMIN_URL; ?>/images/integrations/activecampaign_logo.png" alt="">
                        <h5><?php echo __('Active Campaign Settings', $this->plugin_name) ?></h5>
                    </legend>
                    <div class="form-group row">
                        <div class="col-sm-12" style="padding:20px;">
                            <div class="pro_features pro_features_integrations" style="justify-content:flex-end;">
                                <div style="margin-right:20px;">
                                    <p style="font-size:20px;">
                                        <?php echo __("This feature is available only in ", $this->plugin_name); ?>
                                        <a href="https://ays-pro.com/wordpress/poll-maker" target="_blank" title="Developer feature"><?php echo __("Developer package!!!", $this->plugin_name); ?></a>
                                    </p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="ays_enable_active_camp">
                                        <?php echo __('Enable ActiveCampaign', $this->plugin_name) ?>
                                    </label>
                                </div>
                                <div class="col-sm-1">
                                    <input type="checkbox" class="ays-enable-timer1" id="ays_enable_active_camp" name="ays_enable_active_camp" value="on"/>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="ays_active_camp_list">
                                        <?php echo __('ActiveCampaign list', $this->plugin_name) ?>
                                    </label>
                                </div>
                                <div class="col-sm-8">                                   
                                    <select name="ays_active_camp_list" id="ays_active_camp_list">
                                        <option value="" disabled
                                                selected><?= __("Select List", $this->plugin_name) ?></option>
                                        <option value=""><?= __("Just create contact", $this->plugin_name) ?></option>
                                    </select>                                    
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="ays_active_camp_automation">
                                        <?php echo __('ActiveCampaign automation', $this->plugin_name) ?>
                                    </label>
                                </div>
                                <div class="col-sm-8">                                    
                                    <select name="ays_active_camp_automation" id="ays_active_camp_automation">
                                        <option value="" disabled
                                                selected><?= __("Select List", $this->plugin_name) ?></option>
                                        <option value=""><?= __("Just create contact", $this->plugin_name) ?></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <hr/>
                <fieldset>
                    <legend>
                        <img class="ays_integration_logo" src="<?php echo POLL_MAKER_AYS_ADMIN_URL; ?>/images/integrations/slack_logo.png" alt="">
                        <h5><?php echo __('Slack Settings', $this->plugin_name) ?></h5>
                    </legend>
                    <div class="form-group row">
                        <div class="col-sm-12" style="padding:20px;">
                            <div class="pro_features pro_features_integrations" style="justify-content:flex-end;">
                                <div style="margin-right:20px;">
                                    <p style="font-size:20px;">
                                        <?php echo __("This feature is available only in ", $this->plugin_name); ?>
                                        <a href="https://ays-pro.com/wordpress/poll-maker" target="_blank" title="Developer feature"><?php echo __("Developer package!!!", $this->plugin_name); ?></a>
                                    </p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="ays_enable_slack">
                                        <?php echo __('Enable Slack integration', $this->plugin_name) ?>
                                    </label>
                                </div>
                                <div class="col-sm-1">
                                    <input type="checkbox" class="ays-enable-timer1" id="ays_enable_slack" name="ays_enable_slack" value="on"/>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="ays_slack_conversation">
                                        <?php echo __('Slack conversation', $this->plugin_name) ?>
                                    </label>
                                </div>
                                <div class="col-sm-8">                                    
                                    <select name="ays_slack_conversation" id="ays_slack_conversation">
                                        <option value="" disabled
                                                selected><?= __("Select Channel", $this->plugin_name) ?></option>
                                    </select>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <hr>
            </div>
			<?php
			wp_nonce_field('poll_action', 'poll_action');
			$other_attributes = array('id' => 'ays-button');
			submit_button(__('Save and close', $this->plugin_name), 'primary', 'ays_submit', false, $other_attributes);
            submit_button(__('Save', $this->plugin_name), '', 'ays_apply', false, $other_attributes);
			?>
        </form>
    </div>
</div>