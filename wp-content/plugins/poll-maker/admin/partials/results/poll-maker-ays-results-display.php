<div class="wrap">
    <h1 class="wp-heading-inline">
        <?php
echo esc_html(get_admin_page_title());
?>
    </h1>

    <a href="https://ays-pro.com/index.php/wordpress/poll-maker/" target="_blank"><button class="disabled-button" style="float: right; margin-right: 5px; cursor: pointer;" title="<?=__('This property available only in PRO version', $this->plugin_name);?>" ><?=__('Export', $this->plugin_name);?></button></a>
    <div class="nav-tab-wrapper">
        <a href="#tab1" class="nav-tab nav-tab-active"><?= __('Results', $this->plugin_name); ?></a>
        <a href="#tab2" class="nav-tab"><?= __('Statistics', $this->plugin_name); ?></a>
    </div>
    <div id="tab1" class="ays-poll-tab-content ays-poll-tab-content-active">
        <div id="poststuff">
            <div id="post-body" class="metabox-holder">
                <div id="post-body-content">
                    <div class="meta-box-sortables ui-sortable">
                        <form method="get" id="filter-div" class="alignleft actions bulkactions">
                            <label for="bulk-action-selector-top" class="screen-reader-text">Select Filter Type</label>
                            <input type="hidden" name="page" value="poll-maker-ays-results">
                            <select name="orderbypoll" id="bulk-action-selector-top">
                                <option value="0" selected><?=__('No Filtering', $this->plugin_name);?></option>
                                <?php
                                    foreach ($this->results_obj->get_polls() as $poll) {?>
                                    <option value="<?=$poll['id'];?>" <?=(isset($_REQUEST['orderbypoll']) && $_REQUEST['orderbypoll'] == $poll['id']) ? 'selected' : '';?>><?=$poll['title'];?></option>
                                    <?php }
                                ?>
                            </select>
                            <input type="submit" id="doaction" class="button action" value="<?= __('Filter', $this->plugin_name); ?>" style="width: 3.7rem;">
                        </form>
                        <form method="post">
                            <?php                            
                                $this->results_obj->prepare_items();
                                $this->results_obj->display();
                                $this->results_obj->mark_as_read();
                            ?>
                        </form>
                    </div>
                </div>
            </div>
            <br class="clear">
        </div>
    </div>

    <div id="tab2" class="ays-poll-tab-content" style="padding-top: 14px;">
        <div class="col-sm-12" style="padding:20px;">
            <div class="pro_features" style="justify-content:flex-end;">
                <div style="margin-right:20px;">
                    <p style="font-size:20px;">
                        <?php echo __("This feature is available only in ", $this->plugin_name); ?>
                        <a href="https://ays-pro.com/wordpress/poll-maker" target="_blank" title="DEVELOPER feature"><?php echo __("DEVELOPER packadge!!!", $this->plugin_name); ?></a>
                    </p>
                </div>
            </div>
            <a href="https://ays-pro.com/index.php/wordpress/poll-maker/" target="_blank" title="<?=__('This property available only in PRO version', $this->plugin_name);?>">
                <img src="<?=plugins_url() . '/poll-maker/admin/images/chart_screen.png';?>" alt="Statistics" style="width:100%" >
            </a>
        </div>
    </div>
</div>
