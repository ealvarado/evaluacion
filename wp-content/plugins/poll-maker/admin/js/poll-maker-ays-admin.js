(function ($) {
    'use strict';

    window.FontAwesomeConfig = {
        autoReplaceSvg: false
    };

    $('.apm-unread').each(function () {
        let tr = $(this).parent().parent();
        tr.find('td').each(function () {
            $(this).css('color', '#dc3545');
        })
    });

    $.fn.serializeFormJSON = function () {
        let o = {},
            a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
    //CHECK REQUIRED FIELDS
    $('#ays-poll-form').on('submit', function (e) {
        if ($(this).find(`[data-required="true"]`).length > 0) {
            $(this).find(`[name][data-required="true"]`).each(function () {
                if ($(this).val() == '' || $(this).val() == null) {
                    let el = $(this);
                    if (el.hasClass('apm-pro-feature')) {
                        return
                    }
                    let name = $.trim($(this).parents('.form-group').find('.col-sm-3 label').text());
                    let tabContent = $(this).parents('.ays-poll-tab-content');
                    let tabID = tabContent.attr('id');
                    let tab = $(`.nav-tab-wrapper a.nav-tab[href="#${tabID}"]`);
                    let activeTab = tab.attr('data-title');
                    $(document).find('.nav-tab-wrapper a.nav-tab').each(function () {
                        if ($(this).hasClass('nav-tab-active')) {
                            $(this).removeClass('nav-tab-active');
                        }
                    });
                    tab.addClass('nav-tab-active');
                    $(document).find('.ays-poll-tab-content').each(function () {
                        $(this).css('display', 'none');
                    });
                    $('#ays_poll_active_tab').val(activeTab);
                    tabContent.css('display', 'block');

                    $("html, body").animate({
                        scrollTop: el.offset().top - 40
                    }, "slow");
                    let errorDiv = $('<div class="apm-alert alert alert-danger alert-dismissible fade show">'+
                                            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
                                            '<strong>${name}</strong> field is required. Please fill it for save the poll.'+
                                       '</div>');
                    $('#ays-poll-form .apm-alert').fadeOut();
                    $('#ays-poll-form').append(errorDiv);
                    setTimeout(() => {
                        errorDiv.fadeOut()
                    }, 7500);
                    e.preventDefault();
                    return false;
                }
            })
        }
    });

    $('[data-toggle="tooltip"]').tooltip();

    // Users limits
    $('.if-limit-users').css("display", "flex").hide();
    if ($('#apm_limit_users').prop('checked')) {
        $('.if-limit-users').fadeIn();
    }
    $('#apm_limit_users').on('change', function () {
        $('.if-limit-users').fadeToggle()
    });

    $('.if-users-roles').css("display", "flex").hide();
    if ($('#ays_enable_restriction_pass').prop('checked')) {
        $('.if-users-roles').css('display', 'unset');
        $('#ays_enable_logged_users').prop('checked', true)
    }
    $('#ays_enable_restriction_pass').on('change', function () {
        if ($(this).prop('checked')) {
            $('#ays_enable_logged_users').prop('checked', true);
            $('.if-logged-in').fadeIn();
            $('.if-users-roles').css('display', 'unset')
        } else {
            $('.if-users-roles').fadeOut()
        }
    });

    $('.if-logged-in').css("display", "block").hide();
    if ($('#ays_enable_logged_users').prop('checked')) {
        $('.if-logged-in').fadeIn();
    }
    $('#ays_enable_logged_users').on('change', function () {
        $('.if-logged-in').fadeToggle();
        if (!$(this).prop('checked')) {
            $('#ays_enable_restriction_pass').prop('checked', false);
            $('.if-users-roles').fadeOut()
        }
    });

    // Email notification
    $('.if-notify-email').css("display", "flex").hide();
    if ($('#ays_notify_by_email_on').prop('checked')) {
        $('.if-notify-email').fadeIn();
    }
    $('#ays_notify_by_email_on').on('change', function () {
        $('.if-notify-email').fadeToggle()
    });

    // Redirect after vote
    $('.if-redirect-after-vote').css("display", "flex").hide();
    if ($('#ays_redirect_after_vote').prop('checked')) {
        $('.if-redirect-after-vote').fadeIn();
    }
    $('#ays_redirect_after_vote').on('change', function () {
        $('.if-redirect-after-vote').fadeToggle()
    });

    // Answer Sound
    $('.if_answer_sound').css("display", "flex").hide();
    if ($('#ays_enable_asnwers_sound').prop('checked')) {
        $('.if_answer_sound').fadeIn();
    }
    $('#ays_enable_asnwers_sound').on('change', function () {
        $('.if_answer_sound').fadeToggle()
    });

    //Hide results
    $('.if-ays-poll-hide-results').css("display", "flex").hide();
    if ($('#ays-poll-hide-results').prop('checked')) {
        $('.if-ays-poll-hide-results').fadeIn();
    }
    $('#ays-poll-hide-results').on('change', function () {
        $('.if-ays-poll-hide-results').fadeToggle();
        if ($('#ays-poll-hide-results').prop('checked')) {
            $('#ays-poll-allow-not-vote').prop('checked', false)
        }
    });

    //Hide result message
    $('.if_poll_hide_result_message').css("display", "block").hide();
    if ($('#ays_poll_result_message').prop('checked')) {
        $('.if_poll_hide_result_message').fadeIn();
    }
    $('#ays_poll_result_message').on('change', function () {
        $('.if_poll_hide_result_message').fadeToggle();
    });

    //Allow not to vote
    $('#ays-poll-allow-not-vote').on('change', function () {
        if ($('#ays-poll-allow-not-vote').prop('checked')) {
            $('#ays-poll-hide-results').prop('checked', false);
            $('.if-ays-poll-hide-results').fadeOut()
        }
    });

    //Loading effect
    $('.if-loading-gif').css("display", "flex").hide();
    if ($('#ays-poll-load-effect').val() == 'load_gif') {
        $('.if-loading-gif').fadeIn();
    }
    $('#ays-poll-load-effect').on('change', function () {
        let effect = $(this).val();
        if (effect == 'load_gif') {
            $('.if-loading-gif').fadeIn();
        } else {
            $('.if-loading-gif').fadeOut()
        }
    });

    //User data form
    $(".ays-poll-sel-fields input[type='checkbox']").on('change', function () {
        let id = $(this).val();
        if ($(this).prop('checked')) {
            $('#ays-poll-box-rfield-' + id).fadeIn()
        } else {
            $('#ays-poll-box-rfield-' + id).find('input').prop('checked', false);
            $('#ays-poll-box-rfield-' + id).fadeOut();
        }
    });

    $(document).find('#ays_user_roles_poll').select2({
        placeholder: 'Select role'
    });

    $('.apm-cat-select2').select2({
        placeholder: 'Select category'
    });
    $('select.ays-select').not('.ays-select-search').select2({
        minimumResultsForSearch: -1
    });
    $('.ays-select-search').select2();

    $(document).on('click', 'a.add-question-image', function (e) {
        openMediaUploader(e, $(this));
    });
    $(document).on('click', 'a.add-bg-image', function (e) {
        openMediaUploaderBg(e, $(this));
    });

    $(document).on('click', '.ays-remove-question-img', function () {
        $(this).parent().find('img#ays-poll-img').attr('src', '');
        $(this).parent().find('input#ays-poll-image').val('');
        $(this).parent().fadeOut();
        $(document).find('.ays-field label a.add-question-image').text('Add Image');
        $('.ays-poll-img').remove();
    });
    $(document).on('click', '.ays-remove-bg-img', function () {
        $('img#ays-poll-bg-img').attr('src', '');
        $('input#ays-poll-bg-image').val('');
        $('.ays-poll-bg-image-container').parent().fadeOut();
        $(document).find('.ays-field a.add-bg-image').text('Add Image');
        $('.box-apm').css('background-image', 'unset');
        if ($(document).find('#ays-enable-background-gradient').prop('checked')) {
            toggleBackgrounGradient();
        }
    });
    let themes = [
        'personal',
        {
            'name': 'light',
            'mainColor': '#0C6291',
            'textColor': '#0C6291',
            'iconColor': '#0C6291',
            'bgColor': '#FBFEF9',
            'answerBgColor': '#FBFEF9',
            'titleBgColor': '#FBFEF9',
        },
        {
            'name': 'dark',
            'mainColor': '#FBFEF9',
            'textColor': '#FBFEF9',
            'iconColor': '#FBFEF9',
            'bgColor': '#222222',
            'answerBgColor': '#222222',
            'titleBgColor': '#222222',
        },
        {
            'name': 'minimal',
            'mainColor': '#7a7a7a',
            'textColor': '#424242',
            'iconColor': '#424242',
            'bgColor'  : 'rgba(0,0,0,0)',
            'answerBgColor': 'rgba(0,0,0,0)',
            'titleBgColor': 'rgba(0,0,0,0)',
        },
    ];

    $(document).on('click', '.delete a[href]', function(){
        return confirm('Do you want to delete?');
    });
    
    function openMediaUploader(e, element) {
        e.preventDefault();
        let aysUploader = wp.media({
            title: 'Upload',
            button: {
                text: 'Upload'
            },
            multiple: false
        }).on('select', function () {
            let attachment = aysUploader.state().get('selection').first().toJSON();
            if (attachment.type != 'image') {
                return alert('Please load image file');
            }
            element.text('Edit Image');
            $('.ays-poll-question-image-container').fadeIn();
            $('img#ays-poll-img').attr('src', attachment.url);
            $('input#ays-poll-image').val(attachment.url);
            $('.apm-img-box').empty().fadeIn().append(`<img class='ays-poll-img' src='${attachment.url}'>`);
        }).open();
        return false;
    }

    function openMediaUploaderBg(e, element) {
        e.preventDefault();
        let aysUploader = wp.media({
            title: 'Upload',
            button: {
                text: 'Upload'
            },
            multiple: false
        }).on('select', function () {
            let attachment = aysUploader.state().get('selection').first().toJSON();
            if (attachment.type != 'image') {
                return alert('Please load image file');
            }
            element.text('Edit Image');
            $('.ays-poll-bg-image-container').parent().fadeIn();
            $('img#ays-poll-bg-img').attr('src', attachment.url);
            $('input#ays-poll-bg-image').val(attachment.url);
            $('.box-apm').css('background-image', `url('${attachment.url}')`);
        }).open();
        return false;
    }

    $('#ays-poll-vote-type').on('change', function () {
        switch ($(this).val()) {
            case 'hand':
                $('#vote-res').removeClass().addClass('ays_poll_far ays_poll_fa-thumbs-up');
                break;
            case 'emoji':
                $('#vote-res').removeClass().addClass('ays_poll_fas ays_poll_fa-smile');
                break;
            default:
                break;
        }
    });
    $('#ays-poll-rate-type').on('change', function () {
        switch ($(this).val()) {
            case 'star':
                $('#rate-res').removeClass().addClass('ays_poll_fas ays_poll_fa-star');
                break;
            case 'emoji':
                $('#rate-res').removeClass().addClass('ays_poll_fas ays_poll_fa-smile');
                break;
            default:
                break;
        }
    });

    function rateType() {
        let val = $('#ays-poll-rate-value').val();
        $('#ays-poll-rate-value').empty();
        for (let i = 5; i > 2; i--) {
            if ($('#ays-poll-rate-type').val() == 'emoji' && i == 4) continue;
            let option = $(`<option value="${i}" ${i == val ? 'selected' : ''}>${i}</option>`);
            $('#ays-poll-rate-value').append(option)
        }
        $('#ays-poll-rate-value').show();
    }

    rateType();
    $('#ays-poll-rate-type').on('change', rateType);
    $('#add-answer').on({
        mouseover() {
            $(this).removeClass('ays_poll_far').addClass('ays_poll_fas');
        },
        mouseout() {
            $(this).removeClass('ays_poll_fas').addClass('ays_poll_far')
        },
        click() {
            let answersCount = $('.if-choosing .answers-col').find('input[type="text"]').length;
            let id = 1 + answersCount;
            $('.if-choosing .answers-col').append(`<div><input type="text" class="ays-text-input ays-text-input-short" name='ays-poll-answers[]' data-id='${id}'><input type="hidden" name="ays-poll-answers-ids[]" data-id='${id}' value="0"> <i class='ays_poll_fas ays_poll_fa-minus-square remove-answer' data-id='${id}' title='Remove answer'></i></div>`);

            let addedRedirectBox = '';

            addedRedirectBox += '<div class="ays-redirect-parent-box">';
                addedRedirectBox += '<div class="ays-redirect-content-left">';
                    addedRedirectBox += '<label for="ays_submit_redirect_url_'+id+'">URL ';
                        addedRedirectBox += '<a class="ays_help" data-toggle="tooltip">';
                            addedRedirectBox += '<i class="ays_poll_fas ays_poll_fa-info-circle"></i>';
                        addedRedirectBox += '</a>';
                    addedRedirectBox += '</label>';
                addedRedirectBox += '</div>';
                    
                addedRedirectBox += '<div>';
                    addedRedirectBox += '<input type="url" style="width: 370px;" class="ays-text-input ays-text-input-short" id="ays_submit_redirect_url_'+id+'" name="ays_submit_redirect_url[]" />';
                addedRedirectBox += '</div>';
            addedRedirectBox += '</div>';

            $('#ays_redirect_box').append(addedRedirectBox);
        }
    });
    $(document).on('click', '.remove-answer', function () {
        let childId = $(this).attr("data-id");
        $(document).find('#ays_submit_redirect_url_'+childId).parent().parent().remove();
        $(this).parent().remove();
    });
    $(document).on('mouseover', '.remove-answer', function () {
        $(this).removeClass('ays_poll_fas').addClass('ays_poll_far');
    });
    $(document).on('mouseout', '.remove-answer', function () {
        $(this).removeClass('ays_poll_far').addClass('ays_poll_fas');
    });

    $(document).find('.nav-tab-wrapper a.nav-tab').on('click', function (e) {
        let elemenetID = $(this).attr('href');
        let activeTab = $(this).attr('data-title');

        $(document).find('.nav-tab-wrapper a.nav-tab').each(function () {
            if ($(this).hasClass('nav-tab-active')) {
                $(this).removeClass('nav-tab-active');
            }
        });
        $(this).addClass('nav-tab-active');
        $(document).find('.ays-poll-tab-content').each(function () {
            $(this).css('display', 'none');
        });
        $('#ays_poll_active_tab').val(activeTab);
        $('.ays-poll-tab-content' + elemenetID).css('display', 'block');
        e.preventDefault();
    });
    $('.button-primary#ays-button').on('click', function () {
        $('#ays_poll_active_tab').val("General");
    });
    $('.button-primary#ays-button-top').on('click', function () {
        $('#ays_poll_active_tab').val("General");
    });

    function checkTheme() {
        let themeId = $(this).find('input').val() || 0;
        $('.ays_poll_theme_image_div label').each(function () {
            $(this).removeClass('apm_active_theme')
        });
        $(this).find('label').addClass('apm_active_theme');        

        answerStyleChange(themeId);

        textColorChange({
            color: themes[themeId].textColor
        });
        mainColorChange({
            color: themes[themeId].mainColor
        });
        bgColorChange({
            color: themes[themeId].bgColor
        });
        answerBgColorChange({
            color: themes[themeId].answerBgColor
        });
        titleBgColorChange({
            color: themes[themeId].titleBgColor
        });
        iconColorChange({
            color: themes[themeId].iconColor
        });
        $('#ays-poll-text-color').parent().parent().prev().css({
            'background-color': themes[themeId].textColor
        });
        $('#ays-poll-text-color').val(themes[themeId].textColor);
            
        $('#ays-poll-main-color').parent().parent().prev().css({
            'background-color': themes[themeId].mainColor
        });
        $('#ays-poll-main-color').val(themes[themeId].mainColor);

        $('#ays-poll-bg-color').parent().parent().prev().css({
            'background-color': themes[themeId].bgColor
        });
        $('#ays-poll-bg-color').val(themes[themeId].bgColor);

        $('#ays-poll-answer-bg-color').parent().parent().prev().css({
            'background-color': themes[themeId].answerBgColor
        });
        $('#ays-poll-answer-bg-color').val(themes[themeId].answerBgColor);

        $('#ays-poll-title-bg-color').parent().parent().prev().css({ 
            'background-color': themes[themeId].titleBgColor
        });
        $('#ays-poll-title-bg-color').val(themes[themeId].titleBgColor);

        $('#ays-poll-icon-color').parent().parent().prev().css({
            'background-color': themes[themeId].iconColor
        });
        $('#ays-poll-icon-color').val(themes[themeId].iconColor);
    }

    //checkTheme();
    $('.ays_poll_theme_image_div:not(.apm-pro-feature)').on('click', checkTheme);

    function checkType() {
        $('.ays_poll_type_image_div label').each(function () {
            $(this).removeClass('apm_active_type')
        });
        $(this).find('label').addClass('apm_active_type');

        let pollType = $(this).find('input').val();

        $('[class|="if"].poll-type-block ').hide().find('select, input:not([type="hidden"]):not(.ays_redirect_active)').attr('data-required', false);
        $('.if-' + pollType).css('display', 'flex').find('select, input:not([type="hidden"]):not(.ays_redirect_active)').attr('data-required', true);
    }

    let pollType = $('.apm_active_type').parent().find('input').val();
    $('.if-' + pollType).css('display', 'flex').find('select, input:not([type="hidden"]):not(.ays_redirect_active)').attr('data-required', true);
   
    $('.ays_poll_type_image_div:not(.apm-pro-feature)').on('click', checkType);

    function goToPro() {
        window.open(
            'https://ays-pro.com/wordpress/poll-maker/',
            '_blank'
        );
        return false;
    }

    $(document).on('click', "[id|='select2'] li[id$='-pro']", goToPro);
    $('.ays_poll_theme_image_div.apm-pro-feature').on('click', goToPro);
    $('.apm-loader.apm-pro-feature').on('click', goToPro);
    $('.apm-pro-feature-link').on('click', goToPro);

    //**********************/
    //LIVE PREVIEW
    //*********************/
    let themeId = $('.apm_active_theme+input').val() || 0;

    $(document).find('#ays-poll-main-color').wpColorPicker({
        defaultColor: themes[themeId].mainColor,
        change(event, ui) {
            $(this).wpColorPicker({defaultColor: themes[$('.apm_active_theme+input').val()].mainColor});
            mainColorChange(ui)
        }
    });
    $(document).find('#ays-poll-text-color').wpColorPicker({
        defaultColor: themes[themeId].textColor,
        change(event, ui) {
            $(this).wpColorPicker({defaultColor: themes[$('.apm_active_theme+input').val()].textColor});
            textColorChange(ui)
        }
    });
    $(document).find('#ays-poll-icon-color').wpColorPicker({
        defaultColor: themes[themeId].iconColor,
        change(event, ui) {
            $(this).wpColorPicker({defaultColor: themes[$('.apm_active_theme+input').val()].iconColor});
            iconColorChange(ui)
        }
    });
    $(document).find('#ays-poll-bg-color').wpColorPicker({
        defaultColor: themes[themeId].bgColor,
        change(event, ui) {
            $(this).wpColorPicker({defaultColor: themes[$('.apm_active_theme+input').val()].bgColor});
            bgColorChange(ui)
        }
    });
    $(document).find('#ays-poll-answer-bg-color').wpColorPicker({
        defaultColor: themes[themeId].answerBgColor,
        change(event, ui) {
            $(this).wpColorPicker({defaultColor: themes[$('.apm_active_theme+input').val()].answerBgColor});
            answerBgColorChange(ui)
        }
    });
    $(document).find('#ays-poll-title-bg-color').wpColorPicker({ //aray
        defaultColor: themes[themeId].titleBgColor,
        change(event, ui) {
            $(this).wpColorPicker({defaultColor: themes[$('.apm_active_theme+input').val()].titleBgColor});
            titleBgColorChange(ui)
        }
    });
    $(document).find('#ays-poll-box-shadow-color').wpColorPicker({
        defaultColor: '#000000',
        change(event, ui) {
            boxShadowColorChange(ui)
        }
    });


    $(document).find('#ays-background-gradient-color-1').wpColorPicker({
        defaultColor: '#103251',
        change: function(event, ui) {
            toggleBackgrounGradient()
        }
    });
     $(document).find('#ays-background-gradient-color-2').wpColorPicker({
        defaultColor: '#607593',
        change: function(event, ui) {
            toggleBackgrounGradient()
        }
    });
    if($(document).find('#ays-poll-bg-image').val() != ''){
        $('.box-apm').css('background-image', 'url('+ $(document).find('#ays-poll-bg-image').val() +')');
    }


    function mainColorChange(ui) {
        let color = ui.color.toString();
        document.documentElement.style.setProperty('--theme-main-color', color);
        $('#ays-poll-main-color').attr('value', color);
    }

    function textColorChange(ui) {
        let color = ui.color.toString();
        document.documentElement.style.setProperty('--theme-text-color', color);
        $('#ays-poll-text-color')
            .attr('value', color)
            .parents('.wp-picker-container')
            .find('.color-alpha').css('background-color', color);
    }

    function iconColorChange(ui) {
        let color = ui.color.toString();
        document.documentElement.style.setProperty('--theme-icon-color', color);
        $('#ays-poll-icon-color')
            .attr('value', color)
            .parents('.wp-picker-container')
            .find('.color-alpha').css('background-color', color);
    }

    function bgColorChange(ui) {
        let color = ui.color.toString();
        document.documentElement.style.setProperty('--theme-bg-color', color);
        $('#ays-poll-bg-color')
            .attr('value', color)
            .parents('.wp-picker-container')
            .find('.color-alpha').css('background-color', color);
    }

    var answer_border_side = $(document).find('#ays-poll-border-side').val();
    answerBorderSideChange(answer_border_side);

    function answerBorderSideChange(side) {
        var checked_answ = $(document).find( '#ays_poll_enable_answer_style').prop("checked");
        if (side == 'none' || !checked_answ) {
            $(document).find('.box-apm.choosing-poll label').each(function(){
                $(this)[0].style.border = 'none';
            });
        }else{
            switch(side) {
              case 'all_sides':
                $(document).find('.box-apm.choosing-poll label').each(function(){
                    $(this)[0].style.border = '1px solid';
                });
                break;
              case 'top':
                $(document).find('.box-apm.choosing-poll label').each(function(){
                    $(this)[0].style.border = 'none';
                    $(this)[0].style.borderTop = '1px solid';
                });
                break;
              case 'bottom':
                $(document).find('.box-apm.choosing-poll label').each(function(){
                    $(this)[0].style.border = 'none';
                    $(this)[0].style.borderBottom = '1px solid';
                });
                break;
              case 'left':
                $(document).find('.box-apm.choosing-poll label').each(function(){
                    $(this)[0].style.border = 'none';
                    $(this)[0].style.borderLeft = '1px solid';
                });
                break;
              case 'right':
                $(document).find('.box-apm.choosing-poll label').each(function(){
                    $(this)[0].style.border = 'none';
                    $(this)[0].style.borderRight = '1px solid';
                });
                break;
            }
        }
    }
    
    function answerStyleChange(theme_id) {
        if (theme_id == 3) {
            $('.box-apm').find('.apm-choosing').addClass('ays_poll_minimal_theme');
            $('.box-apm').find('.btn.ays-poll-btn').addClass('ays_poll_minimal_theme_btn');
            $('#ays_poll_enable_answer_style').removeAttr('checked');
            $('.ays_answer_style').hide();
            answerBorderSideChange("none");
            $('#ays-poll-border-width').val(0);
            document.documentElement.style.setProperty('--poll-border-width', `0px`);
        }else{
            $('.box-apm').find('.apm-choosing').removeClass('ays_poll_minimal_theme');
            $('.box-apm').find('.btn.ays-poll-btn').removeClass('ays_poll_minimal_theme_btn');
            $('#ays_poll_enable_answer_style').prop('checked');
            $('.ays_answer_style').show();
            answerBorderSideChange("all_sides");
        }        
    }

    function answerBgColorChange(ui) {
        let color = ui.color.toString();
        if (color == 'transparent' || !$(document).find('#ays_poll_enable_answer_style').prop("checked")) {
            document.documentElement.style.setProperty('--theme-answer-bg-color', "initial");
        }else{
            document.documentElement.style.setProperty('--theme-answer-bg-color', color);
            $('#ays-poll-answer-bg-color')
                .attr('value', color)
                .parents('.wp-picker-container')
                .find('.color-alpha').css('background-color', color);
        }
        
    }

    function titleBgColorChange(ui) { //aray
        let color = ui.color.toString();
        document.documentElement.style.setProperty('--theme-title-bg-color', color);
        $('#ays-poll-title-bg-color')
            .attr('value', color)
            .parents('.wp-picker-container')
            .find('.color-alpha').css('background-color', color);
    }

    function boxShadowColorChange(ui) {
        let color = ui.color.toString();
        if (color == 'transparent' || !$(document).find('#ays_poll_enable_box_shadow').prop("checked")) {
            document.documentElement.style.setProperty('--poll-box-shadow', "initial");
        }else{
            document.documentElement.style.setProperty('--poll-box-shadow', color + " 0 0 10px 0");
            $('#ays-poll-box-shadow-color')
                .attr('value', color)
                .parents('.wp-picker-container')
                .find('.color-alpha').css('background-color', color);
            }
    }
    
    /* 
    ========================================== 
        Background Gradient 
    ========================================== 
    */
   function toggleBackgrounGradient() {
        if ($(document).find('#ays-enable-background-gradient').prop('checked') || $(document).find('input#ays-poll-bg-image').val() != '') {
            if($(document).find('#ays_poll_gradient_direction').val() != '') {
                let ays_poll_gradient_direction = $(document).find('#ays_poll_gradient_direction').val();
                switch(ays_poll_gradient_direction) {
                    case "horizontal":
                        ays_poll_gradient_direction = "to right";
                        break;
                    case "diagonal_left_to_right":
                        ays_poll_gradient_direction = "to bottom right";
                        break;
                    case "diagonal_right_to_left":
                        ays_poll_gradient_direction = "to bottom left";
                        break;
                    default:
                        ays_poll_gradient_direction = "to bottom";
                }

                if($(document).find('input#ays-poll-bg-image').val() != ''){
                    return false;
                }else{
                    $(document).find('.box-apm').css({'background-image': "linear-gradient(" + ays_poll_gradient_direction + ", " + $(document).find('input#ays-background-gradient-color-1').val() + ", " + $(document).find('input#ays-background-gradient-color-2').val()+")"
                    });
                }
            }
        }
        else{
            $(document).find('.box-apm').removeAttr('style');
            return false;
        }
    }

    let ays_poll_box_gradient_color1_picker = {
            change: function (e) {
                setTimeout(function () {
                    toggleBackgrounGradient();
                }, 1);
            }
        };
        
    let ays_poll_box_gradient_color2_picker = {
        change: function (e) {
            setTimeout(function () {
                toggleBackgrounGradient();
            }, 1);
        }
    };

    $(document).find('#ays_poll_gradient_direction').on('change', function () {
        toggleBackgrounGradient();
    });

    $(document).find('#ays-background-gradient-color-1').wpColorPicker(ays_poll_box_gradient_color1_picker);
    $(document).find('#ays-background-gradient-color-2').wpColorPicker(ays_poll_box_gradient_color2_picker);

    toggleBackgrounGradient();
    $(document).find('input#ays-enable-background-gradient').on('change', function () {
        toggleBackgrounGradient();
    });

    $(document).on('click', '.ays-remove-poll-bg-img', function () {
        $(document).find('.box-apm').css({'background-image': 'none'});
        toggleBackgrounGradient();
    });

    $(document).find('#ays_poll_enable_answer_style').on('change', function () {
        let color = $(this).prop('checked') ? $(document).find('#ays-poll-answer-bg-color').val() : "transparent";
        let side = $(this).prop('checked') ? $(document).find('#ays-poll-border-side').val() : "none";
        answerBgColorChange({color});
        answerBorderSideChange(side);
    });

    $(document).find('#ays_poll_enable_box_shadow').on('change', function () {
        let color = $(this).prop('checked') ? $(document).find('#ays-poll-box-shadow-color').val() : "transparent";
        boxShadowColorChange({color});
    });
    $(document).find('#ays-enable-background-gradient').on('change', function () {
        let color = $(this).prop('checked') ? $(document).find('#ays-background-gradient-color-1').val() : "transparent";
        toggleBackgrounGradient({color});
    });

    $(document).find('#ays-enable-background-gradient').on('change', function () {
        let color = $(this).prop('checked') ? $(document).find('#ays-background-gradient-color-2').val() : "transparent";
        toggleBackgrounGradient({color});
    });

    $('#ays-poll-icon-size').on('change', function () {
        let val = (+$(this).val() > 10) ? $(this).val() : 10;
        $(this).val(val);
        document.documentElement.style.setProperty('--poll-icons-size', `${val}px`);
    });

    $('#ays-poll-width').on('change', function () {
        let val = +$(this).val();
        if (val === 0) {
            val = "100%";
        } else {
            val = (val > 249 ? val : 250) + "px";
        }
        document.documentElement.style.setProperty('--poll-width', val);
    });

    $('#ays-poll-btn-text').on('change', function () {
        let val = $(this).val() !== '' ? $(this).val() : "Vote";
        $('.ays-poll-btn').val(val)
    });

    $('#ays-poll-border-style').on('change', function () {
        let val = $(this).val();
        document.documentElement.style.setProperty('--poll-border-style', val);
    });

    $('#ays-poll-border-side').on('change', function () {
        let val = $(this).val();        
        
        switch(val) {
          case 'none':
            $(document).find('.box-apm.choosing-poll label').each(function(){
                $(this)[0].style.border = 'none';
            });
            break;
          case 'all_sides':
            $(document).find('.box-apm.choosing-poll label').each(function(){
                $(this)[0].style.border = '1px solid';
            });
            break;
          case 'top':
            $(document).find('.box-apm.choosing-poll label').each(function(){
                $(this)[0].style.border = 'none';
                $(this)[0].style.borderTop = '1px solid';
            });
            break;
          case 'bottom':
            $(document).find('.box-apm.choosing-poll label').each(function(){
                $(this)[0].style.border = 'none';
                $(this)[0].style.borderBottom = '1px solid';
            });
            break;
          case 'left':
            $(document).find('.box-apm.choosing-poll label').each(function(){
                $(this)[0].style.border = 'none';
                $(this)[0].style.borderLeft = '1px solid';
            });
            break;
          case 'right':
            $(document).find('.box-apm.choosing-poll label').each(function(){
                $(this)[0].style.border = 'none';
                $(this)[0].style.borderRight = '1px solid';
            });
            break;
        }
    });

    $(document).on('click', 'a.add-poll-bg-music', function (e) {
        openMusicMediaUploader(e, $(this));
    });

    $('#ays-poll-border-radius').on('change', function () {
        let val = $(this).val();
        document.documentElement.style.setProperty('--poll-border-radius', `${val}px`);
    });

    $('#ays-poll-border-width').on('change', function () {
        let val = $(this).val();
        document.documentElement.style.setProperty('--poll-border-width', `${val}px`);
    });

    $('#ays_custom_css').on('change', function () {
        let val = $(this).val();
        $('#apm-custom-css').html(val);
    });

    $('#show-title').on('change', function () {
        $('.apm-title-box').fadeToggle();
    });

    $('#ays-poll-title').on('change', function () {
        let val = $(this).val();
        $('.apm-title-box').text(val)
    });    

    //PRO features lightbox
    $('.open-lightbox').on('click', function (e) {
        e.preventDefault();
        var image = $(this).attr('href');
        $('html').addClass('no-scroll');
        $('.ays-poll-row ').append('<div class="lightbox-opened"><img src="' + image + '"></div>');
    });
    $('body').on('click', '.lightbox-opened', function () {
        $('html').removeClass('no-scroll');
        $('.lightbox-opened').remove();
    });


    if ($(document).find('.box-apm').length > 0) {
        var width = '40%';
        if ($('#ays-poll-width').val() != 0) {
            width = $('#ays-poll-width').val() + "px";
        }
        window.addEventListener('scroll', function () {
            if ($(window).scrollTop() < parseInt($(document).find('.box-apm').offset().top) + 20) {
                $(document).find('.box-apm-scroll').css({
                    'position': 'static',
                    'width': '100%'
                });
            }
            if ($(window).scrollTop() > parseInt($(document).find('.box-apm').offset().top) - 30) {
                $(document).find('.box-apm-scroll').css({
                    'position': 'fixed',
                    'top': '50px',
                    'width': width,
                    'max-width': '500px'
                });
            }
        });
        document.addEventListener('click', function () {
            if ($(window).scrollTop() < parseInt($(document).find('.box-apm').offset().top) + 20) {
                $(document).find('.box-apm-scroll').css({
                    'position': 'static',
                    'width': '100%'
                });
            }
            if ($(window).scrollTop() > parseInt($(document).find('.box-apm').offset().top) - 30) {
                $(document).find('.box-apm-scroll').css({
                    'position': 'fixed',
                    'top': '50px',
                    'width': width,
                    'max-width': '500px'
                });
            }
        });
    }


    $(document).on('change', '.ays_toggle', function (e) {
        let state = $(this).prop('checked');
        if ($(this).hasClass('ays_toggle_slide')) {
            switch (state) {
                case true:
                    $(this).parent().find('.ays_toggle_target').slideDown(250);
                    break;
                case false:
                    $(this).parent().find('.ays_toggle_target').slideUp(250);
                    break;
            }
        } else {
            switch (state) {
                case true:
                    $(this).parent().find('.ays_toggle_target').show(250);
                    break;
                case false:
                    $(this).parent().find('.ays_toggle_target').hide(250);
                    break;
            }
        }
    });

    $(document).on("input", 'input', function (e) {
        if (e.keyCode == 13) {
            return false;
        }
    });
    $(document).on("keydown", function (e) {
        if (e.target.nodeName == "TEXTAREA") {
            return true;
        }
        if (e.keyCode == 13) {
            return false;
        }
    });

    $('.active_date_check').change(function () {
        if ($(this).prop('checked')) {
            $('.active_date').show(250);
        } else {
            $('.active_date').hide(250);
        }
    });

    $('#ays_poll_show_timer').change(function () {
        if ($(this).prop('checked')) {
            $('.ays_show_time').show(250);
        } else {
            $('.ays_show_time').hide(250);
        }
    });

    //info_form

    $("#ays_poll_info_form").change(function () {
        if ($(this).prop('checked')) {
            $(this).parents(".form-group").find(".form-group").show(250);
        } else {
            $(this).parents(".form-group").find(".form-group").hide(250);
        }
    });

    $('#ays_users_roles').select2();
    $('#ays_user_roles').select2();
    $('[data-toggle="tooltip"]').tooltip();

    $(document).find('#ays-deactive, #ays-active').datetimepicker({
        controlType: 'select',
        oneLine: true,
        dateFormat: "yy-mm-dd",
        timeFormat: "HH:mm:ss"
    });

    let unread_result_parent = $(document).find(".unread-result").parent().parent();

    if (unread_result_parent != undefined) {
        unread_result_parent.css({"font-weight":"bold"});
    }

    $(document).find('input[type="checkbox"]#ays_redirect_after_submit').on('change', function(e){
        if($(this).prop('checked') == false){
            $(document).find('#ays_redirect_box').css('display', 'none');
            $(document).find('.ays_redirect_active').attr('data-required', false);
        }else{
            $(document).find('#ays_redirect_box').removeAttr('style');
            $(document).find('.ays_redirect_active').attr('data-required', false);
        }

    });

    var checkLabelHover = $(document).find('label.ays_label_poll');       
    var disableAnswerHover = $(document).find("#ays_disable_answer_hover");
    disableAnswerHover.on('change' , function(){
        if(checkLabelHover.hasClass('ays_enable_hover') && !checkLabelHover.hasClass('disable_hover')){
            checkLabelHover.addClass('disable_hover');
            checkLabelHover.removeClass('ays_enable_hover');
        }else{            
            checkLabelHover.addClass('ays_enable_hover');
            checkLabelHover.removeClass('disable_hover');
        }
    });

    function openMusicMediaUploader(e, element) {
        e.preventDefault();
        let aysUploader = wp.media({
            title: 'Upload music',
            button: {
                text: 'Upload'
            },
            library: {
                type: 'audio'
            },
            multiple: false
        }).on('select', function () {
            let attachment = aysUploader.state().get('selection').first().toJSON();
            element.next().attr('src', attachment.url);
            element.parent().find('input.ays_poll_bg_music').val(attachment.url);
        }).open();
        return false;
    }

})(jQuery);