(function($) {
    'use strict';

    $(document).on('click', '[data-slug="poll-maker"] .deactivate a', function () {
        swal({
            html: "<h2>Do you want to upgrade to Pro version or permanently delete the plugin?</h2><ul><li>Upgrade: Your data will be saved for upgrade.</li><li>Uninstall: Your data will be deleted completely.</li></ul>",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Upgrade',
            cancelButtonText: 'Deactivate'
        }).then((result) => {
            let upgrade_plugin = false;
            if (result.value) upgrade_plugin = true;
            let data = {
                action: 'apm_deactivate_plugin_option_pm',
                upgrade_plugin
            };
            $.post({
                url: apm_admin_ajax_obj.ajaxUrl,
                dataType: 'json',
                data,
                success() {
                    location.replace($('[data-slug="poll-maker"] .deactivate a').attr('href'))
                }
            });
        });
        return false;
    });
})(jQuery);