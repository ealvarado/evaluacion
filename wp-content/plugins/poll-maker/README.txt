﻿=== Poll Maker ===
Contributors: ays-pro
Donate link: https://ays-pro.com/index.php/wordpress/poll-maker
Tags:  poll maker, poll builder, voting, rating, elections
Requires at least: 4.0
Tested up to: 5.5.1
Stable tag: 2.3.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

POLL MAKER plugin allows you to make an unlimited number of polls (choosing, rating, voting)

== Description ==


### Wordpress Poll maker

* [WordPress Poll plugin pricing](https://ays-pro.com/index.php/wordpress/poll-maker)
* [Poll Maker Free Demo](https://freedemo.ays-pro.com/poll-maker-demo-free/)
* [Poll Maker Pro Demo](https://prodemo.ays-pro.com/poll-maker-demo-pro/)

https://www.youtube.com/watch?v=vHBpUt0FPm4

Poll maker is a reciprocal plugin which is very easy to use and is a perfect time-saving plugin.

You can customize the types, settings (text color, icons’ color, main color etc.) choose theme (light, dark) for each individual poll.
It gives an opportunity of:

* lifetime usage
* does all on one page
* has default WordPress editor

And here is vital to say that it is responsive to all kinds of gadgets. It has interactive content and is a captivating way to get in touch with your users as soon as they arrive on your website. This helps to spread democracy among users or employees. The most number of votes that is resulted from the poll was the presidential poll in the world. It sounds like «will trump win» poll. Consequently, that was a big web poll which brought a huge information result.

== Poll maker plugin features, advantages ==

= Poll question editor =

There is a default WordPress classic editor for adding and editing the poll question.
You can use it for formatting your question text and adding media.

= Poll types =

Poll Maker gives you an opportunity to make 4 types polls:

* **choosing** - You can add as many options for choosing as you want. There is also an option that allows users to add their custom option(Pro).
* **rating** - With this type you can make polls with rating a single product. Poll maker allows you to choose the type of rating: with stars or with emojis. It depends on the subject you want to examine.
* **voting** - With this type you can make polls with voting. Here is again two types: like/dislike or emojis.
It is an easy way to understand users love your product or not.
* **VS (versus)** Available only in [Pro version](https://ays-pro.com/wordpress/poll-maker) - This is the type for making competition with two products or persons. There are awesome styles for making versus polls.

= Social sharing =

It has an option for social share buttons - facebook, tweeter, LinkedIn. You can enable and disable it. Social sharing will attract many people to attend to your poll and also visit your website.

= Notify admin by email =

You can enable sending email to admin when anyone passes your poll. Some time it is very important to know about every vote. It allows you to be informed and doesn't check your admin dashboard frequently.


= Schedule Poll =

Now there is wide used function: schedule. We also include this function into our poll builder plugin.
It saves a mass of time. For example, if you are going to make a poll in the near future you don't need to remember and set an alarm for that. You can make your poll right now and set a start date for displaying it.
Also, it is important to set an end date for the poll, as long as you may need to check product rating for a specific period of time. The plugin will show the expiring message after the end date, which provided from the dashboard.

= Limit by user roles =

This feature allows you to select specific user roles for displaying poll. Only users with the selected user group can take the poll. Also, you can set the restriction that only logged in users have permission to take the poll. There is a message parameter for every option.

= Limit to vote only once =

If you want to have a fair polling system this feature very important for you. It allows the user to vote only once so the final results of the poll will be objective.

= Templates and style options =

The poll maker has 6 awesome templates (4 of them available only in [Pro version](https://ays-pro.com/wordpress/poll-maker)). You can choose your preferred template and customize it with style options. There are 13+ style options for customizing the poll's view. You can customize colors, images, border, backgrounds.

= Information form with custom fields  =

It is available only in [Pro version](https://ays-pro.com/wordpress/poll-maker). You can add information form to your poll. After or before taking the poll user must fill his information like name, email, phone or any other information you have added into the form. So it is a very useful tool for collecting user's data and using it for your marketing.

= Mailchimp integration  =

This feature is available only in [Pro version](https://ays-pro.com/wordpress/poll-maker). MailChimp is a powerful online tool for email marketing. We've made integration with Mailchimp which allows you to collect users' emails automatically. It saves a lot of time and gives you an opportunity to make a huge community.


== The notion of ONLINE VS OFFLINE ==
* Offline As most of the people do not answer directly to the questions of elections when they are recorded so this quick poll allows them to answer truly suchlike questions. And here we can add that poll gives completely another result.

* By online polling system, you conclude to more data which is the key to success of nowadays world. This easy poll builder can give you an information on anonymous users which will bring again and again MAXIMUM RESULT OF DATA. You know you have succeeded when your poll captivates your audience with engaging and interesting questions.

You can use a poll as a means to interact with your audience, both get and give some information about your sphere, and use the answers as a basis for the development of your company. That is why we can say that by poll results you can get the information, for instance about the upcoming elections or just voting the Team Leader in your office at the same time spreading democracy. So your users’ opinion and feedback may be a good ground for making your features better and suitable to their needs.

The effectiveness of the poll depends on the questions it asks your users in order to keep them interested and engaged. You should know your target audience very well and choose appropriate topics to discuss with them. Think thoroughly when suggesting options for the answers. Keep the variants diverse, so that the users have a wide range to choose their answer from.

== Easy in use ==
The phenomenon of easy poll maker is just for this case. The most essential information is, that you can create your own rating poll on one page. You don’t need to create separate poll give style to it, give color to questions and answers, etc. these all you just do in one page in our poll maker. And after it do tour poll. You can easily make your own news poll, art poll, etc. in one page. For instance custom the poll yourself for the aim of motivating your employees and get quick responses. And the number of polls is unlimited. You can create any kind of poll questions connected with video, audio and image poll.

You can custom here:

* Settings
* Style (theme, icons, colors)
* Variants of polls
* Default WordPress editor

The advantage of the poll is that the users do not need to go to other websites for voting it is an instant poll.

It has types of functions of
a) voting
b) choosing
c) rating (rating-emoji, rating-stars).

So by these different types of poll questions assume this or that answers. This allows to construct any kind of question such as video, photo, audio, etc. and you can insert whatever you want with shortcodes.

== SURVEY VS POLL ==
* Survey need time, additional information, type mode, etc.
* As for the poll, it is time-saving, easy to use, results in more data, anonymous users and you can custom yourself.
That is why this way of getting information is more flexible and useful than survey.
Our poll also has a widget possibility and while installing the plugin widget is also installed. A number of websites are most likely to place the poll on the sidebar keeping it dominant. So your clients do not need to waste their time while participating in some elections they just do it on the spot by one click and this provides the huge amount of result. And also the dashboard gives information about users' IP address which is the most important thing in the nowadays modern world.

== Why choose our plugin? ==
* Easy to use and time-saving
* All in one page
* Types: choosing, rating, voting
* Themes dark/light
* Result/data
* Always will be updated

**Features**

* Poll types - choosing, rating, voting
* Unlimited answers
* Different icons
* Categorize polls
* Category-multiple view
* Reports in dashboard
* Social share buttons
* Notification by e-mail
* Limit users rate once
* Only for logged in users option
* Limit by user roles
* Style settings (10+)
* Two themes
* Live preview


**[PRO](https://ays-pro.com/index.php/wordpress/poll-maker) Features**

* Includes ALL Free version Features and
* VS type of poll (versus)
* User information form
* Ability to add custom option
* Extra 2 themes
* Results with charts
* Export results to CSV
* Custom Form Fields
* Text instead of results
* Vote reason option
* Voted user location
* SMTP integration
* Import/Export polls
* MailChimp integration
* Campaign Monitor integration
* Zapier integration
* ActiveCampaign integration
* Slack integration
* More on the way ...

Don't forget, in case of any problems or upcoming questions feel free to contact us via e-mail info@ays-pro.com.

== Screenshots ==

1.  Poll Maker - front end poll preview gif
2.  Poll Maker - front end poll preview
3.  Poll Maker - front end poll preview
4.  Poll Maker - dashboard polls list
5.  Poll Maker - dashboard edit poll
6.  Poll Maker - dashboard edit poll
7.  Poll Maker - dashboard edit poll style
8.  Poll Maker - dashboard poll results
9.  Poll Maker - dashboard preview gif

== Installation ==


There are two ways to install Poll maker: the easy way, when you install Poll maker from your WordPress dashboard, and the not so easy way, when you install it from WordPress.org.

* 1.1 The easiest way to enjoy Poll Maker:
* 1.1.1	Login to your WordPress dashboard
* 1.1.2	Go to Plugins
* 1.1.3	Add New
* 1.1.4	Search for Poll maker
* 1.1.5	Click to install
* 1.2 The second way:
* 1.2.1Download the zip file from https://wordpress.org/plugins/poll-maker/
* 1.2.2 Go to Plugins
* 1.2.3 Add New
* 1.2.4 Upload plugin
* 1.2.5 Choose file ays poll_maker.zip
* 1.2.6	Click to install

* 1.3 In order to install the Poll maker from Wordpress.org you must unzip the archive and copy the poll-maker folder into your plugins folder (\wp-content\plugins).
After one ofthis steps you can activate the plugin from the Plugins menu.
Once activated configure any options as desired and you can enjoy our Poll maker.

== Frequently Asked Questions ==

= 1. How can I add a Poll in my site? =

At first, you need to install the plugin and create a Poll in it. After it, a shortcode will be
generated which you can add wherever you want in your site. And then you can see your Poll.

= 2. Are there any limitations in the way of making questions for Poll. =

The plugin has no limitation in the way of making questions Poll. You can make a Poll
yourself and can add as many Polls as you want. And, you can construct any kind of Poll
question such as video, photo, audio, etc. And you can insert whatever you want with shortcodes.

= 3. Can I add the same poll in different pages? =

Yes, of course. You can add the same Poll at the same time in different pages of the site. You
can also add the Polls in widgets on sidebar. And this does not interrupt the traffic of your site.

= 4. How to check the results in Poll? =

After the installation of the plugin you will need to open settings ("Poll maker" in dashboard)
and entering "Results" section you will find the results of your Poll with the help of filters.

= 5. How many types are there in the Poll? =

Our Poll has the following types: voting, choosing, rating (rating-emoji, rating-stars)

= 6. What shall I do in case of any problem? =

Our support team works very hard to provide answers within 24 hours. We do our best for
giving you thorough answers. For the future, we also plan many new and fantastic features and,
of course, new updates.

= 7. How can I add a poll on the widget? =

There are two Polls set by default in our plugin. For adding a Poll in widget you need to
follow the instruction: Go to the widget section and if you have a Poll in our plugin, you will see
there our widget. Then put it on the sidebar and select the Poll and a width for it. Afterward, we
can say that it is a widget poll.


== Changelog ==
= 2.3.4 =
* Fixed: Show timer option calculation
* Tested: Compatible up to: 5.5.1

= 2.3.3 =
* Fixed: Some small issue concerning overall design in the admin dashboard
* Updated: POT and PO files

= 2.3.2 =
* Added: Answers sound option

= 2.3.1 =
* Fixed: CSS improvement
* Updated: POT and PO files

= 2.3.0 =
* Tested: Compatible up to: 5.5

= 2.2.9 =
* Added: Custom class for poll container

= 2.2.8 =
* Fixed: Empty form mail issue
* Fixed: Some style bugs in login form
* Fixed: Some small issue concerning overall design in the admin dashboard

= 2.2.7 =
* Fixed: Form mail issue
* Fixed: login form style
* Fixed: Some style bugs in admin dashboard 

= 2.2.6 =
* Added: Show timer option
* Updated: POT and PO files

= 2.2.5 =
* Added: Disable IP storing option
* Fixed: Some style bugs in front-end

= 2.2.4 =
* Added: Show creation date option
* Added: Show author option

= 2.2.3 =
* Added: Create date and author functionality for poll

= 2.2.2 =
* Fixed: Screen option issue
* Tested: Compatible up to: 5.4․2

= 2.2.1 =
* Added: Answer hover disable option

= 2.2.0 =
* Fixed: All code standard issues reported by Coderisk
* Fixed: PHP error in front end

= 2.1.9 =
* Fixed: Redirect after voting issue
* Fixed: Some style bugs in admin dashboard 

= 2.1.8 =
* Fixed: All code standard issues reported by Coderisk
* Fixed: PHP error in front end

= 2.1.7 =
* Added: Greek language support
* Updated: POT and PO files

= 2.1.6 =
* Added: Minimal theme
* Tested: Compatible up to: 5.4․1

= 2.1.5 =
* Fixed: PHP errors in front-end and in the dashboard
* Fixed: Gutenberg block bug

= 2.1.4 =
* Added: Answer border side option

= 2.1.3 =
* Added: Russian language support
* Updated: POT and PO files

= 2.1.2 =
* Added: Answer style option

= 2.1.1 =
* Added: List table completed count
* Fixed: Choose answer style issue

= 2.1.0 =
* Fixed: Some small bugs in the dashboard
* Updated: PO file
* Deleted: Poll Maker from WP settings

= 2.0.9 =
* Added: Results message option
* Updated : POT file
* Tested: Compatible up to: 5.4

= 2.0.8 =
* Changed: Poll results shortcode ordering

= 2.0.7 =
* Added: Shorcode to display Poll results in the front-end

= 2.0.6 =
* Added: Pre start message option for scheduling

= 2.0.5 =
* Added: Selecting Multiple user roles

= 2.0.4 =
* Added: Redirection after selecting an answer option

= 2.0.3 =
* Added: Unread answer(s) bold in result page
* Fixed: PHP error in front end

= 2.0.2 =
* Added: Background gradient option

= 2.0.1 =
* Fixed: Exact correct time for the schedule option

= 2.0.0 =
* Fixed: JavaScript  conflict in admin dashboard

= 1.9.9 =
* Fixed: Category shortcode issue

= 1.9.8 =
* Added: Shortcode to display all the polls
* Fixed: Style bugs in some themes

= 1.9.7 =
* Added: Elementor Widget

= 1.9.6 =
* Changed: Schedule option style
* Fixed: Some style bugs

= 1.9.5 =
* Fixed: Selected answer issue after result sort

= 1.9.4 =
* Fixed: Font Awesome icons svg making
* Changed: Text for Save and Apply button

= 1.9.3 =
* Added: Show answer percent option

= 1.9.2 =
* Added: Showing selected for all type answer after voting

= 1.9.1 =
* Added: Show result option after the schedule end date

= 1.9.0 =
* Added: Showing selected answer after voting
* Fixed: Some style bugs

= 1.8.9 =
* Fixed: Category shortcode initialization issue
* Fixed: Some admin dashboard style bugs
* Fixed: Border width option issue

= 1.8.8 =
* Fixed: Show question for logged in users issue

= 1.8.7 =
* Fixed: Database error issue

= 1.8.6 =
* Added: Text referring shortcode after Poll completion

= 1.8.5 =
* Added: Confirm for delete poll
* Fixed: Color picker style issue

= 1.8.4 =
* Fixed: Conflict See Results button with Information Form
* Tested: Compatible up to: 5.3.2

= 1.8.3 =
* Fixed: Some front-end bugs
* Fixed: Some admin dashboard style bugs

= 1.8.2 =
* Fixed: Widget functionality issue
* Tested: Compatible up to: 5.3.1

= 1.8.1 =
* Fixed: Gutenberg block bug
* Fixed: Page load time issue
* Fixed: Load JS and CSS only for poll pages
* Fixed: Ajax see result bug


= 1.8.0 =
* Fixed: Font-awesome cdn issue
* Fixed: Some style small bugs

= 1.7.9 =
* Fixed: Internet explorer issues
* Fixed: Some small bugs

= 1.7.8 =
* Changed: Plugin size

= 1.7.7 =
* Fixed: Some small bugs

= 1.7.6 =
* Tested: Compatible up to: 5.3

= 1.7.5 =
* Added: Mailchimp integration

= 1.7.4 =
* Added: Option Show login form

= 1.7.3 =
* Fixed: JavaScript bugs
* Fixed: Some small bugs

= 1.7.2 =
* Added: Sorting for the result page
* Fixed: Gutenberg Widget functionality issue

= 1.7.1 =
* Added: Schedule time option
* Tested: Compatible up to: 5.2.4

= 1.7.0 =
* Fixed: AJAX conflict in dashboard

= 1.6.9 =
* Fixed: Some small bugs
* Fixed: Widget styles
* Fixed: Some WP Themes styles conflict

= 1.6.8 =
* Changed: Gutenberg Widget functionality

= 1.6.7 =
* Fixed: Redirect message countdown issue
* Fixed: Information form button issue

= 1.6.6 =
* Fixed: Redirect message issue
* Fixed: Information form issue
* Fixed: Content max width
* Fixed: Information form send button issue

= 1.6.5 =
* Fixed: Poll answers value, that depends on the poll type

= 1.6.4 =
* Fixed: Limitation users issue
* Added: Show count votes option

= 1.6.3 =
* Fixed: Duplicate votes issue

= 1.6.2 =
* Fixed: Delete results issue

= 1.6.1 =
* Tested: Compatible up to: 5.2.3
* Added: Poll direction center option
* Added: Apply button in the Add new page

= 1.6.0 =
* Updated: Added "Limitation Method" option
* Updated: Added "WP User" column to reports
* Fixed: Storefront theme styles conflict
* Fixed: Information form inputs border-color

= 1.5.9 =
* Updated: Color picker with alpha channel
* Fixed: Buttons margin in some themes
* Fixed: Upgrade pop-up
* Fixed: Two same items in settings menu

= 1.5.8 =
* Fixed: Randomize answers option for rating/voting types
* Fixed: Poll margin in some themes
* Fixed: Some small bugs

= 1.5.7 =
* Fixed: Results page issue
* Fixed: Issue max-width 90% for information form inputs
* Added: Title background color option

= 1.5.6 =
* Added: User Name, User Email, User Phone columns in results page

= 1.5.5 =
* Updated: Added user data information form
* Fixed: icon style in MailPoet3 dashboard

= 1.5.4 =
* Updated: Added randomize answers option

= 1.5.3 =
* Updated: AysPro Top menu
* Updated: Added default answers values
* Fixed: Some bugs in admin dashboard

= 1.5.2 =
* Updated: Added duplicate poll action
* Updated: Default width of widget set 100%
* Updated: Active tab saving method
* Fixed: Buttons width and margins in some themes

= 1.5.1 =
* Fixed: Only for logged in users behavior
* Fixed: Some small bugs
* Fixed: Default messages
* Fixed: Vote button width in some themes

= 1.5.0 =
* Fixed: Widgets styles loading
* Fixed: Wordpress 4.7 WPEditor error
* Fixed: Category view

= 1.4.9 =
* Fixed: Some front-end bugs

= 1.4.8 =
* Updated: Rolled back some changes

= 1.4.7 =
* Updated: Performance optimised
* Fixed: has_shortcode issue

= 1.4.6 =
* Fixed: Widget styles
* Fixed: Some WP Themes styles conflict
* Fixed: Removed files requests
* Fixed: Font Awesome icons svg making

= 1.4.5 =
* Fixed: Some front-end bugs
* Fixed: Disabled data saving
* Tested: Compatible up to: 5.2.2

= 1.4.4 =
* Updated: Live preview
* Added: Answers background color option
* Fixed: Some bugs in admin dashboard

= 1.4.3 =
* Updated: Pro Features information
* Fixed: Poll maker widget
* Fixed: Some bugs in admin dashboard
* Fixed: Border width default value issue

= 1.4.2 =
* Added: "See Results" button text option
* Fixed: Some small bugs

= 1.4.1 =
* Added: Right to Left direction
* Added: Status column in list table of polls

= 1.4.0 =
* Updated: Poll type selecting field
* Added: New default poll

= 1.3.9 =
* Updated: Choosing answers field
* Fixed: Small bugs in admin dashboard

= 1.3.8 =
* Fixed: Choosing view type default value

= 1.3.7 =
* Fixed: Choosing view type save issue
* Fixed: Old results saving issue
* Tested: Compatible up to: 5.2.1

= 1.3.6 =
* Updated: Expired poll message available
* Fixed: Mail notification issue
* Fixed: Some dashboard bugs

= 1.3.5 =
* Updated: Scripts optimised
* Updated: Results page
* Fixed: Redirection issue
* Tested: Compatible up to: 5.2

= 1.3.4 =
* Updated: 4 new loading animations
* Updated: Vote button now optional

= 1.3.3 =
* Removed: G+ Share button
* Updated: Themes choosing option

= 1.3.2 =
* Fixed: Loading option "Custom GIF" open in new tab
* Added: "Code include" column in list table
* Updated: HTML tags support in answers fields

= 1.3.1 =
* Added: Default color button in color-picker
* Added: Restart feature after polling

= 1.3.0 =
* Added: active status enable/disable option
* Fixed: Custom css issue
* Fixed: deactivator issue

= 1.2.9 =
* Added: Box shadow option
* Added: Border width option
* Fixed: Some issues in dashboard

= 1.2.8 =
* Added: Border radius option
* Added: User data and Email tabs
* Fixed: Pro features

= 1.2.7 =
* Added: Start and end date options for poll
* Added: Redirection after vote delay option
* Fixed: Some front-end bugs

= 1.2.6 =
* Added: Show passed users count in front end
* Added: Publish/Unpublish option
* Fixed: Some fixed issues

= 1.2.5 =
* Added: Redirection after vote option
* Added: Results sorting option

= 1.2.4 =
* Fixed: Scripts and styles optimizations

= 1.2.3 =
* Fixed: "Hide results" outputs
* Fixed: "Social share buttons" bugs
* Fixed: Some admin dashboard bugs

= 1.2.2 =
* Fixed: Empty unread result after "See Results"
* Fixed: Submenu "Poll Maker" changed to "Polls"

= 1.2.1 =
* Fixed: Share buttons margin conflict in some themes
* Fixed: Notification mail with slashes

= 1.2.0 =
* Fixed: Widget width
* Fixed: Some bugs
* Updated: Gutenberg block

= 1.1.9 =
* Added: Notification by e-mail
* Added: Unread results badge
* Updated: Customize message isntead results
* Updated: Gutenberg block

= 1.1.8 =
* Added: Restrictions advanced settings
* Added: Only for logged in users option
* Added: Restriction by users role option
* Fixed: Widget error
* Fixed: Categories conflict in some themes

= 1.1.7 =
* Fixed: Updating "Duplicate column name" database error
* Fixed: Some bugs in front-end
* Updated: POT file

= 1.1.6 =
* Updated: Focus on not filled required fields
* Updated: Iframe support in question
* Updated: No limit for sybols in question
* Updated: Limit users to rate only once
* Fixed: Rating and Voting icons cashing
* Fixed: Voting results conflict with some themes

= 1.1.5 =
* Added: PRO features page
* Added: Poll Maker block for Gutenberg
* Updated: POT file

= 1.1.4 =
* Fixed: Some bugs in front-end
* Fixed: FontAwesome SVG autoreplacing
* Added: Results loading animation choosing
* Updated: Settings active tab saving
* Updated: All external scripts/styles are included now
* Tested: Compatible up to: 5.0.3

= 1.1.3 =
* Fixed: Some bugs in back-end
* Added: Social share buttons on/off feature

= 1.1.2 =
* Solved: Font Awesome conflict
* Fixed: Some bugs in front-end
* Fixed: Some bugs in back-end
* Added: Short-code recognition in poll question

= 1.1.1 =
* Fixed: Icons bug in some themes
* Fixed: Upgrade dialog
* Fixed: Gutenberg update error
* Added: Social share buttons

= 1.1.0 =
* Fixed: Some bugs in front-end
* Fixed: Some bugs in back-end
* Added: See results without voting feature

= 1.0.9 =
* Tested: Compatible up to 5.0.2
* Fixed: Some bugs in front-end
* Added: Text instead of results feature
* Added: Upgrade dialog before deactivating

= 1.0.8 =
* Fixed: Some bugs in dashboard
* Added: Hide results feature

= 1.0.7 =
* Fixed: Some prolems in dashboard
* Updated: Results list-table
* Added: Settings active tab saving

= 1.0.6 =
* Fixed: Some prolems in front-end
* Added: Background image setting
* Added: Live preview

= 1.0.5 =
* Fixed: Some prolems in front-end
* Fixed: WordPress 2019 theme conflict
* Fixed: Tooltips for Category
* Added: Category implementation
* Added: Some options for Category

= 1.0.4 =
* Tested: Compatible up to: 5.0

= 1.0.3 =
* Fixed: Widget width issue
* Fixed: Labels issue
* Added: Translation ability

= 1.0.2 =
* Fixed: Some prolems in dashboard
* Fixed: PHP version conflict

= 1.0.1 =
* Added: Default poll with type Rating
* Added: Apply button when user trying to add new poll
* Fixed: Activation problem