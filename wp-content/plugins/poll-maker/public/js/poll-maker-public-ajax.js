(function ($) {
    String.prototype.stripSlashes = function () {
        return this.replace(/\\(.)/mg, "$1");
    }
    $.fn.serializeFormJSON = function () {
        let o = {},
            a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    function socialBtnAdd(formId) {
        let socialDiv = $("<div class='apm-social-btn'></div>")
        socialDiv.append("<a class='fb-share-button ays-share-btn ays-share-btn-branded ays-share-btn-facebook'"+
                                    "title='Share on Facebook'>"+
                                    "<span class='ays-share-btn-icon'></span>"+
                                    "<span class='ays-share-btn-text'>Facebook</span>"+
                                "</a>")
        socialDiv.append("<a class='twt-share-button ays-share-btn ays-share-btn-branded ays-share-btn-twitter'"+
                                "title='Share on Twitter'>"+
                                "<span class='ays-share-btn-icon'></span>"+
                                "<span class='ays-share-btn-text'>Twitter</span>"+
                            "</a>")
        socialDiv.append("<a class='linkedin-share-button ays-share-btn ays-share-btn-branded ays-share-btn-linkedin'"+
                                "title='Share on LinkedIn'>"+
                                "<span class='ays-share-btn-icon'></span>"+
                                "<span class='ays-share-btn-text'>LinkedIn</span>"+
                            "</a>");
        $("#"+formId).append(socialDiv)
        $(document).on('click', '.fb-share-button', function (e) {
            window.open('https://www.facebook.com/sharer/sharer.php?u=' + window.location.href,
                'facebook-share-dialog',
                'width=650,height=450'
            );
            return false;
        })
        $(document).on('click', '.twt-share-button', function (e) {
            window.open('https://twitter.com/intent/tweet?url=' + window.location.href,
                'twitter-share-dialog',
                'width=650,height=450'
            );
            return false;
        })
        $(document).on('click', '.linkedin-share-button', function (e) {
            window.open('https://www.linkedin.com/shareArticle?mini=true&url=' + window.location.href,
                'linkedin-share-dialog',
                'width=650,height=450'
            );
            return false;
        })
        setTimeout(function() {
            $("#"+formId+" .apm-social-btn").css('opacity', '1');
        }, 1000);
    }

    function loadEffect(formId, onOff) {
        let form = $("#"+formId),
            effect = form.attr('data-loading');
        switch (effect) {
            case 'blur':
                form.css({
                    WebkitFilter: onOff ? 'blur(5px)' : 'unset',
                    filter: onOff ? 'blur(5px)' : 'unset',
                })
                break;
            case 'load_gif':
                if (onOff) {
                    let loadSvg = '';
                    switch (form.attr('data-load-gif')) {
                        case 'plg_1':
                            loadSvg = '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="100%" height="100%" viewBox="0 0 24 30" style="enable-background:new 0 0 50 50;" xml:space="preserve">'+
                            '<rect x="0" y="0" width="4" height="10" fill="#333">'+
                              '<animateTransform attributeType="xml"'+
                                'attributeName="transform" type="translate"'+
                                'values="0 0; 0 20; 0 0"'+
                                'begin="0" dur="0.8s" repeatCount="indefinite" />'+
                            '</rect>'+
                            '<rect x="10" y="0" width="4" height="10" fill="#333">'+
                              '<animateTransform attributeType="xml"'+
                                'attributeName="transform" type="translate"'+
                                'values="0 0; 0 20; 0 0"'+
                                'begin="0.2s" dur="0.8s" repeatCount="indefinite" />'+
                            '</rect>'+
                            '<rect x="20" y="0" width="4" height="10" fill="#333">'+
                              '<animateTransform attributeType="xml"'+
                                'attributeName="transform" type="translate"'+
                                'values="0 0; 0 20; 0 0"'+
                                'begin="0.4s" dur="0.8s" repeatCount="indefinite" />'+
                            '</rect>'+
                        '</svg>';
                            break;
                        case 'plg_2':
                            loadSvg = '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"  width="100%" height="100%" viewBox="0 0 24 30" style="enable-background:new 0 0 50 50;" xml:space="preserve">'+
                            '<rect x="0" y="10" width="4" height="10" fill="#333" opacity="0.2">'+
                                '<animate attributeName="opacity" attributeType="XML" values="0.2; 1; .2" begin="0s" dur="0.7s" repeatCount="indefinite" />'+
                                '<animate attributeName="height" attributeType="XML" values="10; 20; 10" begin="0s" dur="0.7s" repeatCount="indefinite" />'+
                                '<animate attributeName="y" attributeType="XML" values="10; 5; 10" begin="0s" dur="0.7s" repeatCount="indefinite" />'+
                            '</rect>'+
                            '<rect x="8" y="10" width="4" height="10" fill="#333"  opacity="0.2">'+
                                '<animate attributeName="opacity" attributeType="XML" values="0.2; 1; .2"    begin="0.15s" dur="0.7s" repeatCount="indefinite" />'+
                                '<animate attributeName="height" attributeType="XML" values="10; 20; 10" begin="0.15s" dur="0.7s" repeatCount="indefinite" />'+
                                '<animate attributeName="y" attributeType="XML" values="10; 5; 10" begin="0.15s"   dur="0.7s" repeatCount="indefinite" />'+
                            '</rect>'+
                            '<rect x="16" y="10" width="4" height="10" fill="#333"  opacity="0.2">'+
                                '<animate attributeName="opacity" attributeType="XML" values="0.2; 1; .2" begin="0.3s" dur="0.7s" repeatCount="indefinite" />'+
                                '<animate attributeName="height" attributeType="XML" values="10; 20; 10" begin="0.3s" dur="0.7s" repeatCount="indefinite" />'+
                                '<animate attributeName="y" attributeType="XML" values="10; 5; 10" begin="0.3s" dur="0.7s" repeatCount="indefinite" />'+
                            '</rect>'+
                        '</svg>';
                            break;
                        case 'plg_3':
                            loadSvg = '<svg width="100%" height="100%" viewBox="0 0 105 105" xmlns="http://www.w3.org/2000/svg" fill="#000">'+
                            '<circle cx="12.5" cy="12.5" r="12.5">'+
                                '<animate attributeName="fill-opacity"'+
                                 'begin="0s" dur="0.9s"'+
                                 'values="1;.2;1" calcMode="linear"'+
                                 'repeatCount="indefinite" />'+
                            '</circle>'+
                            '<circle cx="12.5" cy="52.5" r="12.5" fill-opacity=".5">'+
                                '<animate attributeName="fill-opacity"'+
                                 'begin="100ms" dur="0.9s"'+
                                 'values="1;.2;1" calcMode="linear"'+
                                 'repeatCount="indefinite" />'+
                            '</circle>'+
                            '<circle cx="52.5" cy="12.5" r="12.5">'+
                                '<animate attributeName="fill-opacity"'+
                                 'begin="300ms" dur="0.9s"'+
                                 'values="1;.2;1" calcMode="linear"'+
                                 'repeatCount="indefinite" />'+
                            '</circle>'+
                            '<circle cx="52.5" cy="52.5" r="12.5">'+
                                '<animate attributeName="fill-opacity"'+
                                 'begin="600ms" dur="0.9s"'+
                                 'values="1;.2;1" calcMode="linear"'+
                                 'repeatCount="indefinite" />'+
                            '</circle>'+
                            '<circle cx="92.5" cy="12.5" r="12.5">'+
                                '<animate attributeName="fill-opacity"'+
                                 'begin="800ms" dur="0.9s"'+
                                 'values="1;.2;1" calcMode="linear"'+
                                 'repeatCount="indefinite" />'+
                            '</circle>'+
                            '<circle cx="92.5" cy="52.5" r="12.5">'+
                                '<animate attributeName="fill-opacity"'+
                                 'begin="400ms" dur="0.9s"'+
                                 'values="1;.2;1" calcMode="linear"'+
                                 'repeatCount="indefinite" />'+
                            '</circle>'+
                            '<circle cx="12.5" cy="92.5" r="12.5">'+
                                '<animate attributeName="fill-opacity"'+
                                 'begin="700ms" dur="0.9s"'+
                                 'values="1;.2;1" calcMode="linear"'+
                                 'repeatCount="indefinite" />'+
                            '</circle>'+
                            '<circle cx="52.5" cy="92.5" r="12.5">'+
                                '<animate attributeName="fill-opacity"'+
                                 'begin="500ms" dur="0.9s"'+
                                 'values="1;.2;1" calcMode="linear"'+
                                 'repeatCount="indefinite" />'+
                            '</circle>'+
                            '<circle cx="92.5" cy="92.5" r="12.5">'+
                                '<animate attributeName="fill-opacity"'+
                                 'begin="200ms" dur="0.9s"'+
                                 'values="1;.2;1" calcMode="linear"'+
                                 'repeatCount="indefinite" />'+
                            '</circle>'+
                        '</svg>';
                            break;
                        case 'plg_4':
                            loadSvg = '<svg width="100%" height="100%" viewBox="0 0 57 57" xmlns="http://www.w3.org/2000/svg"  stroke="#000">'+
                            '<g fill="none" fill-rule="evenodd">'+
                                '<g transform="translate(1 1)" stroke-width="2">'+
                                    '<circle cx="5" cy="50" r="5">'+
                                        '<animate attributeName="cy"'+
                                             'begin="0s" dur="2.2s"'+
                                             'values="50;5;50;50"'+
                                             'calcMode="linear"'+
                                             'repeatCount="indefinite" />'+
                                        '<animate attributeName="cx"'+
                                             'begin="0s" dur="2.2s"'+
                                             'values="5;27;49;5"'+
                                             'calcMode="linear"'+
                                             'repeatCount="indefinite" />'+
                                    '</circle>'+
                                    '<circle cx="27" cy="5" r="5">'+
                                        '<animate attributeName="cy"'+
                                             'begin="0s" dur="2.2s"'+
                                             'from="5" to="5"'+
                                             'values="5;50;50;5"'+
                                             'calcMode="linear"'+
                                             'repeatCount="indefinite" />'+
                                        '<animate attributeName="cx"'+
                                             'begin="0s" dur="2.2s"'+
                                             'from="27" to="27"'+
                                             'values="27;49;5;27"'+
                                             'calcMode="linear"'+
                                             'repeatCount="indefinite" />'+
                                    '</circle>'+
                                    '<circle cx="49" cy="50" r="5">'+
                                        '<animate attributeName="cy"'+
                                             'begin="0s" dur="2.2s"'+
                                             'values="50;50;5;50"'+
                                             'calcMode="linear"'+
                                             'repeatCount="indefinite" />'+
                                        '<animate attributeName="cx"'+
                                             'from="49" to="49"'+
                                             'begin="0s" dur="2.2s"'+
                                             'values="49;5;27;49"'+
                                             'calcMode="linear"'+
                                             'repeatCount="indefinite" />'+
                                    '</circle>'+
                                '</g>'+
                            '</g>'+
                        '</svg>';
                            break;
                        default:
                            loadSvg = '<svg version="1.1" id="loader-1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"'+
                            'width="100%" height="100%" viewBox="0 0 50 50" style="enable-background:new 0 0 50  50;" xml:space="preserve">'+
                                '<path fill="#000" d="M43.935,25.145c0-10.318-8.364-18.683-18.683-18.683c-10.318, 0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615c8.072,0, 14.615, 6.543,14.615,14.615H43.935z">'+
                                    '<animateTransform attributeType="xml"'+
                                        'attributeName="transform"'+
                                        'type="rotate"'+
                                        'from="0 25 25"'+
                                        'to="360 25 25"'+
                                        'dur="0.6s"'+
                                        'repeatCount="indefinite"/>'+
                                '</path>'+
                            '</svg>';
                    }
                    let layer = $('<div class="apm-opacity-layer-light">'+
                        '<div class="apm-loading-gif">'+
                            '<div class="apm-loader loader--style3">'+
                                loadSvg+
                            '</div>'+
                        '</div>'+
                    '</div>')
                    form.css({
                        position: 'relative'
                    })
                    form.append(layer)
                    layer.css('opacity', 1);
                } else {
                    $('.apm-opacity-layer-light').css('opacity', 0).empty()
                    setTimeout(function() {
                        $('.apm-opacity-layer-light').remove()
                    }, 500);
                }
                break;
            default:
                if (onOff) {
                    let layer = $('<div class="apm-opacity-layer-dark"></div>')
                    form.css({
                        position: 'relative'
                    })
                    form.append(layer)
                    layer.css('opacity', 1);
                } else {
                    $('.apm-opacity-layer-dark').css('opacity', 0)
                    setTimeout(function() {
                        $('.apm-opacity-layer-dark').remove()
                    }, 500);
                }
                break;
        }
    }

    function sortDate(rateCount, votesSum, answers, formId) {
        let form = $("#"+formId),
            sortable = form.attr('data-res-sort'),
            widths = [];
        for (let i = 0; i < rateCount; i++) {
            let answer = answers[i];
            widths[i] = {};
            let width = (answer.votes * 100 / votesSum).toFixed(0);
            widths[i].width = width;
            widths[i].index = i;
        }
        if (sortable === "ASC") {
            for (let i = 0; i < rateCount; i++) {
                for (let j = (i + 1); j < rateCount; j++) {
                    if (Number(widths[i].width) > Number(widths[j].width)) {
                        let temp = widths[i].width;
                        widths[i].width = widths[j].width;
                        widths[j].width = temp;
                        temp = widths[i].index;
                        widths[i].index = widths[j].index;
                        widths[j].index = temp;
                    }
                }
            }
        } else if (sortable === "DESC") {
            for (let i = 0; i < rateCount; i++) {
                for (let j = (i + 1); j < rateCount; j++) {
                    if (Number(widths[i].width) < Number(widths[j].width)) {
                        let temp = widths[i].width;
                        widths[i].width = widths[j].width;
                        widths[j].width = temp;
                        temp = widths[i].index;
                        widths[i].index = widths[j].index;
                        widths[j].index = temp;
                    }
                }
            }
        }
        return widths;
    }

    let apmIcons = {
        star: "<i class='ays_poll_far ays_poll_fa-star'></i>",
        star1: "<i class='ays_poll_fas ays_poll_fa-star'></i>",
        emoji: [
            "<i class='ays_poll_far ays_poll_fa-dizzy'></i>",
            "<i class='ays_poll_far ays_poll_fa-smile'></i>",
            "<i class='ays_poll_far ays_poll_fa-meh'></i>",
            "<i class='ays_poll_far ays_poll_fa-frown'></i>",
            "<i class='ays_poll_far ays_poll_fa-tired'></i>",
        ],
        emoji1: [
            "<i class='ays_poll_fas ays_poll_fa-dizzy'></i>",
            "<i class='ays_poll_fas ays_poll_fa-smile'></i>",
            "<i class='ays_poll_fas ays_poll_fa-meh'></i>",
            "<i class='ays_poll_fas ays_poll_fa-frown'></i>",
            "<i class='ays_poll_fas ays_poll_fa-tired'></i>",
        ],
        hand: [
            "<i class='ays_poll_far ays_poll_fa-thumbs-up'></i>",
            "<i class='ays_poll_far ays_poll_fa-thumbs-down'></i>"
        ],
        hand1: [
            "<i class='ays_poll_fas ays_poll_fa-thumbs-up'></i>",
            "<i class='ays_poll_fas ays_poll_fa-thumbs-down'></i>"
        ],
    };

    function showInfoForm($form) {
        $form.find('.ays_question, .apm-answers').fadeOut(0);
        $infoForm = $form.find('.apm-info-form');
        $infoForm.fadeIn();
        $form.find('.ays_finish_poll').val($infoForm.attr('data-text'));
        $form.find('.ays_finish_poll').attr('style', 'display:initial !important');
        $form.attr('data-info-form', '')
    }

    var emailValivatePattern = /^[a-zA-Z0-9\._-]+@[a-zA-Z0-9\._-]+\.\w{2,}$/;

    function voting(btnId , type) {
        if (typeof btnId == "undefined"){
            btnId = 0;
        }
        let btn ;
        if( btnId === 0 ){
            btn = $(this);
        }else{
            btn = btnId;
        }
        let seeRes = btn.attr('data-seeRes'),
            formId = btn.attr('data-form'),
            form = $("#"+formId),
            pollId = form.attr('data-id'),
            isRestart = form.attr('data-restart'),
            voteURLRedirection = form.attr('data-redirect-check'),
            voteRedirection = form.attr('data-redirection'),
            infoForm = form.attr('data-info-form');

        if (infoForm) {
            let data = form.parent().serializeFormJSON();
            if ('answer' in data)
            return showInfoForm(form);
            else if(!seeRes)
            return false;
        }
        let valid = true;
        form.find('.apm-info-form input[name]').each(function () {
            $(this).removeClass('ays_poll_shake');
            if ($(this).attr('data-required') == 'true' && $(this).val() == "" && !seeRes) {
                $(this).addClass('apm-invalid');
                $(this).addClass('ays_red_border');
                $(this).addClass('ays_poll_shake');
                valid = false;
            }
        });

        var email_val = $('[check_id="'+formId+'"]');
        if (email_val.attr('type') !== 'hidden' && email_val.attr('check_id') == formId) {
            if(email_val.val() != ''){
                if (!(emailValivatePattern.test(email_val.val())) && !seeRes) {
                    email_val.addClass('ays_red_border');
                    email_val.addClass('ays_poll_shake');
                    valid = false;
                }else{
                    email_val.addClass('ays_green_border');
                }
            }
        }
        
        if (!valid && !seeRes) {
            return false;
        }
        let data = form.parent().serializeFormJSON();
        if (!('answer' in data) && !seeRes) return;
        if (seeRes && ('answer' in data)) delete data['answer'];
        loadEffect(formId, true);
        btn.off();
        data.action = 'ays_finish_poll';
        data.poll_id = pollId;
        $.ajax({
            url: poll_maker_ajax_public.ajax_url,
            dataType: 'json',
            method:'post',
            data: data,
            success: function(res) {
                var answers_sounds = $("#"+formId).parent().find('.ays_poll_ans_sound').get(0);
                if(answers_sounds){
                    setTimeout(function() {
                        resetPlaying(answers_sounds);
                    }, 1000);
                }
                $("#"+formId+" .ays_poll_cb_and_a").hide();
                $("#"+formId+" .ays_poll_show_timer").hide();
                let delay = $('.ays-poll-main').find('div.box-apm[data-delay]').attr('data-delay');
                delayCountDown(delay);
                loadEffect(formId, false)
                form.parent().next().prop('disabled', false);
                $('.answer-' + formId).parent().remove(); //for removing apm-answer
                form.find('.ays_poll_passed_count').remove();
                form.find('.apm-info-form').remove();
                let redirectMessage = voteRedirection ? form.find('.redirectionAfterVote').clone(true) : '';
                $("#"+formId+" .apm-button-box").remove();
                let hideRes = form.attr('data-res')
                if (hideRes != 0) {
                    $("#"+formId+" .ays_question").remove();
                    $("#"+formId+" .hideResults").css("display", "block");
                } else {
                    form.append('<div class="results-apm"></div>');
                    let votesSum = 0;
                    let votesMax = 0;
                    let answer;
                    for ( answer in res.answers) {
                        votesSum = Math.abs(res.answers[answer].votes) + votesSum;
                        if (+res.answers[answer].votes > votesMax) {
                            votesMax = +res.answers[answer].votes
                        }
                    }
                    let answer2 = res.answers;
                    let widths = sortDate(res.answers.length, votesSum, answer2, formId);

                    //show votes count 
                    var showvotescounts = true;
                    if (res.styles.show_votes_count == 0) {
                        showvotescounts = false;
                    }

                    //show result percent 
                    var showrespercent = true;
                    if (res.styles.show_res_percent == 0) {
                        showrespercent = false;
                    }
                    
                    for (let i = 0; i < res.answers.length; i++) {
                        var rightAnswerCheck = (data.answer == res.answers[widths[i].index].id) ? 'ays_check' : '';
                        var starAnswerCheck = (data.answer == res.answers[widths[i].index].id) ? apmIcons.star1 : apmIcons.star;
                        var emojiAnswerCheck = (data.answer == res.answers[widths[i].index].id) ? apmIcons.emoji1 : apmIcons.emoji;
                        var handAnswerCheck = (data.answer == res.answers[widths[i].index].id) ? apmIcons.hand1 : apmIcons.hand;
                        let answer = res.answers;
                        let answerDiv = $('<div class="answer-title flex-apm"></div>'),
                            answerBar = $('<div class="answer-percent" data-percent="'+widths[i].width+'"></div>')
                        answerBar.css({
                            width: '1%'
                        })
                        let answerText = '';
                        switch (type) {
                            case 'choose':
                                answerText = $('<span class="answer-text '+rightAnswerCheck+'"></span>');
                                let htmlstr = answer[widths[i].index].answer.stripSlashes();
                                answerText.html(htmlstr);
                                break;
                            case 'rate':
                                switch (res.view_type) {
                                    case 'emoji':
                                        answerText = emojiAnswerCheck[res.answers.length / 2 + 1.5 - widths[i].index];
                                        break;

                                    case 'star':
                                        for (let j = 0; j <= widths[i].index; j++) {
                                            answerText += starAnswerCheck;
                                        }
                                        break;
                                }
                                answerText = $('<span class="answer-text">'+answerText+'</span>');
                                break;
                            case 'vote':
                                switch (res.view_type) {
                                    case 'hand':
                                        answerText = handAnswerCheck[widths[i].index];
                                        break;

                                    case 'emoji':
                                        answerText = emojiAnswerCheck[2 * widths[i].index + 1]
                                        break;
                                }
                                answerText = $('<span class="answer-text">'+answerText+'</span>');
                                break;
                        }
                        
                        let answerVotes = $('<span class="answer-votes"></span>');
                        if(showvotescounts){
                          answerVotes.text(answer[widths[i].index].votes);
                        }
                        answerDiv.append(answerText).append(answerVotes).appendTo("#"+formId+" .results-apm");
                        $("#"+formId+" .results-apm").append(answerBar);
                        $('.ays_res_mess').fadeIn();
                        $('.redirectionAfterVote').show();

                    }
                    setTimeout(function() {
                        form.find('.answer-percent').each(function () {
                            let percent = $(this).attr('data-percent')
                            $(this).css({
                                width: (percent || 1) + '%'
                            });
                            if (showrespercent) {
                                var aaa = $(this);
                                setTimeout(function() {
                                    aaa.text(percent > 5 ? percent + '%' : '')
                                }, 200);
                            }
                        });
                        form.parents('.ays_poll_category-container').find('.ays-poll-next-btn').prop('disabled', false);
                        var vvv = form.parents('.ays_poll_category-container').data("var");
                        window['showNext'+vvv] = true;

                    }, 100);
                }
                if (form.attr('data-show-social') == 1) {
                    socialBtnAdd(formId)
                }
                if (voteURLRedirection == 1) {
                    let url = form.attr('data-url-href');
                    form.append(redirectMessage)
                    if (url !== '') {
                        $('.redirectionAfterVote').text('You will be redirected');
                        setTimeout(function() {
                            location.href = url
                        });
                    }else if(voteRedirection == 1){
                        let url = form.attr('data-href');
                        let delay = +form.attr('data-delay')
                        form.append(redirectMessage)                    
                        setTimeout(function() {
                            location.href = url
                        }, delay * 1000);
                    }else{
                        $('.redirectionAfterVote').hide();
                    }                    
                }else{
                    voteURLRedirection = false;
                }
                if (voteRedirection == 1 && voteURLRedirection == false) {
                    let url = form.attr('data-href');
                    let delay = +form.attr('data-delay')
                    form.append(redirectMessage)                    
                    setTimeout(function() {
                        location.href = url
                    }, delay * 1000);
                }
                if (isRestart == 'true') {
                    showRestart(formId);
                }
            },
            error: function () {
                loadEffect(formId, false)
                $(".user-form-"+formId).fadeOut()
                form.parent().next().prop('disabled', false);
                $('.answer-' + formId).parent().parent().find('.apm-button-box').remove();
                $('.answer-' + formId).remove()
                btn.remove()
                $("#"+formId+" .ays_question").text("Something went wrong. Please reload page.")
            }
        });

    }

    function showRestart(formId) {
        let restartBtn = $('<div class="apm-button-box"><input type="button" class="btn ays-poll-btn btn-restart" onclick="location.reload()" value="Restart"></div>');
        $("#"+formId).append(restartBtn);
    }

    $(document).on('click', '.ays-poll-btn.choosing-btn', function () {
        voting( $(this), 'choose' );
    });
    $(document).on('click', '.ays-poll-btn.rating-btn', function () {
        voting( $(this), 'rate' );
    });
    $(document).on('click', '.ays-poll-btn.voting-btn', function () {
        voting( $(this), 'vote' );
    });

    $(document).on('change', '.apm-answers-without-submit input', function () {
        if ($(this).parent().hasClass('apm-rating')) {
            voting($(this).parents('.box-apm').find('.apm-button-box input.ays_finish_poll'), 'rate')
        } else if ($(this).parent().hasClass('apm-voting')) {
            voting($(this).parents('.box-apm').find('.apm-button-box input.ays_finish_poll'), 'vote')
        } else if ($(this).parent().hasClass('apm-choosing')) {
            voting($(this).parents('.box-apm').find('.apm-button-box input.ays_finish_poll'), 'choose')
        }
    })

    function delayCountDown(sec) {
      delaySec = parseInt(sec);
      let intervalSec = setInterval(function() {
                  if (delaySec > 0) {
                      delaySec--;
                      $('.ays-poll-main').find('p.redirectionAfterVote span').text(delaySec);
                  } else {
                      clearInterval(intervalSec);
                  }
              }, 1000);
    }

    function resetPlaying(audelems) {
            audelems.pause();
            audelems.currentTime = 0;
    }
})(jQuery);