<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://ays-pro.com/
 * @since      1.0.0
 *
 * @package    Poll_Maker_Ays
 * @subpackage Poll_Maker_Ays/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Poll_Maker_Ays
 * @subpackage Poll_Maker_Ays/public
 * @author     Poll Maker Team <info@ays-pro.com>
 */
class Poll_Maker_Ays_Public {

    /**
     * The settings of this plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      Poll_Maker_Settings_Actions object $settings The current settings of this plugin.
     */
    protected $settings;
	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string $plugin_name The ID of this plugin.
	 */
	private $plugin_name;
	private static $p;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string $version The current version of this plugin.
	 */
	private $version;
	private static $v;

	/**
	 * The instance of this plugin public class.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      Poll_Maker_Ays_Public object.
	 */
	private static $instance = null;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @param string $plugin_name The name of the plugin.
	 * @param string $version The version of this plugin.
	 *
	 * @since    1.0.0
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version     = $version;
		self::$p           = $plugin_name;
		self::$v           = $version;

		add_shortcode('ays_poll', array($this, 'ays_poll_generate_shortcode'));
        $this->settings = new Poll_Maker_Settings_Actions($this->plugin_name);
        add_shortcode('ays_poll_all', array($this, 'ays_poll_all_generate_shortcode'));
        add_shortcode('ayspoll_results', array($this, 'ays_poll_results_generate_shortcode'));
    }
     
	/**
	 * Get instance of this class. Singleton pattern.
	 *
	 * @since    1.4.0
	 */
	public static function get_instance() {
		if (self::$instance == null) {
			self::$instance = new Poll_Maker_Ays_Public(self::$p, self::$v);
		}

		return self::$instance;
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Poll_Maker_Ays_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Poll_Maker_Ays_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		
		wp_enqueue_style('ays_poll_font_awesome', 'https://use.fontawesome.com/releases/v5.6.3/css/all.css', array(), '5.6.3', 'all');
		// wp_enqueue_style('ays_poll_fa_v4_shims', plugin_dir_url(__FILE__) . 'css/font_awesome_v4-shims.css', array(), '5.6.3', 'all');
		/*wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/poll-maker-ays-public.css', array(), $this->version, 'all');*/

	}

	public function enqueue_styles_early(){
        wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/poll-maker-ays-public.css', array(), $this->version, 'all');
    }

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Poll_Maker_Ays_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Poll_Maker_Ays_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		//wp_enqueue_script($this->plugin_name . '-font-awesome', "https://use.fontawesome.com/releases/v5.5.0/js/all.js", array('jquery'), $this->version, true);

		wp_enqueue_script("jquery");
		wp_enqueue_script("jquery-effects-core");
		wp_enqueue_script($this->plugin_name . '-ajax-public', plugin_dir_url(__FILE__) . 'js/poll-maker-public-ajax.js', array('jquery'), $this->version, true);
		wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/poll-maker-ays-public.js', array('jquery'), $this->version, false);
		wp_enqueue_script($this->plugin_name.'-category', plugin_dir_url(__FILE__) . 'js/poll-maker-public-category.js', array('jquery'), $this->version, false);
		wp_localize_script($this->plugin_name . '-ajax-public', 'poll_maker_ajax_public', array('ajax_url' => admin_url('admin-ajax.php')));
	}

	public function ays_poll_results_generate_shortcode($attr) {
		ob_start();

		$this->enqueue_styles();
        $this->enqueue_scripts();

		global $wpdb;
		$id = absint(intval($attr['id']));
		$answ_table = esc_sql($wpdb->prefix."ayspoll_answers");
		$polls_table = esc_sql($wpdb->prefix."ayspoll_polls");
		$ans_sql  = "SELECT * FROM ".$answ_table." WHERE poll_id =%d ORDER BY votes DESC";
		$poll_answers = $wpdb->get_results(
			   	  	$wpdb->prepare( $ans_sql, $id),
			   	  	'ARRAY_A'
				  );

		$poll_sql  = "SELECT * FROM ".$polls_table." WHERE id =%d";
		$polls = $wpdb->get_row(
			   	  	$wpdb->prepare( $poll_sql, $id),
			   	  	'ARRAY_A'
				  );

		if ($polls == null) {
			$content = '<p style="text-align:center;">No ratings yet</p>';
		}else{
			$show_title = isset($polls['show_title']) && $polls['show_title'] == 0 ? false : true;
			$poll_title = isset($polls['title']) ? $polls['title'] : '';
			$votes_count = $this->get_poll_results_count_by_id($id);
			$poll = $this->get_poll_by_id($id);
			$polls_options = $poll['styles'];
	        if (intval($votes_count['res_count']) > 0) {
				$one_percent = 100/intval($votes_count['res_count']);
	        }else{
	        	$one_percent = 1;
	        }

			$content = '<div class="ays-poll-main" style="border:2px solid; background-color: '.$polls_options['bg_color'].';color: '.$polls_options['main_color'].';" id="ays-poll-container-'.htmlentities($id).'">';

			if($show_title){
				$content .= '<div class="apm-title-box">
								<h5 style="text-align:center;">'.stripslashes($poll_title).'</h5>
							</div>';
			}
							
			$content .= '<div class="ays_question">
							<p>'.stripslashes($polls['question']).'</p>
						 </div>';
			if ($votes_count['res_count'] == 0) {
				$content .= '<p style="text-align:center; margin: 0;">No ratings yet</p>';	 	
			}

			 $content .= '<div class="results-apm">';

			 foreach ($poll_answers as $ans_key => $ans_val) {
			 	$percent = round($one_percent*intval($ans_val['votes']));
			 	if ($percent == 0) {
			 		$perc_cont = '';
			 	}else{
			 		$perc_cont = $percent.' %';
			 	}
			 	switch ($polls['type']) {
			 		case 'choosing':
			 			$content .= '<div class="answer-title flex-apm">
			 							<span class="answer-text">'.stripslashes($ans_val['answer']).'</span>
			 							<span class="answer-votes">'.$ans_val['votes'].'</span>
									</div>
									<div class="answer-percent" style="width: '.$percent.'%; background-color: '.$polls_options['main_color'].'; color: '.$polls_options['bg_color'].';">'.$perc_cont.'</div>';
			 			break;

			 		case 'rating':
			 			switch ($polls['view_type']) {
					 		case 'star':
					 			$star_type  = '';
					 			for ($i=0; $i < intval($ans_val['answer']); $i++) { 
					 				$star_type .= '<i class="ays_poll_far ays_poll_fa-star far"></i>';
					 			}
					 			$content .= '<div class="answer-title flex-apm">
					 							<span class="answer-text">'.$star_type.'</span>
					 							<span class="answer-votes">'.$ans_val['votes'].'</span>
											</div>
											<div class="answer-percent" style="width: '.$percent.'%; background-color: '.$polls_options['main_color'].'; color: '.$polls_options['bg_color'].';">'.$perc_cont.'</div>';
					 			break;
					 		
					 		case 'emoji':
					 			$emojy_type  = '';
					 			
				 				switch (intval($ans_val['answer'])) {
				 					case 1:
				 						$emojy_type .= '<i class="ays_poll_far ays_poll_fa-tired far"></i>';
				 						break;
				 					case 2:
				 						$emojy_type .= '<i class="ays_poll_far ays_poll_fa-frown far"></i>';
				 						break;
				 					case 3:
				 						$emojy_type .= '<i class="ays_poll_far ays_poll_fa-meh far"></i>';
				 						break;
				 					case 4:
				 						$emojy_type .= '<i class="ays_poll_far ays_poll_fa-smile far"></i>';
				 						break;
				 					case 5:
				 						$emojy_type .= '<i class="ays_poll_far ays_poll_fa-dizzy far"></i>';
				 						break;
				 					default:
				 						break;
				 				}

					 			$content .= '<div class="answer-title flex-apm">
					 							<span class="answer-text">'.$emojy_type.'</span>
					 							<span class="answer-votes">'.$ans_val['votes'].'</span>
											</div>
											<div class="answer-percent" style="width: '.$percent.'%; background-color: '.$polls_options['main_color'].'; color: '.$polls_options['bg_color'].';">'.$perc_cont.'</div>';

								break;
							default:										
								break;
			 			}

		 				break;

			 		case 'voting':
			 			switch ($polls['view_type']) {
					 		case 'hand':
					 			$hand_type  = '';
					 			if (intval($ans_val['answer'] == 1)) {
					 				$hand_type = '<i class="ays_poll_far ays_poll_fa-thumbs-up far"></i>';
					 			}else{
					 				$hand_type = '<i class="ays_poll_far ays_poll_fa-thumbs-down far"></i>';
					 			}
					 			$content .= '<div class="answer-title flex-apm">
					 							<span class="answer-text">'.$hand_type.'</span>
					 							<span class="answer-votes">'.$ans_val['votes'].'</span>
											</div>
											<div class="answer-percent" style="width: '.$percent.'%; background-color: '.$polls_options['main_color'].'; color: '.$polls_options['bg_color'].';">'.$perc_cont.'</div>';
					 			break;
					 		
					 		case 'emoji':
					 			$emojy_type  = '';
					 			if (intval($ans_val['answer'] == 1)) { 
					 				$emojy_type = '<i class="ays_poll_far ays_poll_fa-smile far"></i>';
					 			}else{
					 				$emojy_type = '<i class="ays_poll_far ays_poll_fa-frown far"></i>';
					 			}
					 			$content .= '<div class="answer-title flex-apm">
					 							<span class="answer-text">'.$emojy_type.'</span>
					 							<span class="answer-votes">'.$ans_val['votes'].'</span>
											</div>
											<div class="answer-percent" style="width: '.$percent.'%; background-color: '.$polls_options['main_color'].'; color: '.$polls_options['bg_color'].';">'.$perc_cont.'</div>';

								break;
							default:										
								break;
			 			}

			 			break;
		 			default:										
						break;
			 	}
	 			
			 }

			$content .= '</div>
						</div>';
		}

        echo $content;

		return str_replace(array("\r\n", "\n", "\r"), '', ob_get_clean());
	}

	public function ays_poll_all_generate_shortcode() {
		ob_start();
		global $wpdb;
		$poll_table = esc_sql($wpdb->prefix."ayspoll_polls");
		$sql  = "SELECT id FROM ".$poll_table;
		$poll = $wpdb->get_results($sql, 'ARRAY_A');
		
		$this->enqueue_styles();
        $this->enqueue_scripts();

		foreach ($poll as $poll_id) {
			$this->show_poll($poll_id);
		}

		return str_replace(array("\r\n", "\n", "\r"), '', ob_get_clean());
	}

	public function ays_poll_generate_shortcode( $attr ) {
		ob_start();

		$this->enqueue_styles();
        $this->enqueue_scripts();

		$this->show_poll($attr);

		return str_replace(array("\r\n", "\n", "\r"), '', ob_get_clean());
	}

	public function show_poll( $attr ) {
		if (isset($attr['id'])) {
			return $this->ays_poll_generate_html($attr['id']);
		} elseif (isset($attr['cat_id'])) {
			return $this->ays_poll_category_generate_html($attr);
		}
	}

	public function ays_poll_generate_html( $poll_id, $echo = true, $width = -1 ) {

		if (!isset($poll_id) || null == $poll_id) {
			return "";
		}

		$id   = absint($poll_id);
		$poll = $this->get_poll_by_id($id);
		if (empty($poll)) {
			return "";
		}

		$this_poll_id = uniqid("ays-poll-id-");
		$options      = $poll['styles'];

		if (isset($options['published']) && intval($options['published']) === 0) {
			return "";
		}

		$info_form = !empty($options['info_form']) && !empty($options['fields']);
		$info_form_title = !empty($options['info_form_title']) ? $options['info_form_title'] : "<h5>" . __("Please fill out the form:", $this->plugin_name) . "</h5>";
		$fields          = !empty($options['fields']) ? explode(",", $options['fields']) : array();
		$required_fields = !empty($options['required_fields']) ? explode(",", $options['required_fields']) : array();

		$is_expired    = false;
		$is_start_soon = false;
		$startDate = '';
		$endDate = '';
		$current_time = strtotime(current_time( "Y:m:d H:i:s" ));
		if (isset($options['active_date_check']) && !empty($options['active_date_check'])) {
			if (isset($options['activeInterval']) && isset($options['deactiveInterval'])) {
				if (isset($options['activeIntervalSec']) && !empty($options['activeIntervalSec'])) {
					$startDate = strtotime($options['activeInterval']." ".$options['activeIntervalSec']);
					$startDate_atr = $startDate - $current_time;			
				}
				else{
					$startDate = strtotime($options['activeInterval']);
					$startDate_atr = $startDate - $current_time;
				}

				if (isset($options['deactiveIntervalSec']) && !empty($options['deactiveIntervalSec'])) {
					$endDate   = strtotime($options['deactiveInterval']." ".$options['deactiveIntervalSec']);
					$endDate_atr = $endDate - $current_time;
				}
				else{
					$endDate   = strtotime($options['deactiveInterval']);
					$endDate_atr = $endDate - $current_time;
				}

				if ($startDate > $current_time) {
					$is_start_soon = true;
				}
								
				if ($startDate > $current_time || $endDate < $current_time) {
					$is_expired = true;
				}
			}
		}

		$poll_directions = isset($options['poll_direction']) && $options['poll_direction'] != '' ? $options['poll_direction'] : 'ltr';
		
		switch ($poll_directions) {
			case 'ltr':
				$poll_direction = 'ltr';
				break;

			case 'center':
				$poll_direction = 'center';
				break;

			case 'rtl':
				$poll_direction = 'rtl';
				break;
			default:
				$poll_direction = 'ltr';
				break;
		}

		$load_effect = isset($options['load_effect']) ? $options['load_effect'] : "opacity";
		$load_gif    = isset($options['load_effect']) && $options['load_effect'] == 'load_gif' && isset($options['load_gif']) ? $options['load_gif'] : "";
		$result_sort = isset($options['result_sort_type']) ? $options['result_sort_type'] : "none";
		if (isset($options['show_social']) && $options['show_social'] == 1) {
			$show_social = true;
		} else {
			$show_social = false;
		}

		$without_vote = isset($options['enable_vote_btn']) && $options['enable_vote_btn'] == 0 ? 'apm-answers-without-submit' : "";

		if (isset($options['hide_results']) && $options['hide_results'] == 1) {
			$hide_results = "1";
			if (!empty($options['hide_results_text'])) {
				$hide_results_text = wpautop($options['hide_results_text']);
			} else {
				$hide_results_text = __("Thanks for your answer", $this->plugin_name);
			}
		} else {
			$hide_results      = "0";
			$hide_results_text = "";
		}

		if (isset( $options['redirect_after_submit'] ) && $options['redirect_after_submit'] == 1) {
			if (isset($options['redirect_users']) && $options['redirect_users'] != 0 && !empty($options['redirect_after_vote_url'])) {
				$redirect_after_vote_url = stripslashes($options['redirect_after_vote_url']);
				$redirect_users          = $options['redirect_users'];
				$redirect_delay          = $options['redirect_after_vote_delay'];
			}else{
				$redirect_after_vote_url = '';
				$redirect_users          = 0; 
				$redirect_delay          = 0;
			}
			$redirect_url_href       = '';
			$redirect_url_checked    = $options['redirect_after_submit'];
			$redirect_after_vote     = "<p class='redirectionAfterVote'>" . __("You will be redirected ".($redirect_delay <= 0 ? "" : " after <span>".$redirect_delay."</span> seconds "), $this->plugin_name) . "</p>";
		}elseif (isset($options['redirect_users']) && $options['redirect_users'] != 0 && !empty($options['redirect_after_vote_url'])) {
			$redirect_after_vote_url = stripslashes($options['redirect_after_vote_url']);
			$redirect_url_href       = '';
			$redirect_users          = $options['redirect_users'];
			$redirect_delay          = $options['redirect_after_vote_delay'];
			$redirect_url_checked    = 0;
			$redirect_after_vote     = "<p class='redirectionAfterVote'>" . __("You will be redirected ".($redirect_delay <= 0 ? "" : " after <span>".$redirect_delay."</span> seconds "), $this->plugin_name) . "</p>";
		} else {
			$redirect_after_vote_url = '';
			$redirect_url_href       = '';
			$redirect_users          = 0;
			$redirect_delay          = 0;
			$redirect_url_checked    = 0;
			$redirect_after_vote     = "";
		}

		$limit_users = 0;
		$limitusers = isset($options['limit_users']) ? intval($options['limit_users']) : 0;
		$load_poll   = false;
		$emoji       = array(
			"<i class='ays_poll_far ays_poll_fa-dizzy'></i>",
			"<i class='ays_poll_far ays_poll_fa-smile'></i>",
			"<i class='ays_poll_far ays_poll_fa-meh'></i>",
			"<i class='ays_poll_far ays_poll_fa-frown'></i>",
			"<i class='ays_poll_far ays_poll_fa-tired'></i>",
		);

		$bg_color        	= $options['bg_color'];
		$bg_image        	= isset($options['bg_image']) ? $options['bg_image'] : '';
		$main_color      	= $options['main_color'];
		$text_color      	= $options['text_color'];
		$icon_color      	= $options['icon_color'];
		$answer_bg_color 	= isset($options['answer_bg_color']) ? $options['answer_bg_color'] : 'transparent';
		$answer_border_side = isset($options['answer_border_side']) ? $options['answer_border_side'] : 'all_sides';
		$title_bg_color  	= isset($options['title_bg_color']) ? $options['title_bg_color'] : 'transparent';

		$enable_pass_count 	= (isset($options['enable_pass_count']) && $options['enable_pass_count'] == 'on') ? true : false;

		$poll_theme = isset($poll['theme_id']) && absint($poll['theme_id']) == 3 ? 'ays-minimal-theme' : '';

		if ($enable_pass_count) {
			$poll_result_reports = $this->get_poll_results_count_by_id($id);
			$poll_result_reports = "<span class='ays_poll_passed_count'><i class='ays_poll_fa ays_poll_fa-users' aria-hidden='true'></i> " . $poll_result_reports['res_count'] . "</span>";
		} else {
			$poll_result_reports = '';
		}
		if ($width < 0) {
			$poll_width = $options['width'] > 0 ? $options['width'] . "px" : "100%";
		} elseif ($width == 0) {
			$poll_width = "100%";
		} else {
			$poll_width = $width . "px";
		}

		$show_res_btn_sch = (isset($options['show_result_btn_schedule']) && $options['show_result_btn_schedule'] == 1) ? true : false;
		$see_result_button = '';
		$see_result_button = wp_nonce_field('ays_finish_poll', 'ays_finish_poll') . "<div class='apm-button-box'>";
		$see_result_button .= "<input type='button' class='btn ays-poll-btn {$poll['type']}-btn' data-form='$this_poll_id' value='" . (isset($options['see_res_btn_text']) && $options['see_res_btn_text'] != '' ? stripslashes($options['see_res_btn_text']) : __("See Results", $this->plugin_name)) . "' data-seeRes='true' >";
		$see_result_button .= '</div>';

		/* 
         * Poll container background gradient
         * 
         */
        
        // Checking exists background gradient option
        $options['enable_background_gradient'] = (!isset($options['enable_background_gradient'])) ? "off" : $options['enable_background_gradient'];
        
        if(isset($options['background_gradient_color_1']) && $options['background_gradient_color_1'] != ''){
            $background_gradient_color_1 = $options['background_gradient_color_1'];
        }else{
            $background_gradient_color_1 = "#103251";
        }

        if(isset($options['background_gradient_color_2']) && $options['background_gradient_color_2'] != ''){
            $background_gradient_color_2 = $options['background_gradient_color_2'];
        }else{
            $background_gradient_color_2 = "#607593";
        }

        if(isset($options['poll_gradient_direction']) && $options['poll_gradient_direction'] != ''){
            $poll_gradient_direction = $options['poll_gradient_direction'];
        }else{
            $poll_gradient_direction = 'vertical';
        }
        switch($poll_gradient_direction) {
            case "horizontal":
                $poll_gradient_direction = "to right";
                break;
            case "diagonal_left_to_right":
                $poll_gradient_direction = "to bottom right";
                break;
            case "diagonal_right_to_left":
                $poll_gradient_direction = "to bottom left";
                break;
            default:
                $poll_gradient_direction = "to bottom";
        }

        // Poll container background gradient enabled/disabled        
        if(isset($options['enable_background_gradient']) && $options['enable_background_gradient'] == "on"){
            $enable_background_gradient = true;
        }else{
            $enable_background_gradient = false;
        }

       	if( isset($bg_image) && $bg_image != false){
            $poll_styles =  "background-image: url('".$bg_image."');";
        }elseif($enable_background_gradient) {
            $poll_styles =  "background-image: linear-gradient($poll_gradient_direction, $background_gradient_color_1, $background_gradient_color_2);";
        }elseif (isset($bg_color)) {
        	$poll_styles = "background-color: ".$bg_color.";";
        }
        else{

        	$poll_styles = "background-image: unset;";
        }

        $options['enable_answer_style'] = isset($options['enable_answer_style']) ? $options['enable_answer_style'] : 'on';

        $answer_style = $options['enable_answer_style'] == 'on' ? true : false;

        $disable_answer_hover = isset($options['disable_answer_hover']) && $options['disable_answer_hover'] == 1 ? 'disable_hover' : 'ays_enable_hover';

		$content = "<style>

        .$this_poll_id.box-apm {
            width: $poll_width;
            margin: 0 auto !important;
            border-style: {$options['border_style']};
            border-color: $main_color;
            border-radius: " . ((isset($options['border_radius']) && !empty($options['border_radius'])) ? (int) $options['border_radius'] . 'px' : 0) . ";
            border-width: " . ((isset($options['border_width']) && $options['border_width'] != '') ? (int) $options['border_width'] . 'px' : '2px') . ";
            box-shadow: " . ((isset($options['box_shadow_color']) && !empty($options['box_shadow_color']) && isset($options['enable_box_shadow']) && $options['enable_box_shadow'] == 'on') ? $options['box_shadow_color'] . ' 0px 0px 10px 0px' : '') . ";".
           	$poll_styles."
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            max-width: 100%;
            position: relative;
            padding-bottom: 40px;
        }

        .$this_poll_id.ays-minimal-theme .apm-choosing{
		    display: flex;
		    align-items: center;
		}

		.$this_poll_id.ays-minimal-theme .apm-choosing input[type=radio]:checked + label, .$this_poll_id.ays-minimal-theme .apm-choosing label.ays_enable_hover:hover{
		    background-color: initial !important;
		    color: $main_color !important;
		    border-color: $main_color !important;
		    font-weight: initial !important;
		    margin:3px 0 !important;
		}

		.$this_poll_id.ays-minimal-theme .apm-choosing label{
			border-color: $text_color !important;
		    font-weight: initial !important;
		    margin:3px 0 !important;
		}

		.$this_poll_id.ays-minimal-theme .apm-choosing input[type='radio']{
		    display: block !important;
		}

		.$this_poll_id.ays-minimal-theme input[type='button'].ays-poll-btn:hover, .$this_poll_id.ays-minimal-theme input[type='button'].ays-poll-btn:focus{
			text-decoration: none;
		}

		.$this_poll_id.ays-minimal-theme input[type='button'].ays-poll-btn{
		    color: initial !important;
		    background: initial !important;
		    border: 1px solid $text_color;
		    border-radius: 3px;
		}

		.$this_poll_id.ays-minimal-theme .ays_poll_passed_count{
		    color: $text_color !important;
		    background: initial !important;
		    border: 1px solid $text_color;
		    border-radius: 3px;
		}

		.$this_poll_id.ays-minimal-theme .ays_poll_passed_count i.ays_poll_fa:before{
		    color: $text_color !important;		    
		}

        .$this_poll_id.ays-minimal-theme .answer-percent{
        	color: initial !important;
		}

        .$this_poll_id.box-apm span.ays_poll_passed_count{
            background-color: $text_color;
            color: $bg_color;
        }

        #$this_poll_id.box-apm span.ays_poll_passed_count i{
            color: $bg_color;
        }
        #$this_poll_id.box-apm .apm-title-box{
            background-color: $title_bg_color;
        }

        .$this_poll_id .answer-percent {
            background-color: $main_color;
            color: $bg_color !important;
        }
        .$this_poll_id .ays-poll-btn {
            background-color: $main_color !important;
            color: $bg_color !important;
            overflow: hidden;
            background: unset;
        }
        .$this_poll_id.box-apm * {
            color: $text_color;
        }
        .$this_poll_id.box-apm .apm-title-box h5 {
            color: $text_color;
            text-transform: inherit;
            font-family: inherit;
        }
        #".$this_poll_id.".box-apm i {
            color: $icon_color;
            font-size: {$options['icon_size']}px;
            font-style: normal;            
        }
        #".$this_poll_id.".box-apm i.ays_poll_far{            
            font-family: 'Font Awesome 5 Free';
        }";

        switch ($answer_border_side) {
        	case 'none':
        		$answer_border_type = "border: none";
        		break;
        	case 'all_sides':
        		$answer_border_type = "border: 1px solid ".$main_color;
        		break;
        	case 'top':
        		$answer_border_type = "border-top: 1px solid ".$main_color;
        		break;
        	case 'bottom':
        		$answer_border_type = "border-bottom: 1px solid ".$main_color;
        		break;
        	case 'left':
        		$answer_border_type = "border-left: 1px solid ".$main_color;
        		break;
        	case 'right':
        		$answer_border_type = "border-right: 1px solid ".$main_color;
        		break;
        	default:
        		$answer_border_type = "border: 1px solid ".$main_color;
        		break;
        }
        
        if ( $answer_style ) {
    	 	$content .= "
	        #".$this_poll_id.".choosing-poll label {
	            background-color: $answer_bg_color;
	            ".$answer_border_type.";
	            text-transform: inherit;
	        }";
        }

        $content .= "
        .$this_poll_id.choosing-poll input[type=radio]:checked + label,
        .$this_poll_id.choosing-poll label.ays_enable_hover:hover {
            background-color: $text_color !important;
            color: $bg_color;
        }
        .$this_poll_id.choosing-poll input[type=radio]:checked + label *,
        .$this_poll_id.choosing-poll label.ays_enable_hover:hover * {
            color: $answer_bg_color;
        }
        .$this_poll_id .apm-info-form input {
            border-color: $main_color;
        }
        button.ays-poll-next-btn:focus {
            background: unset;
            outline: none;
        }
        button.ays-poll-next-btn:disabled {
            cursor: not-allowed;
            background: dimgrey !important;
            color: white !important;
        }
        button.ays-poll-next-btn:enabled {
            cursor: pointer;
        }
        .$this_poll_id .apm-info-form input {
            color: $text_color !important;
            background-color: $answer_bg_color !important;
        } 
        .$this_poll_id div.apm-loading-gif .apm-loader svg path,
        .$this_poll_id div.apm-loading-gif .apm-loader svg rect {
            fill: $main_color;
        }" . (isset($load_gif) && $load_gif == 'plg_4' ?
				".$this_poll_id div.apm-loading-gif .apm-loader svg {
            stroke: $main_color;
        }

        .$this_poll_id.choosing-poll .ays_poll_cb_and_a,
        .$this_poll_id.choosing-poll .ays_poll_cb_and_a * {
            color: " . $this->hex2rgba($text_color) . ";
        }

        .$this_poll_id div.apm-loading-gif .apm-loader svg>g {
            fill: $bg_color;
        }"
				: "")
		           . "{$poll['custom_css']}
        </style>
        <script>
            var dataCss = {
                width: '$poll_width',
                fontSize: '16px',
                padding: '10px',
                margin: '0 auto',
                marginTop: '-1rem',
                borderStyle: '{$options['border_style']}',
                borderWidth: '2px',
                borderColor: '$main_color',
                background: '$bg_color',
                color: '$main_color',
                transition: '.3s ease',
                WebkitAppearance: 'none',
                appearance: 'none',
                };
            var hoverCss = {
                background: '$main_color',
                color: '$bg_color',
                borderColor: '$bg_color',
            };
        </script>"; 

        if ($poll_direction == 'center') {
        	$poll_direction_center = "style='text-align: center;'";
        	$poll_direction = 'ltr';
        }else{
        	$poll_direction_center = "";        	
        }

		// AV Show login form for not logged in users
        $options['show_login_form'] = isset($options['show_login_form']) ? $options['show_login_form'] : 'off';
        $show_login_form = (isset($options['show_login_form']) && $options['show_login_form'] == "on") ? true : false;
		$add_form = $show_login_form && !is_user_logged_in() ? "" : "<form style='margin-bottom: 0;'>";

		if(isset($options['show_create_date']) && $options['show_create_date'] == 1){
            $show_create_date = true;
        }else{
            $show_create_date = false;
        }

        if(isset($options['show_author']) && $options['show_author'] == 1){
            $show_author = true;
        }else{
            $show_author = false;
        }

        //Enabled ansers sound
        $enable_asnwers_sound =  isset($options['enable_asnwers_sound']) && $options['enable_asnwers_sound'] == 'on' ? true : false;
         $answers_sound = '';
         $answers_sound_class = '';
        if ($enable_asnwers_sound) {
        	$poll_settings = $this->settings;
        	$settings_options = ($poll_settings->ays_get_setting('options') === false) ? json_encode(array()) : $poll_settings->ays_get_setting('options');
            $setting_options = json_decode($settings_options, true);
            $answers_sound_path = isset($setting_options['answers_sound']) && !empty($setting_options['answers_sound']) ? $setting_options['answers_sound'] : false;
           
            if ($answers_sound_path != false) {
            	$answers_sound = "<audio id='ays_poll_ans_sound_".$id."' class='ays_poll_ans_sound' src='".$answers_sound_path."'></audio>";
         		$answers_sound_class = 'poll_answers_sound';

            }
        }        

		//AV show timer
        $activeDateCheck =  isset($options['active_date_check']) && !empty($options['active_date_check']) ? true : false;
        $activeDeactiveDateCheck =  isset($options['deactiveInterval']) && !empty($options['deactiveInterval']) ? true : false;
        $activeActiveDateCheck =  isset($options['activeInterval']) && !empty($options['activeInterval']) ? true : false;
        $show_timer_type = isset($options['ays_show_timer_type']) && !empty($options['ays_show_timer_type']) ? $options['ays_show_timer_type'] : 'countdown';
        $show_timer = '';
		if ( $activeDateCheck && $activeDeactiveDateCheck && !$is_start_soon) {
		    if (isset($options['ays_poll_show_timer']) && $options['ays_poll_show_timer'] == 1) {
				$show_timer .= "<div class='ays_poll_show_timer'>";
				if ($show_timer_type == 'countdown') {
					$show_timer .= '<p id="show_timer_countdown" data-timer_countdown="'.$endDate_atr.'"></p>';
				}else if ($show_timer_type == 'enddate') {
					$show_timer .= '<p id="show_timer_countdown">'.__('This Poll active until ',$this->plugin_name).gmdate('jS \of F Y H:i:s', intval($endDate)).'</p>';
				}
				$show_timer .= "</div>";
		    }
		}elseif ($activeDateCheck && $activeActiveDateCheck && $is_start_soon) {
			if (isset($options['ays_poll_show_timer']) && $options['ays_poll_show_timer'] == 1) {
				$show_timer .= "<div class='ays_poll_show_timer'>";
				if ($show_timer_type == 'countdown') {
					$show_timer .= '<p id="show_timer_countdown" data-timer_countdown="'.$startDate_atr.'"></p>';
				}else if ($show_timer_type == 'enddate') {
					$show_timer .= '<p id="show_timer_countdown">'.__('This Poll will start ',$this->plugin_name).gmdate('jS \of F Y H:i:s', intval($startDate)).'</p>';
				}
				$show_timer .= "</div>";
		    }
		}

		$show_cd_and_author = "<div class='ays_poll_cb_and_a'>";
        if($show_create_date){
            $poll_create_date = (isset($options['create_date']) && $options['create_date'] != '') ? $options['create_date'] : "0000-00-00 00:00:00";
            if(Poll_Maker_Ays_Admin::validateDate($poll_create_date)){
                $show_cd_and_author .= "<span>".__("Created on",$this->plugin_name)." </span><strong><time>".date("F d, Y", strtotime($poll_create_date))."</time></strong>";
            }else{
                $show_cd_and_author .= "";
            }
        }
        
        if($show_author){
            if(isset($options['author'])){
                if(is_array($options['author'])){
                    $author = $options['author'];
                }else{
                    $author = json_decode($options['author'], true);
                }
            }else{
                $author = array("name"=>"Unknown");
            }
            $user_id = 0;
            if(isset($author['id']) && intval($author['id']) != 0){
                $user_id = intval($author['id']);
            }
            $image = get_avatar($user_id, 32);
            if($author['name'] !== "Unknown"){
                if($show_create_date){
                    $text = __("By", $this->plugin_name);
                }else{
                    $text = __("Created by", $this->plugin_name);
                }
                $show_cd_and_author .= "<span>   ".$text." </span>".$image."<strong>".$author['name']."</strong>";
            }else{
                $show_cd_and_author .= "";
            }
        }

        $show_cd_and_author .= "</div>";

        $poll_login_form = "";
        if($show_login_form){
            $args = array(
                'echo' 		  => false,
                'form_id'     => 'ays_loginform_'.$this_poll_id,
                'id_username' => 'ays_user_login_'.$this_poll_id,
                'id_password' => 'ays_user_pass_'.$this_poll_id,
                'id_remember' => 'ays_rememberme_'.$this_poll_id,
                'id_submit'   => 'ays-submit_'.$this_poll_id
            );
            $poll_login_form = "<div class='ays_poll_login_form'>" . wp_login_form( $args ) . "</div>";
    	}

    	$ays_result_message = isset($options['result_message']) ? do_shortcode(wpautop(stripslashes($options['result_message']))) : '';
    	$result_message = isset($options['hide_result_message']) && $options['hide_result_message'] == 1 ? "<div class='apm-title-box ays_res_mess'>" . $ays_result_message . "</div>" : "";

    	$custom_class = isset($options['custom_class']) && $options['custom_class'] != '' ? $options['custom_class'] : "";

		$content .= "<div style='margin-bottom: 1rem; width:$poll_width' class='ays-poll-main ".$custom_class."' id='ays-poll-container-" . $id . "'>
        ".$add_form."
        ".$answers_sound."        
        <div
        $poll_direction_center
        dir='$poll_direction'
        data-loading='$load_effect'
        data-load-gif='$load_gif'
        data-show-social='$show_social'
        class='box-apm $poll_theme {$poll['type']}-poll $this_poll_id'
        id='$this_poll_id'
        data-res='$hide_results'
        data-res-sort='$result_sort'
        data-restart ='" . (isset($options['enable_restart_button']) && $options['enable_restart_button'] ? 'true' : 'false') . "'
        data-redirection = '$redirect_users'
        data-redirect-check = '".$redirect_url_checked."'
        data-url-href = '".$redirect_url_href."'
        data-href = '$redirect_after_vote_url'
        data-delay = '$redirect_delay'
        data-id='{$poll['id']}'
        data-info-form='$info_form'
        >" . $poll_result_reports;

		$content .= $show_cd_and_author;
		$content .= $show_timer;
		$content .= ($poll['show_title'] == 1) ? "<div class='apm-title-box'><h5>" . stripslashes($poll['title']) . "</h5></div>" : "";	
		$content .= "<div class='$this_poll_id ays_question'>" . do_shortcode(wpautop(stripslashes($poll['question']))) . "</div>";
		$content .= $poll['image'] ? "<div class='apm-img-box'><img class='ays-poll-img' src='{$poll['image']}'></div>" : "";
		$content .= "<div class='$this_poll_id hideResults'>" . $hide_results_text . "</div>";
		if (!$is_expired) {
			//CHECK IF ENABLED ONLY LOGGED IN USERS OPTION
			if (isset($options['enable_logged_users']) && $options['enable_logged_users'] == 1 && !is_user_logged_in()) {
				$logged_users_message = isset($options['enable_logged_users_message']) && $options['enable_logged_users_message'] != '' ? stripslashes($options['enable_logged_users_message']) : "<p>" . __('You must sign in for voting.', $this->plugin_name) . "</p>";

				$content .= "<div class='apm-need-sign-in'>".$logged_users_message."</div>";

				if($logged_users_message !== null){
		            if(!is_user_logged_in()){
						$content .= "<div class='apm-need-sign-in'>".$poll_login_form;
		            }
		        }
			} else {
				$load_poll = true;
				if (isset($options['enable_restriction_pass']) && $options['enable_restriction_pass'] == 1) {
					// Users role Aro start
			        global $wp_roles;
					$user      = wp_get_current_user();
			        $users_roles  = $wp_roles->role_names;
					$message   = (isset($options['restriction_pass_message']) && $options['restriction_pass_message'] != '') ? stripslashes($options['restriction_pass_message']) : ("<p>" . __('You not have permissions for voting.', $this->plugin_name) . "</p>");
			        $users_role = (isset($options['users_role']) && $options['users_role'] != '') ? $options['users_role'] : '';
			        $users_role = json_decode($users_role);
			        if(!empty($users_role)){
			            if (is_array($users_role)) {
			                foreach($users_role as $key => $role){
			                    if(in_array($role, $users_roles)){
			                        $users_role[$key] = array_search($role, $users_roles);
			                    }                        
			                }
			            }else{
			                if(in_array($users_role, $users_roles)){
			                    $users_role = array_search($users_role, $users_roles);
			                }
			            }
			            $is_user_role = false;
			            if(is_array($users_role)){
			                foreach($users_role as $role){                        
			                    if (in_array(strtolower($role), (array)$user->roles)) {
			                        $is_user_role = true;
			                        break;
			                    }
			                }                    
			            }else{
			                if (in_array(strtolower($users_role), (array)$user->roles)) {
			                    $is_user_role = true;
			                }
			            }

			            if (!$is_user_role) {
			                $content   .= "<div class='ays-poll-limitation'>$message</div>";
							$load_poll = false;
			            }
			        }
			    //Aro end
				}
				if ($load_poll) {
					$limit_users = 0;
					if ($limitusers) {
						global $wpdb;
						$report_table = esc_sql($wpdb->prefix."ayspoll_reports");
						$answ_table = esc_sql($wpdb->prefix."ayspoll_answers");
						if ((!empty($options['limit_users_method']) && $options['limit_users_method'] == 'ip') || empty($options['limit_users_method'])) {
							$user_ips = $this->get_user_ip();
							$user_ip = esc_sql($user_ips);					
							$args_ip = array($user_ip,$id);							
							$sql = "SELECT COUNT(*) 
									FROM ".$report_table." 
									JOIN ".$answ_table." 
									ON ".$answ_table.".id = ".$report_table.".answer_id 
									WHERE ".$report_table.".user_ip = %s 
									AND ".$answ_table.".poll_id = %d";
                			$limit_users = $wpdb->get_var(
						   	  	$wpdb->prepare( $sql, $args_ip)
						  	);
						} else {
							$user_id = is_user_logged_in() ? wp_get_current_user()->ID : 0;
							$args_id = array($user_id, $id);
							$sql = "SELECT COUNT(*) 
									FROM ".$report_table." 
									JOIN ".$answ_table." 
									ON ".$answ_table.".id = ".$report_table.".answer_id 
									WHERE ".$report_table.".user_id = %d 
									AND ".$answ_table.".poll_id = %d";
							if ($user_id > 0) {
								$limit_users = $wpdb->get_var(
							   	  	$wpdb->prepare( $sql, $args_id)
							  	);
							}else{
								$limit_users = 0;
							}
						}
					}
					if ($limit_users == 0) {
						$content .= "<div class='apm-answers $without_vote'>";
						switch ( $poll['type'] ) {
							case 'choosing':
								$randomize_answers = (isset($poll['styles']['randomize_answers']) && $poll['styles']['randomize_answers'] == 'on') ? true : false;
								$redirect_after_submit = (isset($poll['styles']['redirect_after_submit']) && $poll['styles']['redirect_after_submit'] == 1) ? 'redirect-after-vote-url' : '';
								
								if ($randomize_answers) {
									shuffle($poll['answers']);
								}
								foreach ( $poll['answers'] as $index => $answer ) {
									$content .= "<div class='apm-choosing answer-$this_poll_id'><input type='radio' name='answer' id='radio-$index-$this_poll_id' value='{$answer['id']}'>
                                            <label for='radio-$index-$this_poll_id' class='ays_label_poll ".$answers_sound_class." ".$redirect_after_submit." ".$disable_answer_hover."' answers-url='".$answer['redirect']."'>" . stripcslashes(html_entity_decode($answer['answer'])) . "</label>
                                        </div>";
								}
								break;
							case 'voting':
								switch ( $poll['view_type'] ) {
									case 'hand':
										foreach ( $poll['answers'] as $index => $answer ) {
											$content .= "<div class='apm-voting answer-$this_poll_id'><input type='radio' name='answer' id='radio-$index-$this_poll_id' value='{$answer['id']}'>
                                                    <label for='radio-$index-$this_poll_id' class='".$answers_sound_class."'>";
											$content .= ((int) $answer['answer'] > 0 ? "<i class='ays_poll_far ays_poll_fa-thumbs-up'></i>" : "<i class='ays_poll_far ays_poll_fa-thumbs-down'></i>") . "</label></div>";
										}
										break;
									case 'emoji':
										foreach ( $poll['answers'] as $index => $answer ) {
											$content .= "<div class='apm-voting answer-$this_poll_id'><input type='radio' name='answer' id='radio-$index-$this_poll_id' value='{$answer['id']}'>
                                                    <label for='radio-$index-$this_poll_id' class='".$answers_sound_class."'>";
											$content .= ((int) $answer['answer'] > 0 ? $emoji[1] : $emoji[3]) . "</label></div>";
										}
										break;
									default:
										break;
								}
								break;
							case 'rating':
								switch ( $poll['view_type'] ) {
									case 'star':
										foreach ( $poll['answers'] as $index => $answer ) {
											$content .= "<div class='apm-rating answer-$this_poll_id'><input type='radio' name='answer' id='radio-$index-$this_poll_id' value='{$answer['id']}'>
                                                    <label for='radio-$index-$this_poll_id'><i class='ays_poll_far ays_poll_fa-star ".$answers_sound_class."'></i></label></div>";
										}
										break;
									case 'emoji':
										foreach ( $poll['answers'] as $index => $answer ) {
											$content .= "<div class='apm-rating answer-$this_poll_id'><input type='radio' name='answer' id='radio-$index-$this_poll_id' value='{$answer['id']}'>
                                                    <label class='emoji ".$answers_sound_class."' for='radio-$index-$this_poll_id'>" . $emoji[(count($poll['answers']) / 2 - $index + 1.5)] . "</label></div>";
										}
										break;
									default:
										break;
								}
								break;
							default:
								break;
						}
						$content .= "</div>";
						$content .= "<div class='apm-cashed-fa'>";
						foreach ( $poll['answers'] as $index => $answer ) {
							$content .= "<div>
                                <i class='ays_poll_fas ays_poll_fa-star' style='font-size: 0'></i>
                            </div>";
						}
						$content .= "</div>";
						if ($info_form) {
							$content .= "\n
							<div class='apm-info-form' data-text='" . __("Send", $this->plugin_name) . "' style='display: none;'>
								$info_form_title
								<div class='amp-info-form-input-box'>
							";
							foreach ( $fields as $f ) {
								$required = array_search($f, $required_fields) !== false ? "true" : "false";
								switch ( $f ) {
									case "apm_email":
										$content .= "
										<input type='email' check_id='".$this_poll_id."' class='ays_animated_xms' name='$f' data-required='$required' placeholder='E-mail'>
										";
										break;
									case "apm_phone":
										$content .= "
										<input type='tel' class='ays_animated_xms' name='$f' data-required='$required' placeholder='Phone'>
										";
										break;
									default:
										$content .= "
										<input type='text' class='ays_animated_xms' name='$f' data-required='$required' placeholder='Name'>
										";
										break;
								}
							}
							$content .= "</div></div>";
						}
					} else {
						$content .= "<div class='ays-poll-limitation'>" . (isset($options['limitation_message']) && $options['limitation_message'] != '' ? stripslashes($options['limitation_message']) : ("<p>" . __("You have already voted.", $this->plugin_name) . "</p>")) . "</div>";
						if (isset($options['redirect_url']) && $options['redirect_url'] != '' && isset($options['redirection_delay']) && $options['redirection_delay'] != 0) {
							$content .= "<div class='apm-redirection apm-redirection-$this_poll_id'>
                                        <p data-id='$this_poll_id' data-href='" . stripslashes($options['redirect_url']) . "' data-delay='{$options['redirection_delay']}'>" . __('Redirecting after', $this->plugin_name)
							            . " <b>{$this->secondsToWords($options['redirection_delay'])}</b>
                                        </p>
                                    </div>";
						}
					}
				}
			}
			
			$show_res_button = !is_user_logged_in() && !$show_login_form ? true : false;
			
			if (is_user_logged_in() || $show_res_button ) {
				$content .= wp_nonce_field('ays_finish_poll', 'ays_finish_poll') . "<div class='apm-button-box'>";
				if (!empty($options['allow_not_vote']) || $limit_users > 0 ) {
					$content .= "<input type='button' class='btn ays-poll-btn {$poll['type']}-btn' data-form='$this_poll_id' value='" . (isset($options['see_res_btn_text']) && $options['see_res_btn_text'] != '' ? stripslashes($options['see_res_btn_text']) : __("See Results", $this->plugin_name)) . "' data-seeRes='true' >";
				}
				if ($limit_users == 0 && $load_poll) {
					$content .= "<input type='button' 
	                    name='ays_finish_poll'
	                    class='btn ays-poll-btn {$poll['type']}-btn ays_finish_poll'
	                    data-form='$this_poll_id'
	                    " . (!$load_poll ? "data-allow='false'" : "") .
					            'value="' . ((isset($options['btn_text']) && '' != $options['btn_text']) ? stripslashes($options['btn_text']) : __("Vote", $this->plugin_name)) . '"
	                    >';
					
				}
			}

			$content .= '</div>';
			$content .= $result_message;
			$content .= $redirect_after_vote;
		} elseif ($is_start_soon) {
			$poll_is_start_message = isset($options['active_date_message_soon']) ? stripslashes($options['active_date_message_soon']) : "<p>" . __('The poll will be available soon.', $this->plugin_name) . "</p>";
			$content              .= "<div class='apm_expired_poll'>".$poll_is_start_message."</div>";

				if ($show_res_btn_sch) {
					$content .=  $see_result_button;
				}
		} else {
			$expired_poll_message = isset($options['active_date_message']) ? stripslashes($options['active_date_message']) : "<p>" . __('The poll has expired.', $this->plugin_name) . "</p>";
			$content              .= "<div class='apm_expired_poll'>$expired_poll_message</div>";

				if ($show_res_btn_sch) {
					$content .=  $see_result_button;
				}
		}

		$content .= '</div></form></div>';
		if ($echo) {
			echo $content;
		} else {
			return $content;
		}
	}

	public function ays_poll_category_generate_html($attr, $echo = true){
		$id = absint($attr['cat_id']);
		global $wpdb;
		$poll_table = esc_sql($wpdb->prefix . "ayspoll_polls");
		$like = '%' . $wpdb->esc_like( $id ) . '%';
		$sql    = "SELECT id FROM ".$poll_table." WHERE categories LIKE %s;";
		$result = $wpdb->get_results(
			   	  	$wpdb->prepare( $sql, $like),
			   	  	'ARRAY_A'
				  );

		if (empty($result)) {
			return "";
		}
		$cat = $this->ays_get_poll_category($id);
		if (empty($cat)) {
			return "";
		}
		$polls_pool = $this->ays_get_polls_pool($result);
		$cat_opt    = json_decode($cat['options'], true);
		$btn_text   = isset($cat_opt['next_text']) ? stripslashes($cat_opt['next_text']) : __('Next', $this->plugin_name);
		$show_next  = isset($cat_opt['allow_skip']) && $cat_opt['allow_skip'] == 'allow' ? 'true' : 'false';
		$cat_id     = uniqid('ays-poll-category-pool-');
		$j          = uniqid('JsVariable');
		$content    = "
            <div style='margin:1rem auto;' class='ays_poll_category-container' data-var='".$j."' id='$cat_id'>
            </div>
            <script>
                var catContainer".$j." = '".$cat_id."';
                var pollsGlobalPool".$j." = ".$polls_pool.";
                var showNext".$j." = ".$show_next.";
                var catIndex".$j." = 0;
                var aysPollBtnText".$j." = '" . $btn_text . "';
            </script>";
		if ($echo) {
			echo $content;
		} else {
			return $content;
		}
	}

	private function get_poll_by_id( $id ) {
		global $wpdb;
		$args_id = absint(intval($id));
		$poll_table = esc_sql($wpdb->prefix."ayspoll_polls");
		$sql  = "SELECT * FROM ".$poll_table." WHERE id=%d";

		$poll = $wpdb->get_row(
		   	  		$wpdb->prepare( $sql, $args_id),
			   	  	'ARRAY_A'
			  	);

		if (empty($poll)) {
			return $poll;
		}
		$answ_table = esc_sql($wpdb->prefix."ayspoll_answers");
		$sql = "SELECT * FROM ".$answ_table." WHERE poll_id=%d ORDER BY id ASC";

		$poll['answers'] = $wpdb->get_results(
					   	  		$wpdb->prepare( $sql, $args_id),
					   	  		'ARRAY_A'
						   );
		$json            = $poll['styles'];
		$poll['styles']  = json_decode($json, true);

		return $poll;
	}

	public function get_poll_results_count_by_id( $id ) {
		global $wpdb;

		$args_id = absint(intval($id));
		$rep_table = esc_sql($wpdb->prefix."ayspoll_reports");
		$answ_table = esc_sql($wpdb->prefix."ayspoll_answers");

		$sql = "SELECT COUNT(*) AS res_count
                FROM ".$rep_table."
                INNER JOIN ".$answ_table." 
                ON ".$answ_table.".id=".$rep_table.".answer_id
                WHERE ".$answ_table.".poll_id = %d";

        $poll = $wpdb->get_row(
		   	  		$wpdb->prepare( $sql, $args_id),
			   	  	'ARRAY_A'
			  	);

		return $poll;
	}

	private function get_user_ip() {
		$ipaddress = '';
		if (getenv('HTTP_CLIENT_IP')) {
			$ipaddress = getenv('HTTP_CLIENT_IP');
		} else if (getenv('HTTP_X_FORWARDED_FOR')) {
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		} else if (getenv('HTTP_X_FORWARDED')) {
			$ipaddress = getenv('HTTP_X_FORWARDED');
		} else if (getenv('HTTP_FORWARDED_FOR')) {
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
		} else if (getenv('HTTP_FORWARDED')) {
			$ipaddress = getenv('HTTP_FORWARDED');
		} else if (getenv('REMOTE_ADDR')) {
			$ipaddress = getenv('REMOTE_ADDR');
		} else {
			$ipaddress = 'UNKNOWN';
		}

		return $ipaddress;
	}

	public function secondsToWords( $seconds ) {
		$ret = "";
		/*** get the days ***/
		$days = intval(intval($seconds) / (3600 * 24));
		if ($days > 0) {
			$ret .= "$days days ";
		}
		/*** get the hours ***/
		$hours = (intval($seconds) / 3600) % 24;
		if ($hours > 0) {
			$ret .= "$hours hours ";
		}
		/*** get the minutes ***/
		$minutes = (intval($seconds) / 60) % 60;
		if ($minutes > 0) {
			$ret .= "$minutes minutes ";
		}
		/*** get the seconds ***/
		$seconds = intval($seconds) % 60;
		if ($seconds > 0) {
			$ret .= "$seconds seconds";
		}

		return $ret;
	}

	public function ays_get_polls_pool( $array_of_poll_id ) {
		$pool = array();
		foreach ( $array_of_poll_id as $poll ) {
			$pool[] = $this->ays_poll_generate_html($poll['id'], false);
		}

		return json_encode($pool);
	}

	public function ays_get_poll_category( $id ) {
		global $wpdb;
		$cat_id = absint(intval($id));
		$cat_table = esc_sql($wpdb->prefix."ayspoll_categories");
		$sql = "SELECT * FROM ".$cat_table." WHERE id=%d";
		$result = $wpdb->get_row(
			   	  	$wpdb->prepare( $sql, $cat_id),
			   	  	'ARRAY_A'
				  );

		return $result;
	}

	public function ays_finish_poll() {
		if (wp_verify_nonce($_POST["ays_finish_poll"], 'ays_finish_poll')) {
			$poll_id   = absint($_POST['poll_id']);
			$answer_id = (isset($_POST['answer']) && $_POST['answer'] !== null) ? absint($_POST['answer']) : 0;

            $poll    = $this->get_poll_by_id($poll_id);
            $options = $poll['styles'];
			if ($answer_id > 0) {
				if (!empty($options['info_form'])) {
	                $user_email = isset($_POST["apm_email"]) && $_POST['apm_email'] !== null ? sanitize_email($_POST["apm_email"]) : '';
				}else{
					$user_email = '';
				}
                // MailChimp

                if (isset($options['enable_mailchimp']) && $options['enable_mailchimp'] == 'on') {
                    if (isset($options['mailchimp_list']) && $options['mailchimp_list'] != "") {
                        $poll_settings = $this->settings;
                        $mailchimp_res = ($poll_settings->ays_get_setting('mailchimp') === false) ? json_encode(array()) : $poll_settings->ays_get_setting('mailchimp');
                        $mailchimp = json_decode($mailchimp_res, true);
                        $mailchimp_username = isset($mailchimp['username']) ? $mailchimp['username'] : '';
                        $mailchimp_api_key = isset($mailchimp['apiKey']) ? $mailchimp['apiKey'] : '';
                        $mailchimp_list = (isset($options['mailchimp_list'])) ? $options['mailchimp_list'] : '';
                        $mailchimp_email = $user_email;
                        $user_name = isset($_POST['apm_name']) ? explode(" ", wp_filter_post_kses($_POST['apm_name'])) : array();
                        $mailchimp_fname = (isset($user_name[0]) && $user_name[0] != "") ? $user_name[0] : "";
                        $mailchimp_lname = (isset($user_name[1]) && $user_name[1] != "") ? $user_name[1] : "";
                        $user_phone = isset($_POST['apm_phone']) ? explode(" ", wp_filter_post_kses($_POST['apm_phone'])) : array();
                        $mailchimp_phone = (isset($user_phone[0]) && $user_phone[0] != "") ? $user_phone[0] : "";
                        if ($mailchimp_username != "" && $mailchimp_api_key != "") {
                            $args = array(
                                "email" => $mailchimp_email,
                                "fname" => $mailchimp_fname,
                                "lname" => $mailchimp_lname,
                                "pnumber" => $mailchimp_phone,
                            );
                            $mresult = $this->ays_add_mailchimp_transaction($mailchimp_username, $mailchimp_api_key, $mailchimp_list, $args);
                        }
                    }
                }

				$answer = $this->get_answer_by_id($answer_id);

				$votes  = isset($answer['voted']) && $answer['voted'] !== null ? $answer['voted'] : 0;
				$votes++;

				if (!empty($options['notify_email_on'])) {
					$notify_admin_email = $options['notify_email'];
					$mail_text          = "Someone answer \"" . stripslashes($answer['answer']) . "\" in your \"" . stripslashes($poll['title']) . "\" poll on " . home_url() . ".";
					wp_mail($notify_admin_email, "Poll Maker Ays Notification", $mail_text);
				}
				$other_info = array(
					"name"  => "",
					"email" => "",
					"phone" => "",
				);
				if (!empty($options['info_form'])) {
					$other_info = array(
						"name"  => !empty($_POST['apm_name']) ? sanitize_text_field($_POST['apm_name']) : "",
						"email" => !empty($_POST['apm_email']) ? sanitize_email($_POST['apm_email']) : "",
						"phone" => !empty($_POST['apm_phone']) ? sanitize_text_field($_POST['apm_phone']) : "",
					);
				}
				global $wpdb;
				$answ_table = esc_sql($wpdb->prefix."ayspoll_answers");
				$report_table = esc_sql($wpdb->prefix."ayspoll_reports");

				// AV  IP Storing
				$settings_table = esc_sql($wpdb->prefix."ayspoll_settings");
				$key_meta = esc_sql('options');
				$sql_ip = "SELECT meta_value FROM ".$settings_table." WHERE meta_key = %s";
        		$res_ip = $wpdb->get_var(
                    $wpdb->prepare( $sql_ip, $key_meta)
                );

        		$options_res = ($res_ip === false) ? json_encode(array()) : $res_ip;
				$option_res = json_decode($options_res, true);

				$user_ips = isset($option_res['disable_ip_storing']) && $option_res['disable_ip_storing'] == 'on' ? '' : $this->get_user_ip();

				$user_ip = esc_sql($user_ips);
				$wpdb->update(
					$answ_table,
					array('votes' => $votes),
					array('id' => $answer_id),
					array('%d'),
					array('%d')
				);
				$wpdb->insert(
					$report_table,
					array(
						'answer_id'  => $answer_id,
						'user_ip'    => $user_ip,
						'user_id'    => is_user_logged_in() ? wp_get_current_user()->ID : 0,
						'vote_date'  => date('Y-m-d G:i:s'),
						'other_info' => json_encode($other_info),
					),
					array('%d', '%s', '%s', '%s', '%s')
				);
			}
			ob_end_clean();
			$ob_get_clean = ob_get_clean();
			wp_send_json($this->get_poll_by_id($poll_id));
		}
	}

    public function ays_add_mailchimp_transaction( $username, $api_key, $list_id, $args ) {

        $email = isset($args['email']) ? $args['email'] : null;
        $fname = isset($args['fname']) ? $args['fname'] : "";
        $lname = isset($args['lname']) ? $args['lname'] : "";
        $phone = isset($args['pnumber']) ? $args['pnumber'] : "";

        $api_prefix = explode("-", $api_key)[1];

        $fields = array(
            "email_address" => $email,
            "status"        => "subscribed",
            "merge_fields"  => array(
                "FNAME" => $fname,
                "LNAME" => $lname,
                "PHONE" => $phone
            )
        );
        $curl   = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL            => "https://" . $api_prefix . ".api.mailchimp.com/3.0/lists/" . $list_id . "/members/",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_USERPWD        => "$username:$api_key",
            CURLOPT_CUSTOMREQUEST  => "POST",
            CURLOPT_POSTFIELDS     => json_encode($fields),
            CURLOPT_HTTPHEADER     => array(
                "Content-Type: application/json",
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);

        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #: " . $err;
        } else {
            return $response;
        }
    }

    private function get_answer_by_id( $id ) {
		global $wpdb;
        $args_id = absint(intval($id));
        $answ_table = esc_sql($wpdb->prefix."ayspoll_answers");
        $rep_table = esc_sql($wpdb->prefix."ayspoll_reports");
		$sql = "SELECT a.*, COUNT(r.id) as voted FROM ".$answ_table." as a
                JOIN ".$rep_table." as r
                ON r.answer_id = a.id
                WHERE a.id = %d GROUP BY a.id";

		$answ_id = $wpdb->get_row(
			   	  		$wpdb->prepare( $sql, $args_id),
				   	  	'ARRAY_A'
				  	);

		return $answ_id;
	}

	protected function hex2rgba($color, $opacity = false){

        $default = 'rgb(0,0,0)';

        //Return default if no color provided
        if (empty($color))
            return $default;

        //Sanitize $color if "#" is provided
        if ($color[0] == '#') {
            $color = substr($color, 1);
        }else{
            return $color;
        }

        //Check if color has 6 or 3 characters and get values
        if (strlen($color) == 6) {
            $hex = array($color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5]);
        } elseif (strlen($color) == 3) {
            $hex = array($color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2]);
        } else {
            return $default;
        }

        //Convert hexadec to rgb
        $rgb = array_map('hexdec', $hex);

        //Check if opacity is set(rgba or rgb)
        if ($opacity) {
            if (abs($opacity) > 1)
                $opacity = 1.0;
            $output = 'rgba(' . implode(",", $rgb) . ',' . $opacity . ')';
        } else {
            $output = 'rgb(' . implode(",", $rgb) . ')';
        }

        //Return rgb(a) color string
        return $output;
    }
}
