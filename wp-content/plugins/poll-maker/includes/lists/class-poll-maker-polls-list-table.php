<?php
ob_start();

class Polls_List_Table extends WP_List_Table {
	private $plugin_name;

	/** Class constructor */
	public function __construct( $plugin_name ) {
		$this->plugin_name = $plugin_name;
		parent::__construct(array(
			'singular' => __('Poll', $this->plugin_name), //singular name of the listed records
			'plural'   => __('Polls', $this->plugin_name), //plural name of the listed records
			'ajax'     => false, //does this table support ajax?
		));
		add_action('admin_notices', array($this, 'poll_notices'));
	}

	public function get_categories() {
		global $wpdb;
		$cat_table = esc_sql($wpdb->prefix."ayspoll_categories");
		$sql = "SELECT * FROM ".$cat_table;

		return $wpdb->get_results($sql, 'ARRAY_A');
	}

	public function add_or_edit_polls( $data, $id = null, $ays_change_type = "" ) {
		global $allowedtags;
		$old_allowedtags = $allowedtags;
		$default_attribs = array(
			'id'    => array(),
			'class' => array(),
			'title' => array(),
			'style' => array(),
		);
		$allowedtags     = array(
			'div'        => $default_attribs,
			'span'       => $default_attribs,
			'p'          => $default_attribs,
			'a'          => array_merge($default_attribs, array(
				'href'   => array(),
				'target' => array('_blank', '_top'),
			)),
			'input'      => array_merge($default_attribs, array(
				'type'  => array(),
				'name'  => array(),
				'value' => array(),
			)),
			'textarea'   => array_merge($default_attribs, array(
				'type' => array(),
				'name' => array(),
			)),
			'u'          => $default_attribs,
			'i'          => $default_attribs,
			'q'          => $default_attribs,
			'b'          => $default_attribs,
			'ul'         => $default_attribs,
			'dl'         => $default_attribs,
			'ol'         => $default_attribs,
			'li'         => $default_attribs,
			'br'         => $default_attribs,
			'hr'         => $default_attribs,
			'strong'     => $default_attribs,
			'blockquote' => $default_attribs,
			'del'        => $default_attribs,
			'strike'     => $default_attribs,
			'em'         => $default_attribs,
			'code'       => $default_attribs,
		);

		global $wpdb;
		$poll_table   = esc_sql($wpdb->prefix . 'ayspoll_polls');
		$answer_table = esc_sql($wpdb->prefix . 'ayspoll_answers');
		if (isset($data["poll_action"]) && wp_verify_nonce($data["poll_action"], 'poll_action')) {
			$title                   = sanitize_text_field($data['ays-poll-title']);
			$show_title              = isset($data['show_title']) && $data['show_title'] == 'show' ? 1 : 0;
			$limit_users             = isset($data['apm_limit_users']) && $data['apm_limit_users'] == 'on' ? 1 : 0;
			$limit_users_method      = isset($data['ays_limit_method']) ? sanitize_text_field($data['ays_limit_method']) : 'ip';
			$limit_users_msg         = isset($data['ays_limitation_message']) ? wpautop($data['ays_limitation_message']) : "";
			$limit_users_url         = isset($data['ays_redirect_url']) ? wp_http_validate_url($data['ays_redirect_url']) : "";
			$limit_users_delay       = isset($data['ays_redirection_delay']) ? absint($data['ays_redirection_delay']) : 0;
			$limit_users_role_enable = isset($data['ays_enable_restriction_pass']) && $data['ays_enable_restriction_pass'] == 'on' ? 1 : 0;
			$limit_users_role        = (isset($data["ays_users_roles"]) && !empty($data["ays_users_roles"])) ? $data["ays_users_roles"] : array();
			$limit_users_role_msg      = isset($data['restriction_pass_message']) ? wpautop($data['restriction_pass_message']) : "";
			$limit_users_logged_enable = (isset($data['ays_enable_logged_users']) && $data['ays_enable_logged_users'] == 'on') || $limit_users_role_enable ? 1 : 0;
			$limit_users_logged_msg    = isset($data['ays_enable_logged_users_message']) ? wpautop($data['ays_enable_logged_users_message']) : "";
			$hide_results              = isset($data['ays-poll-hide-results']) && 'hide' == $data['ays-poll-hide-results'] ? 1 : 0;
			$hide_result_message       = isset($data['ays_poll_result_message']) && 'hide' == $data['ays_poll_result_message'] ? 1 : 0;
			$hide_results_text         = stripcslashes($data['ays-poll-hide-results-text']);
			$ays_result_message        = stripcslashes($data['ays_result_message']);
			$allow_not_vote            = isset($data['ays-poll-allow-not-vote']) && 'allow' == $data['ays-poll-allow-not-vote'] ? 1 : 0;
			$show_social               = isset($data['ays-poll-show-social']) && 'show' == $data['ays-poll-show-social'] ? 1 : 0;
			$categories                = isset($data['ays-poll-categories']) ? ',' . implode(',', $data['ays-poll-categories']) . ',' : ',1,';
			$description               = sanitize_textarea_field($data['ays-poll-description']);
			$type                      = sanitize_text_field($data['ays-poll-type']);
			$question                  = stripcslashes($data['ays_poll_question']);
			$image                     = wp_http_validate_url($data['ays_poll_image']);
			$theme_id                  = absint($data['ays_poll_theme']);
			$main_color              = sanitize_text_field($data['ays_poll_main_color']);
			$text_color              = sanitize_text_field($data['ays_poll_text_color']);
			$icon_color              = sanitize_text_field($data['ays_poll_icon_color']);
			$bg_color                = sanitize_text_field($data['ays_poll_bg_color']);
			$answer_bg_color         = sanitize_text_field($data['ays_poll_answer_bg_color']);
			$answer_border_side      = isset($data['ays_poll_border_side']) ? sanitize_text_field($data['ays_poll_border_side']) : 'all_sides';
			$bg_image                = wp_http_validate_url($data['ays_poll_bg_image']);
			$randomize_answers       = !isset($data['randomize_answers']) ? "off" : $data['randomize_answers'];
			$enable_asnwers_sound    = !isset($data['ays_poll_enable_asnwers_sound']) ? "off" : $data['ays_poll_enable_asnwers_sound'];
			$icon_size               = absint($data['ays_poll_icon_size']) >= 10 ? absint($data['ays_poll_icon_size']) : 24;
			$width                   = absint($data['ays_poll_width']);
			$btn_text                = sanitize_text_field($data['ays_poll_btn_text']);
			$see_res_btn_text        = sanitize_text_field($data['ays_poll_res_btn_text']);
			$border_style            = sanitize_text_field($data['ays_poll_border_style']);
			$border_radius           = sanitize_text_field($data['ays_poll_border_radius']);
			$border_width            = sanitize_text_field($data['ays_poll_border_width']);
			$box_shadow_color        = sanitize_text_field($data['ays_poll_box_shadow_color']);
			
			//Gradient
			$enable_background_gradient    = sanitize_text_field($data['ays_enable_background_gradient']);
			$background_gradient_color_1   = sanitize_text_field($data['ays_background_gradient_color_1']);
			$background_gradient_color_2   = sanitize_text_field($data['ays_background_gradient_color_2']);
			$poll_gradient_direction       = sanitize_text_field($data['ays_poll_gradient_direction']);
			///

			// Redirect after submit
            $redirect_after_submit = ( isset( $data['ays_redirect_after_submit'] ) && $data['ays_redirect_after_submit'] == 'on' ) ? 1 : 0;

            $submit_redirect_url = !isset($data['ays_submit_redirect_url']) ? '' : $data['ays_submit_redirect_url'];
            // $submit_redirect_delay = !isset($data['ays_submit_redirect_delay']) ? '' : $data['ays_submit_redirect_delay'];
            ///

			$enable_box_shadow         = sanitize_text_field($data['ays_poll_enable_box_shadow']);
			$enable_answer_style       = sanitize_text_field($data['ays_poll_enable_answer_style']);
			$load_effect               = sanitize_text_field($data['ays-poll-load-effect']);
			$load_gif                  = isset($data['ays-poll-load-gif']) ? sanitize_text_field($data['ays-poll-load-gif']) : 'plg_default';
			$notify_on                 = isset($data['ays_notify_by_email_on']) && $data['ays_notify_by_email_on'] == 'on' ? 1 : 0;
			$notify_email              = isset($data['ays_notify_email']) && !empty($data['ays_notify_email']) ? sanitize_email($data['ays_notify_email']) : get_option('admin_email');
			$redirect_users            = isset($data['ays_redirect_after_vote']) && $data['ays_redirect_after_vote'] == 'on' ? 1 : 0;
			$redirect_after_vote_url   = isset($data['redirection_url']) ? wp_http_validate_url($data['redirection_url']) : "";
			$redirect_after_vote_delay = isset($data['redirection_delay']) ? absint($data['redirection_delay']) : 0;
			$result_sort_type          = sanitize_text_field($data['ays-poll-result-sort-type']);
			$poll_direction            = sanitize_text_field($data['ays_poll_direction']);
			$published                 = absint(intval($data['ays_publish']));
			$enable_pass_count         = !isset($data['ays_enable_pass_count']) ? null : $data['ays_enable_pass_count'];

			$activeInterval_full       = isset($data['ays-active']) ? $data['ays-active'] : "";
			$activeInterval_fullArr    = explode(" ",$activeInterval_full);
			$activeInterval            = isset($activeInterval_fullArr[0]) ? $activeInterval_fullArr[0] : "";
			$activeIntervalSec         = isset($activeInterval_fullArr[1]) ? $activeInterval_fullArr[1] : "";

			$deactiveInterval_full       = isset($data['ays-deactive']) ? $data['ays-deactive'] : "";
			$deactiveInterval_fullArr    = explode(" ",$deactiveInterval_full);
			$deactiveInterval            = isset($deactiveInterval_fullArr[0]) ? $deactiveInterval_fullArr[0] : "";
			$deactiveIntervalSec         = isset($deactiveInterval_fullArr[1]) ? $deactiveInterval_fullArr[1] : "";

			$active_date_message       = wpautop($data['active_date_message']);
			$active_date_message_soon  = wpautop($data['active_date_message_soon']);
			$css                       = stripcslashes($data['ays_custom_css']);
			$active_date_check         = isset($data['active_date_check']) && !empty($data['active_date_check']) ? $data['active_date_check'] : '';
			$enable_restart_button     = isset($data['ays_enable_restart_button']) && 'on' == $data['ays_enable_restart_button'] ? 1 : 0;
			$enable_vote_btn           = isset($data['ays_enable_vote_button']) && 1 == $data['ays_enable_vote_button'] ? 1 : 0;
			$show_votes_count          = isset($data['show_votes_count']) && 1 == $data['show_votes_count'] ? 1 : 0;
			$show_create_date          = isset($data['show_poll_creation_date']) && 1 == $data['show_poll_creation_date'] ? 1 : 0;
			$show_author          	   = isset($data['show_poll_author']) && 1 == $data['show_poll_author'] ? 1 : 0;
			$show_res_percent          = isset($data['show_res_percent']) && 1 == $data['show_res_percent'] ? 1 : 0;
			$show_result_btn_schedule  = isset($data['show_result_btn_schedule']) && 1 == $data['show_result_btn_schedule'] ? 1 : 0;
			$ays_poll_show_timer  	 = isset($data['ays_poll_show_timer']) && 1 == $data['ays_poll_show_timer'] ? 1 : 0;
			$ays_show_timer_type  	 = isset($data['ays_show_timer_type']) && !empty($data['ays_show_timer_type']) ? $data['ays_show_timer_type'] : 'countdown';
			$info_form               = isset($data['ays_poll_info_form']) && 'on' == $data['ays_poll_info_form'] ? 1 : 0;
			$form_fields             = implode(',', $data['ays-poll-form-fields']);
			$form_required_fields    = implode(',', $data['ays-poll-form-required-fields']);
			$info_form_title         = isset($data['ays-poll-info-form-text']) ? wpautop($data['ays-poll-info-form-text']) : "";
			$title_bg_color          = sanitize_text_field($data['ays_poll_title_bg_color']);

			$poll_create_date        = !isset($data['ays_poll_ctrate_date']) ? '0000-00-00 00:00:00' : $data['ays_poll_ctrate_date'];

			$author = isset($data['ays_poll_author'])?stripslashes($data['ays_poll_author']) : '';
            $author = json_decode($author, true);
            // MailChimp
            $enable_mailchimp          = isset($data['ays_enable_mailchimp']) && $data['ays_enable_mailchimp'] == 'on' ? "on": "off";
            $mailchimp_list             = !isset($data['ays_mailchimp_list'])?"":$data['ays_mailchimp_list'];


            // Show login form for not logged in users
            $show_login_form = (isset($data['ays_show_login_form']) && $data['ays_show_login_form'] == "on" && $limit_users_logged_enable == 1) ? 'on' : 'off';
            // Disable answer hover 
            $disable_answer_hover = (isset($data['ays_disable_answer_hover']) && $data['ays_disable_answer_hover'] == 'on') ? 1 : 0;

            $custom_class = (isset($data['ays_poll_custom_class']) && $data['ays_poll_custom_class'] != '') ? $data['ays_poll_custom_class'] : '';
            
			$styles = json_encode(array(
				'main_color'                  => $main_color,
				'text_color'                  => $text_color,
				'icon_color'                  => $icon_color,
				'bg_color'                    => $bg_color,
				'answer_bg_color'             => $answer_bg_color,
				'answer_border_side'          => $answer_border_side,
				'icon_size'                   => $icon_size,
				'width'                       => $width,
				'btn_text'                    => $btn_text,
				'see_res_btn_text'            => $see_res_btn_text,
				'border_style'                => $border_style,
				'border_radius'               => $border_radius,
				'border_width'                => $border_width,
				'box_shadow_color'            => $box_shadow_color,
				'enable_box_shadow'           => $enable_box_shadow,
				'enable_answer_style'         => $enable_answer_style,
				'bg_image'                    => $bg_image,
				'hide_results'                => $hide_results,
				'hide_result_message'         => $hide_result_message,
				'hide_results_text'           => $hide_results_text,
				'result_message'          	  => $ays_result_message,
				'allow_not_vote'              => $allow_not_vote,
				'show_social'                 => $show_social,
				'load_effect'                 => $load_effect,
				'load_gif'                    => $load_gif,
				'limit_users'                 => $limit_users,
				'limit_users_method'          => $limit_users_method,
				'limitation_message'          => $limit_users_msg,
				'redirect_url'                => $limit_users_url,
				'redirection_delay'           => $limit_users_delay,
				'user_role'                   => $limit_users_role,
				'enable_restriction_pass'     => $limit_users_role_enable,
				'restriction_pass_message'    => $limit_users_role_msg,
				'enable_logged_users'         => $limit_users_logged_enable,
				'enable_logged_users_message' => $limit_users_logged_msg,
				'notify_email_on'             => $notify_on,
				'notify_email'                => $notify_email,
				'result_sort_type'            => $result_sort_type,
				'create_date'                 => $poll_create_date,
				'author'                      => $author,
				'redirect_users'              => $redirect_users,
				'redirect_after_vote_url'     => $redirect_after_vote_url,
				'redirect_after_vote_delay'   => $redirect_after_vote_delay,
				'published'                   => $published,
				'enable_pass_count'           => $enable_pass_count,
				'activeInterval'              => $activeInterval,
				'deactiveInterval'         	  => $deactiveInterval,
				'activeIntervalSec'           => $activeIntervalSec,
				'deactiveIntervalSec'         => $deactiveIntervalSec,
				'active_date_message'         => $active_date_message,
				'active_date_message_soon'    => $active_date_message_soon,
				'active_date_check'           => $active_date_check,
				'enable_restart_button'       => $enable_restart_button,
				'enable_vote_btn'             => $enable_vote_btn,
				'show_votes_count'            => $show_votes_count,
				'show_create_date'            => $show_create_date,
				'show_author'            	  => $show_author,
				'show_res_percent'            => $show_res_percent,
				'show_result_btn_schedule'    => $show_result_btn_schedule,
				'ays_poll_show_timer'    	  => $ays_poll_show_timer,
				'ays_show_timer_type'    	  => $ays_show_timer_type,
				'show_login_form'             => $show_login_form,
				'poll_direction'              => $poll_direction,
				'randomize_answers'           => $randomize_answers,
				'enable_asnwers_sound'        => $enable_asnwers_sound,
				'info_form'                   => $info_form,
				'fields'                      => sanitize_text_field($form_fields),
				'required_fields'             => sanitize_text_field($form_required_fields),
				'info_form_title'             => $info_form_title,
				'title_bg_color'              => $title_bg_color, //aray
                'enable_mailchimp'            => $enable_mailchimp,
                'enable_background_gradient'  => $enable_background_gradient,
                'background_gradient_color_1' => $background_gradient_color_1,
                'background_gradient_color_2' => $background_gradient_color_2,
                'poll_gradient_direction'     => $poll_gradient_direction,
                'redirect_after_submit'       => $redirect_after_submit,
                'mailchimp_list'              => $mailchimp_list,
                "users_role"                  => json_encode($limit_users_role),
                "disable_answer_hover"        => $disable_answer_hover,
                "custom_class"        		  => $custom_class
			));
			
			switch ( $type ) {
				case 'choosing':
					$view_type       = sanitize_text_field($data['ays-poll-choose-type']);
					$answers         = $data['ays-poll-answers'];
					$answers_ids     = $data['ays-poll-answers-ids'];
					$answers_url     = $data['ays_submit_redirect_url'];
					break;
				case 'voting':
					$view_type = $data['ays-poll-vote-type'];
					$answers   = array(1, -1);
					break;
				case 'rating':
					$view_type  = $data['ays-poll-rate-type'];
					$rate_value = $data['ays-poll-rate-value'];
					$answers    = range(1, $rate_value);
					break;
				default:
					# code...
					break;
			}
			if (0 == $id) {
				$poll_result = $wpdb->insert(
					$poll_table,
					array(
						'title'       => $title,
						'description' => $description,
						'type'        => $type,
						'question'    => $question,
						'view_type'   => $view_type,
						'categories'  => $categories,
						'image'       => $image,
						'show_title'  => $show_title,
						'styles'      => $styles,
						'custom_css'  => $css,
						'theme_id'    => $theme_id,
					),
					array(
						'%s',
						'%s',
						'%s',
						'%s',
						'%s',
						'%s',
						'%s',
						'%d',
						'%s',
						'%s',
						'%d',
					)
				);
				$last_id     = $wpdb->insert_id;
				foreach ( $answers as $k => $answer ) {
					if (empty($answer)) {
						continue;
					}

					$wpdb->insert(
						$answer_table,
						array(
							'poll_id'   => $last_id,
							'answer'    => wp_filter_kses($answer),
							'redirect'  => $answers_url[$k]
						),
						array(
							'%d',
							'%s',
							'%s'
						)
					);
				}
				$message = 'created';
			} else {
				$poll        = $this->get_poll_by_id($id);
				$poll_result = $wpdb->update(
					$poll_table,
					array(
						'title'       => $title,
						'description' => $description,
						'type'        => $type,
						'question'    => $question,
						'view_type'   => $view_type,
						'categories'  => $categories,
						'image'       => $image,
						'show_title'  => $show_title,
						'styles'      => $styles,
						'custom_css'  => $css,
						'theme_id'    => $theme_id,
					),
					array('id' => $id),
					array(
						'%s',
						'%s',
						'%s',
						'%s',
						'%s',
						'%s',
						'%s',
						'%d',
						'%s',
						'%s',
						'%d',
					),
					array('%d')
				);
				if ($type != $poll['type']) {					
				    $reports_table  = esc_sql($wpdb->prefix."ayspoll_reports");
				    $poll_id = absint(intval($id));
				 	$sql = "DELETE r FROM ".$reports_table." as r JOIN ".$answer_table." ON ".$answer_table.".id = r.answer_id JOIN ".$poll_table." ON ".$poll_table.".id = ".$answer_table.".poll_id WHERE ".$poll_table.".id = %d";

					$wpdb->query(
						$wpdb->prepare($sql,$poll_id)
					);

					$wpdb->delete(
						$answer_table,
						array('poll_id' => $id),
						array('%d')
					);
					
					foreach ( $answers as $k => $answer ) {
						if (empty($answer)) {
							continue;
						}
						
						$wpdb->insert(
							$answer_table,
							array(
								'poll_id'   => $id,
								'answer'    => wp_filter_kses($answer),
								'redirect'  => $answers_url[$k]
							),
							array(
								'%d',
								'%s',
								'%s'
							)
						);
					}
				}
				if ('choosing' == $type && $type == $poll['type']) {
					foreach ( $poll['answers'] as $answer ) {
						$old_id = $answer['id'];
						$index  = array_search($old_id, $answers_ids);
						if ($index !== false) {
							$new_answer = $answers[$index];
							$new_url = $answers_url[$index];
							if (empty($new_answer)) {
								continue;
							}
							if (empty($new_url)) {
								$new_url = null;
							}

							$wpdb->update(
								$answer_table,
								array(
									'answer'    => wp_filter_kses($new_answer),
									'redirect'  => $new_url
								),
								array('id' => $old_id),
								array('%s'),
								array('%d')
							);
						} else {
							$report_table  = esc_sql($wpdb->prefix."ayspoll_reports");
							$wpdb->delete(
								$report_table,
								array('answer_id' => $old_id),
								array('%d')
							);
							$wpdb->delete(
								$answer_table,
								array('id' => $old_id),
								array('%d')
							);
						}
					}
					foreach ( $answers_ids as $index => $value ) {
						if (0 == $value) {
							if (empty($answers[$index])) {
								continue;
							}

							$wpdb->insert(
								$answer_table,
								array(
									'poll_id'   => $id,
									'answer'    => wp_filter_kses($answers[$index]),
									'redirect'  => $answers_url[$index],
								),
								array('%d', '%s', '%s')
							);
						}
					}
				}
				if ($type == 'rating' && $poll['type'] == 'rating' && count($poll['answers']) != $rate_value) {
					if (count($poll['answers']) > $rate_value) {
						for ( $i = $rate_value; $i < count($poll['answers']); $i++ ) {
							$wpdb->delete(
								"{$wpdb->prefix}ayspoll_reports",
								array('answer_id' => $poll['answers'][$i]['id']),
								array('%d')
							);
							$wpdb->delete(
								$answer_table,
								array('id' => $poll['answers'][$i]['id']),
								array('%d')
							);
						}
					} else {
						for ( $i = count($poll['answers']); $i < $rate_value; $i++ ) {
							$wpdb->insert(
								$answer_table,
								array(
									'poll_id' => $id,
									'answer'  => wp_filter_kses($answers[$i]),
								),
								array('%d', '%s')
							);
						}
					}
				}
				$message = 'updated';
			}
			$active_tabs = !empty($data['ays_poll_active_tab']) ? sanitize_text_field($data['ays_poll_active_tab']) : "General";
			$active_tab = urlencode($active_tabs);

			if ($poll_result >= 0) {
				if ('' != $ays_change_type) {
					if($id == 0){
                       $url = esc_url_raw( add_query_arg( array(
                           "action"    	 => "edit",
                           "poll"      	 => $last_id,
                           "active-tab"  => $active_tab,
                           "status"    	 => $message
                       ) ) );
                   	}else{
                       $url = esc_url_raw( remove_query_arg(false) ) . '&active-tab='.$active_tab.'&status=' . $message;
                   	}					
					wp_redirect($url);
				} else {
					$url = esc_url_raw(remove_query_arg(['action'])) . "&active-tab=".$active_tab."&status=" . $message;
					wp_redirect($url);
				}
			}
		}
		$allowedtags = $old_allowedtags;
	}

	public function duplicate_poll( $id ) {
		global $wpdb;

	 	$poll_table  = esc_sql($wpdb->prefix."ayspoll_polls");
	 	$answers_table  = esc_sql($wpdb->prefix."ayspoll_answers");

		$duplicate = $this->get_poll_by_id($id, false);
		if (empty($duplicate)) {
			$url = esc_url_raw(remove_query_arg(array('action', 'poll'))) . '&status=failed';
			wp_redirect($url);
		}
		
		$user_id = get_current_user_id();
        $user = get_userdata($user_id);
        $author = array(
            'id' => $user->ID,
            'name' => $user->data->display_name
        );
		$options = json_decode($duplicate['styles'], true);
        
        $options['create_date'] = current_time( 'mysql' );
        $options['author'] = $author;

		$answers = $duplicate['answers'];
		unset($duplicate['answers']);
		unset($duplicate['id']);
		$duplicate['title']  = "Copy - " . $duplicate['title'];
		$duplicate['styles'] = json_encode($options);
		$result              = $wpdb->insert(
			$poll_table,
			$duplicate,
			array(
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%d',
				'%s',
				'%s',
				'%d',
			)
		);

		$poll_id         = $wpdb->insert_id;
		$answers_results = array();
		$flag            = true;

		foreach ( $answers as $answer ) {
			$answer['poll_id'] = $poll_id;
			unset($answer['id']);
			unset($answer['votes']);			
			$answers_results[] = $wpdb->insert(
				$answers_table,
				$answer,
				array(
					'%d',
					'%s',
					'%s',
				)
			);
		}

		foreach ( $answers_results as $answers_result ) {
			if ($answers_result >= 0) {
				$flag = true;
			} else {
				$flag = false;
				break;
			}
		}
		$message = 'duplicated';
		if ($result >= 0 && $flag == true) {
			$url = esc_url_raw(remove_query_arg(array('action', 'poll'))) . '&status=' . $message;
			wp_redirect($url);
		}

	}

	public static function get_poll_pass_count($id) {
        global $wpdb;
        $poll_id = absint(intval($id));
        $args_id = esc_sql($poll_id);
        $answ_table = esc_sql($wpdb->prefix."ayspoll_answers");
        $sql = "SELECT SUM(votes) FROM ".$answ_table." WHERE poll_id=%d";
        $result = $wpdb->get_var(
			   	  	$wpdb->prepare( $sql, $args_id)
				  );

        return $result;
    }

	public function get_poll_by_id( $id, $decode = true ) {
		global $wpdb;
		$poll_id = absint(intval($id));
		$poll_table = esc_sql($wpdb->prefix."ayspoll_polls");
		$answ_table = esc_sql($wpdb->prefix."ayspoll_answers");
		$sql  = "SELECT * FROM ".$poll_table." WHERE id=%d";
		$poll = $wpdb->get_row(
			   	  	$wpdb->prepare( $sql, $poll_id),
			   	  	'ARRAY_A'
				  );

		if (empty($poll)) {
			return array();
		}		

		$sql_answ = "SELECT * FROM ".$answ_table." WHERE poll_id=%d ORDER BY id ASC";
		$poll['answers'] = $wpdb->get_results(
			   	  	$wpdb->prepare( $sql_answ, $poll_id),
			   	  	'ARRAY_A'
				  );

		if ($decode) {
			$cats               = explode(',', $poll['categories']);
			$poll['categories'] = !empty($cats) ? $cats : array();
			$json               = $poll['styles'];
			$poll['styles']     = json_decode($json, true);
		}

		return $poll;
	}

	/** Text displayed when no customer data is available */
	public function no_items() {
		_e('There are no polls yet.', $this->plugin_name);
	}

	/**
	 * Render a column when no column specific method exist.
	 *
	 * @param array $item
	 * @param string $column_name
	 *
	 * @return mixed
	 */
	public function column_default( $item, $column_name ) {
		switch ( $column_name ) {
			case 'title':
			case 'categories':
			case 'type':
			case 'shortcode':
			case 'code_include':
			case 'create_date':
			case 'completed_count':
			case 'id':
			case 'publish':
				return $item[$column_name];
				break;
			default:
				return print_r($item, true); //Show the whole array for troubleshooting purposes
		}
	}

	/**
	 * Render the bulk edit checkbox
	 *
	 * @param array $item
	 *
	 * @return string
	 */
	function column_cb( $item ) {
		return sprintf(
			'<input type="checkbox" name="bulk-action[]" value="%s" />',
			$item['id']
		);
	}

	/**
	 * Method for name column
	 *
	 * @param array $item an array of DB data
	 *
	 * @return string
	 */
	function column_title( $item ) {
		$delete_nonce = wp_create_nonce($this->plugin_name . '-delete-poll');

		$title   = sprintf('<a href="?page=%s&action=%s&poll=%d">%s</a>', esc_attr($_REQUEST['page']), 'edit', absint($item['id']), stripslashes($item['title']));
		$actions = [
			'edit'      => sprintf('<a href="?page=%s&action=%s&poll=%d">' . __('Edit', $this->plugin_name) . '</a>', esc_attr($_REQUEST['page']), 'edit', absint($item['id'])),
			'duplicate' => sprintf('<a href="?page=%s&action=%s&poll=%d">' . __('Duplicate', $this->plugin_name) . '</a>', esc_attr($_REQUEST['page']), 'duplicate', absint($item['id'])),
			'delete'    => sprintf('<a href="?page=%s&action=%s&poll=%d&_wpnonce=%s">' . __('Delete', $this->plugin_name) . '</a>', esc_attr($_REQUEST['page']), 'delete', absint($item['id']), $delete_nonce),
		];

		return $title . $this->row_actions($actions);
	}

	function column_shortcode( $item ) {
		return sprintf('<input type="text" onClick="this.setSelectionRange(0, this.value.length)" readonly value="[ays_poll id=%s]" />', $item["id"]);
	}

	function column_code_include( $item ) {
		return sprintf('<input type="text" onClick="this.setSelectionRange(0, this.value.length)" readonly value="<?php echo do_shortcode(\'[ays_poll id=%s]\'); ?>" />', $item["id"]);
	}

	function column_categories( $item ) {
		if ($item['categories'] == '') {
			return '';
		}
		global $wpdb;
		$cats_ids     = explode(',', $item['categories']);
		$cats_content = "<ul id='cats-in-table'>";
		$cat_table = esc_sql($wpdb->prefix."ayspoll_categories");
		foreach ( $cats_ids as $id ) {
			if (empty($id)) {
				continue;
			}
			$args_id = esc_sql($id);
			$sql = "SELECT * FROM ".$cat_table." WHERE id=%d";

			$result = $wpdb->get_row(
					   	  		$wpdb->prepare( $sql, $args_id),
						   	  	'ARRAY_A'
						  	);
			if (empty($result)) {
				continue;
			}
			$cats_content .= "<li>".$result['title']."</li>";
		}
		$cats_content .= "</ul>";
		
		return $cats_content;
	}

	function column_create_date( $item ) {
        // var_dump($item); die();
        $options = json_decode($item['styles'], true);
        $date = isset($options['create_date']) && $options['create_date'] != '' ? $options['create_date'] : "0000-00-00 00:00:00";
        if(isset($options['author'])){
            if(is_array($options['author'])){
                $author = $options['author'];
            }else{
                $author = json_decode($options['author'], true);
            }
        }else{
            $author = array("name"=>"Unknown");
        }
        $text = "";
        if(Poll_Maker_Ays_Admin::validateDate($date)){
            $text .= "<p><b>Date:</b> ".$date."</p>";
        }
        if($author['name'] !== "Unknown"){
            $text .= "<p><b>Author:</b> ".$author['name']."</p>";
        }
        return $text;
    }

	function column_completed_count( $item ) {
        $id = $item['id'];
        $passed_count = $this->get_poll_pass_count($id);
        $text = "<p style='text-align:center;font-size:14px;'>".$passed_count."</p>";
        return $text;
    }

	function column_publish( $item ) {
		global $wpdb;
		$id = absint(intval($item['id']));
		$poll_table = esc_sql($wpdb->prefix."ayspoll_polls");
		$sql     = "SELECT * FROM ".$poll_table." WHERE id=%d";

		$res = $wpdb->get_row(
			   	  		$wpdb->prepare( $sql, $id),
				   	  	'ARRAY_A'
				  	  );
		$options = json_decode($res['styles'], true);
		$status  = isset($options['published']) ? $options['published'] : 1;

		return $status ? "<i class=\"ays_poll_far ays_poll_fa-check-square\"></i> " . __("Published", $this->plugin_name) : "<i class=\"ays_poll_far ays_poll_fa-square\"></i> " . __("Unpublished", $this->plugin_name);
	}

	/**
	 * Associative array of columns
	 *
	 * @return array
	 */
	function get_columns() {
		$columns = array(
			'cb'				=> '<input type="checkbox" />',
			'title'        		=> __('Title', $this->plugin_name),
			'type'         		=> __('Type', $this->plugin_name),
			'shortcode'   		=> __('Shortcode', $this->plugin_name),
			'code_include' 		=> __('Code Include', $this->plugin_name),
			'categories'   		=> __('Categories', $this->plugin_name),
			'create_date'   	=> __('Created', $this->plugin_name ),
			'completed_count'   => __('Completed count', $this->plugin_name ),
			'publish'      		=> __('Status', $this->plugin_name),
			'id'           		=> __('ID', $this->plugin_name),
		);

		return $columns;
	}

	/**
	 * Columns to make sortable.
	 *
	 * @return array
	 */
	public function get_sortable_columns() {
		$sortable_columns = array(
			'title' => array('title', true),
			'type'  => array('type', true),
			'id'    => array('id', true),
		);

		return $sortable_columns;
	}

	/**
	 * Returns an associative array containing the bulk action
	 *
	 * @return array
	 */
	public function get_bulk_actions() {
		$actions = array(
			'bulk-delete' => __('Delete', $this->plugin_name),
		);

		return $actions;
	}

	/**
	 * Handles data query and filter, sorting, and pagination.
	 */
	public function prepare_items() {

		$this->_column_headers = $this->get_column_info();

		/** Process bulk action */
		$this->process_bulk_action();

		$per_page     = $this->get_items_per_page('polls_per_page', 5);
		$current_page = $this->get_pagenum();
		$total_items  = self::record_count();

		$this->set_pagination_args(array(
			'total_items' => $total_items, //WE have to calculate the total number of items
			'per_page'    => $per_page, //WE have to determine how many items to show on a page
		));

		$this->items = self::get_polls($per_page, $current_page);
	}

	public function process_bulk_action() {
		//Detect when a bulk action is being triggered...
		$message = 'deleted';
		if ('delete' === $this->current_action()) {

			// In our file that handles the request, verify the nonce.
			$nonce = esc_attr($_REQUEST['_wpnonce']);

			if (!wp_verify_nonce($nonce, $this->plugin_name . '-delete-poll')) {
				die('Go get a life script kiddies');
			} else {
				self::delete_polls(absint($_GET['poll']));

				// esc_url_raw() is used to prevent converting ampersand in url to "#038;"
				// add_query_arg() return the current url

				$url = esc_url_raw(remove_query_arg(['action', 'poll', '_wpnonce'])) . '&status=' . $message;
				wp_redirect($url);
			}
		}

		// If the delete bulk action is triggered
		if ((isset($_POST['action']) && 'bulk-delete' == $_POST['action'])
		    || (isset($_POST['action2']) && 'bulk-delete' == $_POST['action2'])
		) {

			$delete_ids = esc_sql($_POST['bulk-action']);

			// loop over the array of record IDs and delete them
			foreach ( $delete_ids as $id ) {
				self::delete_polls($id);
			}

			// esc_url_raw() is used to prevent converting ampersand in url to "#038;"
			// add_query_arg() return the current url

			$url = esc_url_raw(remove_query_arg(['action', 'poll', '_wpnonce'])) . '&status=' . $message;
			wp_redirect($url);
		}
	}

	/**
	 * Delete a customer record.
	 *
	 * @param int $id customer ID
	 */
	public static function delete_polls( $id ) {
		global $wpdb;
		$poll_table = esc_sql($wpdb->prefix . "ayspoll_polls");
		$answ_table = esc_sql($wpdb->prefix . "ayspoll_answers");
		$reports_table = esc_sql($wpdb->prefix . "ayspoll_reports");
		$wpdb->delete(
			$poll_table,
			array('id' => $id),
			array('%d')
		);
		$wpdb->delete(
			$answ_table,
			array('poll_id' => $id),
			array('%d')
		);
		$sql = "DELETE FROM ".$reports_table."
        WHERE answer_id NOT IN (SELECT id FROM ".$answ_table.")";
		$wpdb->query($sql);
	}

	/**
	 * Returns the count of records in the database.
	 *
	 * @return null|string
	 */
	public static function record_count() {
		global $wpdb;
		$poll_table = esc_sql($wpdb->prefix . "ayspoll_polls");
		$sql = "SELECT COUNT(*) FROM ".$poll_table;

		return $wpdb->get_var($sql);
	}

	/**
	 * Retrieve customers data from the database
	 *
	 * @param int $per_page
	 * @param int $page_number
	 *
	 * @return mixed
	 */
	public static function get_polls( $per_page = 5, $page_number = 1 ) {

		global $wpdb;
		$poll_table = esc_sql($wpdb->prefix . "ayspoll_polls");
		$sql = "SELECT * FROM ".$poll_table;
		$args = array();

		$gp_orderby = isset($_REQUEST['orderby']) ? $_REQUEST['orderby'] : "id"; 
		switch ($gp_orderby) {
			case 'id':
				$gpolls_orderby = "id";
				break;
			case 'type':
				$gpolls_orderby = "type";
				break;			
			case 'title':
				$gpolls_orderby = "title";
				break;			
			default:
				$gpolls_orderby = "id";
				break;
		}

		$gr_order = isset($_REQUEST['order']) && !empty($_REQUEST['order']) ? $_REQUEST['order'] : "DESC";
		switch ($gr_order) {
			case 'asc':
				$req_order = "ASC";
				break;
			case 'desc':
				$req_order = "DESC";
				break;			
			default:
				$req_order = "DESC";
				break;
		}
		
		if (!empty($_REQUEST['orderby'])) {
			$sql .= ' ORDER BY ' . esc_sql($gpolls_orderby);
			$sql .= " ". $req_order;
		} else {
			$sql .= ' ORDER BY id DESC';
		}
		$sql .= " LIMIT %d";
		$args[] = $per_page;
		$offset = ($page_number - 1) * $per_page;
		$sql .= " OFFSET %d";
		$args[] = $offset;
		$result = $wpdb->get_results(
			   	  	$wpdb->prepare( $sql, $args),
			   	  	'ARRAY_A'
				  );

		return $result;
	}

	public function poll_notices() {
		$status = (isset($_REQUEST['status'])) ? sanitize_text_field($_REQUEST['status']) : '';

		if (empty($status)) {
			return;
		}

		if ('created' == $status) {
			$updated_message = esc_html(__('Poll created.', $this->plugin_name));
		} elseif ('updated' == $status) {
			$updated_message = esc_html(__('Poll saved.', $this->plugin_name));
		} elseif ('duplicated' == $status) {
			$updated_message = esc_html(__('Poll duplicated.', $this->plugin_name));
		} elseif ('deleted' == $status) {
			$updated_message = esc_html(__('Poll deleted.', $this->plugin_name));
		}

		if (empty($updated_message)) {
			return;
		}

		?>
        <div class="notice notice-success is-dismissible">
            <p>
				<?php echo $updated_message; ?>
            </p>
        </div>
		<?php
	}
}
