/*eslint-disable*/
;(function (){
    var charts = jQuery('.ninja-charts-container');
    if (charts.length) {
        charts.each(function () {
            var chartId = jQuery(this).data('id');
            var uniqid = jQuery(this).data('uniqid');
            var chartInstance = 'ninja_charts_instance_' + chartId;
            var canvasDom = 'ninja_charts_instance' + uniqid;
            var renderData = window[chartInstance];
            var options = JSON.parse(renderData.options);
            var canvas = document.getElementById(canvasDom);
            var ctx = canvas.getContext('2d');
            new Chart(ctx, {
                type: renderData.chart_type === 'area' ? 'line' : renderData.chart_type,
                data: renderData.chart_data,
                options: {
                    // responsive: options.chart.responsive,
                    maintainAspectRatio: false,
                    legend: {
                        display: options.legend.display === 'true' ? true : false,
                        position: options.legend.position,
                        labels: {
                            fontColor: options.legend.fontColor
                        }
                    },
                    tooltips: {
                        enabled: options.tooltip.enabled === 'true' ? true : false,
                        backgroundColor: options.tooltip.backgroundColor,
                        titleFontSize: Number(options.tooltip.titleFontSize),
                        titleFontColor: options.tooltip.titleFontColor,
                        bodyFontColor: options.tooltip.bodyFontColor,
                        bodyFontSize: Number(options.tooltip.bodyFontSize),
                        displayColors: true,
                        borderColor: options.tooltip.borderColor,
                        borderWidth: Number(options.tooltip.borderWidth)
                    },
                    title: {
                        display: options.title.display === 'true' ? true : false,
                        text: renderData.chart_name,
                        position: options.title.position,
                        fontColor: options.title.fontColor,
                        fontStyle: options.title.fontStyle,
                        fontSize: Number(options.title.fontSize)
                    },
                    scales: {
                        yAxes: [{
                            scaleLabel: {
                                display: options.axes.display === 'true' ? true : false,
                                labelString: options.axes.y_axis_label === null ? '' : options.axes.y_axis_label,
                                fontColor: options.chart.fontColor,
                                fontSize: Number(options.chart.fontSize),
                                fontStyle: options.chart.fontStyle
                            },
                            gridLines: {
                                display: options.axes.display === 'true' ? true : false
                            },
                            ticks: {
                                display: options.axes.display === 'true' ? true : false,
                                fontColor: options.chart.fontColor,
                                fontSize: Number(options.chart.fontSize),
                                fontStyle: options.chart.fontStyle
                            }
                        }],
                        xAxes: [{
                            scaleLabel: {
                                display: options.axes.display === 'true' ? true : false,
                                labelString: options.axes.x_axis_label === null ? '' : options.axes.x_axis_label,
                                fontColor: options.chart.fontColor,
                                fontSize: Number(options.chart.fontSize),
                                fontStyle: options.chart.fontStyle
                            },
                            gridLines: {
                                display: options.axes.display === 'true' ? true : false,
                            },
                            ticks: {
                                display: options.axes.display === 'true' ? true : false,
                                fontColor: options.chart.fontColor,
                                fontSize: Number(options.chart.fontSize),
                                fontStyle: options.chart.fontStyle
                            }
                        }]
                    }
                }
            });
        })
    }
})();
