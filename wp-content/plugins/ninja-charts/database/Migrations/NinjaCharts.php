<?php

namespace NinjaCharts\Database\Migrations;

class NinjaCharts
{
    static $tableName = 'ninja_charts';

    public static function migrate()
    {
        global $wpdb;

        $charsetCollate = $wpdb->get_charset_collate();

        $table = $wpdb->prefix . static::$tableName;

        $indexPrefix = $wpdb->prefix . '_index_';

        if ($wpdb->get_var("SHOW TABLES LIKE '$table'") != $table) {
            $sql = "CREATE TABLE $table (
                `id` BIGINT(20) UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
                `table_id` BIGINT(20) UNSIGNED NULL,
                `options` VARCHAR(1500) NOT NULL,
                `final_keys` VARCHAR(1000) NOT NULL,
                `chart_name` VARCHAR(50) NOT NULL,
                `render_engine` VARCHAR(20) NOT NULL,
                `chart_type` VARCHAR(10) NOT NULL,
                `data_source` VARCHAR(20) NOT NULL,
                `manual_inputs` TEXT NULL,
                `created_at` TIMESTAMP NULL,
                `updated_at` TIMESTAMP NULL,
                INDEX `{$indexPrefix}_table_id_idx` (`table_id` ASC)
            ) $charsetCollate;";

            dbDelta($sql);
        }
    }
}
