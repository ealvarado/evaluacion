<?php

namespace NinjaCharts\Hooks\Handlers;

use NinjaCharts\Database\DBSeeder;
use NinjaCharts\Database\DBMigrator;

class Activation
{
    public function handle($network_wide = false)
    {
        DBMigrator::run($network_wide);
        DBSeeder::run();
    }
}
