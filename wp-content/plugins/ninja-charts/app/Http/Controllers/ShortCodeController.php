<?php

namespace NinjaCharts\Http\Controllers;

use NinjaCharts\App;
use NinjaCharts\Traits\Helper;
use NinjaCharts\Models\NinjaCharts;
use NinjaCharts\Framework\Support\Arr;

class ShortCodeController extends Controller
{
    use Helper;

    protected $class_instance;

    public function createClassInstance($source)
    {
        $this->class_instance  =  $this->classInstanceHelper($source);

        return $this->class_instance;
    }

    public function makeShortCode($atts = [], $content = null, $tag = '')
    {
        // normalize attribute keys, lowercase
        $atts = array_change_key_case((array)$atts, CASE_LOWER);
        // override default attributes with user attributes
        $wporg_atts = shortcode_atts([
            'id' => null,
        ], $atts, $tag);

        $id = Arr::get($wporg_atts, 'id');
        $ninja_charts = NinjaCharts::find($id);
        if ($ninja_charts){
            $this->class_instance = $this->createClassInstance($ninja_charts->data_source);
            $chart_data = $this->class_instance->renderChart($ninja_charts);
            return $this->renderView($ninja_charts, $chart_data);
        }else{
            return "Invalid ShortCode...!";
        }

    }

    public function ninjaChartsShortCode()
    {
        add_shortcode('ninja_charts', [$this, 'makeShortCode']);
    }

    public function renderView($ninja_charts, $chart_data)
    {
        $app = App::getInstance();
        if ($ninja_charts->render_engine === 'chart_js') {
            $ninja_charts['chart_data'] = $chart_data;
            self::addInlineVars($ninja_charts, 'ninja_charts_instance');
            self::loadAssets();

            $options = json_decode(Arr::get($ninja_charts,'options'), true);
            $uniqid =  '_' . rand(). '_'. Arr::get($ninja_charts,'id');
            $chart_keys = [
                "uniqid" => $uniqid,
                "id"     => Arr::get($ninja_charts,'id')
            ];

            return $app->view->make('public.chart_js', compact('options','chart_keys', 'chart_data'));
        }
    }

    private static function addInlineVars($data, $chart_instance_name)
    {
        add_action('wp_footer', function () use ($data, $chart_instance_name) {
            $name = $chart_instance_name .'_'. Arr::get($data, 'id');
            ?>
            <script type="text/javascript">
                window['<?php echo $name;?>'] = <?php echo $data; ?>
            </script>
            <?php
        });
    }

    private static function loadAssets() {
        wp_enqueue_script('chartjs',
            NINJA_CHARTS_URL . 'assets/public/js/library/chart.min.js',
            array('jquery'),
            '2.9.3',
            true
        );

        wp_enqueue_script('chart_render_js',
            NINJA_CHARTS_URL . 'assets/public/js/render.js',
            array('chartjs'),
            '1.0.0',
            true
        );
    }
}

