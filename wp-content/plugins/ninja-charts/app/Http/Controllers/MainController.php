<?php

namespace NinjaCharts\Http\Controllers;

use NinjaCharts\Traits\Helper;
use NinjaCharts\Models\NinjaCharts;

class MainController extends Controller
{
    use Helper;

    private $class_instance;

    public function createClassInstance($data_source = null)
    {
        $this->class_instance = $this->classInstanceHelper($data_source);

        return $this->class_instance;
    }

    public function getDataSource()
    {
        $this->class_instance = $this->createClassInstance();
        $table_list = $this->class_instance->getTableList();

        $this->sendSuccess([
            'table_list' => $table_list
        ]);
    }

    public function getKeysByTableId()
    {
        $table_id = $this->request->get('table_id', null);
        $this->class_instance = $this->createClassInstance();
        $type_keys = $this->class_instance->getKeysByTable($table_id);

        // If any WP error occurred
        if (is_wp_error($type_keys)) {
            $this->sendError([
                $type_keys->get_error_message()
            ], 422);
        }

        $this->sendSuccess([
            'type_keys' => $type_keys
        ]);
    }

    public function getAllDataByTableId()
    {
        $table_id = $this->request->table_id;
        $keys = $this->request->keys;
        $chart_type = $this->request->chart_type;
        $extra_data = $this->request->get('extra_data', []);

        $id = $this->request->get('id', '');
        $this->class_instance = $this->createClassInstance();

        if (isset($extra_data['manual_inputs']) || $keys) {
            $chart_data = $this->class_instance->getAllDataByTable($table_id, $keys, $chart_type, $extra_data, $id);
        } else {
            $chart_data = '';
        }

        $this->sendSuccess([
            'chart_data' => $chart_data
        ]);
    }

    public function saveChartData()
    {
        $data = $this->request->data;
        $extra_options = $this->request->extra_options;

        $ninja_charts =  NinjaCharts::saveOrUpdate($data, $extra_options);
        $this->sendSuccess([
            'ninja_charts' => $ninja_charts
        ]);
    }

    public function getChartData()
    {
        $perPage = intval($this->request->get('per_page', 10));
        $keyword = $this->request->keyword;
        $ninja_charts = NinjaCharts::getChartData($keyword, $perPage);

        $this->sendSuccess([
            'ninja_charts' => $ninja_charts
        ]);
    }

    public function deleteChart($id = null)
    {
        $id = $this->request->id;
        $ninja_charts = NinjaCharts::singleOrMultipleDelete($single = true, $id);
        $this->sendSuccess([
            'deleted' => 204
        ]);
    }

    public function deleteMultipleChart()
    {
        $ids = $this->request->ids;
        $ninja_charts = NinjaCharts::singleOrMultipleDelete($single = false, $ids);
        $this->sendSuccess([
            'deleted' => 204
        ]);
    }

    public function cloneRow()
    {
        $id = $this->request->id;
        NinjaCharts::cloneRow($id);
        $this->sendSuccess([
            'success' => 200
        ]);
    }

    public function editChartById()
    {
        $id = $this->request->get('id', null);

        $ninja_charts = NinjaCharts::findOrFail($id);

        $this->sendSuccess([
            'ninja_charts' => $ninja_charts
        ]);
    }

}
