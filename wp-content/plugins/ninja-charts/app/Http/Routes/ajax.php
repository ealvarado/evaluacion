<?php

/**
 * @var $app NinjaCharts\Framework\Foundation\Application
 */

//$app->get('<route_name>', '<controler>@<method>');

//Ninja Table Routes
$app->get('get-data-source', 'MainController@getDataSource');
$app->get('get-keys-table-id', 'MainController@getKeysByTableId');
$app->post('get-all-data-by-table-id', 'MainController@getAllDataByTableId');
$app->post('save-chart-data', 'MainController@saveChartData');
$app->get('get-chart-data', 'MainController@getChartData');
$app->get('delete-chart', 'MainController@deleteChart');
$app->post('delete-multiple-chart', 'MainController@deleteMultipleChart');
$app->get('clone-row', 'MainController@cloneRow');
$app->get('edit-chart-by-id', 'MainController@editChartById');





