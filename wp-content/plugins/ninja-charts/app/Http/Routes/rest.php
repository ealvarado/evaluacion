<?php

/**
 * @var $app NinjaCharts\Framework\Foundation\Application
 */

//$app->rest->get('tickets', 'TicketController@get');
//
//$app->rest->group(function($app) {
//
//    $app->rest->get('/', 'UserController@users');
//
//    $app->rest->post('/', 'UserController@createUser');
//
//    $app->rest->get('/{id}', 'UserController@user')->int('id');
//
//    $app->rest->get('/{id}/{name}/{user_id}/{slug}', 'UserController@test')->where([
//        'id' => 'int',
//        'name' => 'alpha',
//        'user_id' => 'alpha_num',
//        'slug' => 'alpha_num_dash'
//    ]);
//})
//->prefix('users')
//->withPolicy('UserPolicy');


// Alternative of where clause
// Also supports: ->int('id', 'some_id') or ->int(['id', 'some_id'])
// $app->rest->get('users/{id}/{name}/{user_id}/{slug}', 'UserController@test')
//     ->int('id')
//     ->alpha('name')
//     ->alphaNum('user_id')
//     ->alphaNumDash('slug')
//     ->withPolicy('UserPolicy');


// $app->rest->get('posts', 'PostController@posts');

// $app->rest->get('posts/{id}', 'PostController@post')->int('id');
