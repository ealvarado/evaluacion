<?php

namespace NinjaCharts\Traits;

use NinjaCharts\Modules\ManualModule;
use NinjaCharts\Framework\Support\Arr;
use NinjaCharts\Modules\FluentFormModule;
use NinjaCharts\Modules\NinjaTableModule;

trait Helper
{
    public function randomColor()
    {
        $c = null;
        for ($i = 0; $i < 6; $i++) {
            $c .= dechex(rand(0, 15));
        }
        return "#$c";
    }

    public function chartBackgroundColor($chart_type, $rows)
    {
        if ($chart_type === 'line') {
            return 'transparent';
        } elseif ($chart_type === 'pie' || $chart_type === 'doughnut' || $chart_type === 'polarArea') {
            return $this->multipleBackgroundColor($rows);
        } else {
            return $this->randomColor();
        }
    }

    public function multipleBackgroundColor($rows)
    {
        $colors = [];
        foreach ($rows as $row) {
            $colors[] = $this->randomColor();
        }
        return $colors;
    }


    public function dynamicBackgroundColor($ninja_chart = null, $chart_type = null)
    {
        $bg_color = '';
        $color = [];
        if ($ninja_chart !== null) {
            $options = isset($ninja_chart->options) ? json_decode($ninja_chart->options) : '';
            if (isset($options->background_color)) {
                foreach ($options->background_color as $key => $value) {
                    $colors[] = $value;
                }
                $color['bg_color'] = $colors;
            }
            if ($chart_type == $ninja_chart->chart_type) {
                $bg_color = $color;
            }
        }
        return $chart_type === 'line' ? $bg_color = 'transparent' : $bg_color;
    }

    public function dynamicBorderColor($ninja_chart = null, $chart_type = null)
    {
        $extradata = Arr::get($_REQUEST, 'extra_data');
        $series = Arr::get($extradata, 'series');
        $border_color = '';
        $options = isset($ninja_chart->options) ? json_decode($ninja_chart->options) : '';
        if (isset($series)) {
            $border_color = $series;
        } else {
            if (isset($options->series)) {
                $border_color = $options->series;
            } else {
                $border_color = isset($options->border_color) ? $options->border_color : '';
            }
        }
        return $border_color;
    }

    public function classInstanceHelper($data_source = null)
    {
        $source = $data_source ? $data_source : $this->request->data_source;
        if ($source === 'ninja_table') {
            $this->class_instance = new NinjaTableModule();
        } else if ($source === 'fluent_form') {
            $this->class_instance = new FluentFormModule();
        } else if ($source === 'manual') {
            $this->class_instance = new ManualModule();
        }
        return $this->class_instance;
    }

    public function borderColor($ninja_chart, $chart_type, $i) 
    {
        $border_color = '';
        $bd_color = $this->dynamicBorderColor($ninja_chart, $chart_type);

        if (isset($bd_color[$i]) && gettype($bd_color[$i]) === 'object') {
            $color_label = (array)$bd_color[$i];
        } else {
            $color_label = isset($bd_color[$i]) ? $bd_color[$i] : '';
        }
        $border_color = isset($color_label['color']) ? $color_label['color'] : '';

        if ($border_color) {
            $border_color  = $border_color;
        }else {
            if (isset($bd_color['bd_color'][$i])) {
                $border_color = $bd_color['bd_color'][$i];
            }
        }
        return $border_color;
    }

    public function backgroundColor($ninja_chart, $chart_type, $i)
    {
        $background_color = '';
        
        $bg_color = $this->dynamicBackgroundColor($ninja_chart, $chart_type);
        if ($chart_type === 'bar' || $chart_type === 'area' || $chart_type === 'radar') {
            $background_color = $this->borderColor($ninja_chart, $chart_type, $i);
        } else {
            $background_color = isset($bg_color['bg_color'][$i]) ? $bg_color['bg_color'][$i] : '';
        }
        return $background_color;
    }

    public function label($ninja_chart, $chart_type, $i) 
    {
        $label = '';
        $bd_color = $this->dynamicBorderColor($ninja_chart, $chart_type);

        if (isset($bd_color[$i]) && gettype($bd_color[$i]) === 'object') {
            $color_label = (array)$bd_color[$i];
        } else {
            $color_label = isset($bd_color[$i]) ? $bd_color[$i] : '';
        }
        $label = isset($color_label['label']) ? $color_label['label'] : '';
        return $label;
    }
}
