<?php

namespace NinjaCharts\Models;

use NinjaCharts\Framework\Database\Orm\Model;

class FluentFormSubmission extends Model
{
    protected $table = 'fluentform_submissions';
}
