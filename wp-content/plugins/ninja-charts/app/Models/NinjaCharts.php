<?php

namespace NinjaCharts\Models;

use NinjaCharts\Framework\Support\Arr;
use NinjaCharts\Framework\Database\Orm\Model;

class NinjaCharts extends Model
{
    protected $table = 'ninja_charts';

    public static function saveOrUpdate($data, $extra_options = [])
    {
        $options = Arr::get($data, 'options');

        if ($extra_options) {
            foreach ($extra_options as $key => $value) {
                $background_color[] = Arr::get($value, 'backgroundColor');
                $border_color[] = Arr::get($value, 'borderColor');
            }
            $options['background_color'] = $background_color;
            $options['border_color'] = $border_color;
        }

        $id = Arr::get($data, 'id');
        $ninja_charts = $id ? NinjaCharts::findOrFail($id) : new NinjaCharts();
        $ninja_charts = self::allRow($ninja_charts, $data, $options, $clone = false);
        return $ninja_charts;
    }

    public static function getChartData($keyword = '', $perPage = 10)
    {
        $ninja_charts = NinjaCharts::query()
            ->orderBy('id', 'desc')
            ->where(function ($query) use ($keyword) {
                $query->where('chart_name', 'LIKE', "%{$keyword}%")
                    ->orWhere('render_engine', 'LIKE', "%{$keyword}%")
                    ->orWhere('chart_type', 'LIKE', "%{$keyword}%");
            })->paginate($perPage);

        return $ninja_charts;
    }

    public static function cloneRow($id = null)
    {
        $data = NinjaCharts::findOrFail($id);
        $ninja_charts = new NinjaCharts();
        $ninja_charts = self::allRow($ninja_charts, $data, $options = [], $clone = true);
        return $ninja_charts;
    }

    public static function allRow($ninja_charts, $data, $options, $clone)
    {
        $ninja_charts->table_id = Arr::get($data, 'data_source') === 'manual' ? 0 : Arr::get($data, 'table_id');
        $ninja_charts->chart_name = Arr::get($data, 'chart_name');
        $ninja_charts->render_engine = Arr::get($data, 'render_engine');
        $ninja_charts->chart_type = Arr::get($data, 'chart_type');
        $ninja_charts->data_source = Arr::get($data, 'data_source');
        $ninja_charts->final_keys = $clone ? Arr::get($data, 'final_keys') : json_encode(Arr::get($data, 'final_keys'));
        $ninja_charts->options = $clone ? Arr::get($data, 'options') : json_encode($options);
        $ninja_charts->manual_inputs = $clone ? Arr::get($data, 'manual_inputs') : json_encode(Arr::get($data, 'manualInputData'));
        $ninja_charts->save();

        return $ninja_charts;
    }

    public static function singleOrMultipleDelete($status, $ids)
    {
        if ($status) {
            self::deleteChart($ids);
        } else {
            foreach ($ids as $id) {
                $id = Arr::get($id, 'id');
                self::deleteChart($id);
            }
        }
    }

    public static function deleteChart($id)
    {
        NinjaCharts::findOrFail($id)->delete();
    }

}
