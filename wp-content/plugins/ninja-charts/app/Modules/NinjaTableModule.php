<?php

namespace NinjaCharts\Modules;

use NinjaCharts\Traits\Helper;
use NinjaCharts\Models\NinjaTable;
use NinjaCharts\Models\NinjaCharts;
use NinjaCharts\Models\NinjaTableItem;
use NinjaCharts\Models\NinjaTableMeta;
use NinjaCharts\Framework\Support\Arr;

class NinjaTableModule
{
    use Helper;

    public function getTableList()
    {
        $ninja_tables = NinjaTable::wherePostType('ninja-table')->select('ID', 'post_title')->get();

        $list = [];

        foreach ($ninja_tables as $table) {
            $list[] = [
                'id'   => $table->ID,
                'name' => $table->post_title
            ];
        }

        return $list;
    }

    public function getKeysByTable($table_id = null)
    {
        $type_keys = NinjaTableMeta::wherePostId($table_id)->where('meta_key', '_ninja_table_columns')->first();
        $values = unserialize($type_keys->meta_value);
        foreach ($values as $value) {
            $keys_types[] = [
                'key'       => Arr::get($value, 'key'),
                'data_type' => Arr::get($value, 'data_type')
            ];
        }
        return $keys_types;
    }

    public function getAllDataByTable($table_id = null, $keys = null, $chart_type = null, $extra_data = [], $id = null)
    {
        $tableRows = NinjaTableItem::whereTableId($table_id)->select('value')->get();
        $ninja_chart = $id ? NinjaCharts::findOrFail($id) : null;
        $labels = $this->labelFormat($keys, $tableRows);

        if ($chart_type === 'bubble' || $chart_type === 'scatter') {
            $data_sets = $this->bubbleOrScatterChart($labels, $tableRows, $chart_type, $keys, $ninja_chart, $extra_data = []);
        } else {
            $data_sets = $this->otherChart($labels, $tableRows, $chart_type, $keys, $ninja_chart, $extra_data = []);
        }

        $chart_data = [
            "labels"   => Arr::get($labels, 'labels'),
            "datasets" => $data_sets,
        ];

        return $chart_data;
    }


    public function labelFormat($keys, $tableRows)
    {
        $label_key = '';
        $i = 0;
        foreach ($keys as $key) {
            if (Arr::get($key, 'data_type') === 'text') {
                $label_key = Arr::get($key, 'key');
            } elseif ($label_key === '' && gettype(Arr::get($keys[$i], 'key')) === 'string') {
                $label_key = Arr::get($keys[$i], 'key');
            }
            $i++;
        }
        $l_key = Arr::get($keys[0], 'key');
        if ($label_key) {
            $label_key = $label_key;
        } else {
            $label_key = $l_key;
        }

        $label = $tableRows->map(function ($items) use ($label_key) {
            return isset($items->value) ? json_decode($items->value)->$label_key : '';
        });

        return [
            'label_key' => $label_key,
            'labels'    => $label
        ];
    }

    public function renderChart($data)
    {
        if ($data->render_engine === 'chart_js') {
            return $this->chartJsChart($data);
        }
    }

    public function chartJsChart($data)
    {
        $keys = json_decode($data->final_keys, true);
        $chart_data = $this->getAllDataByTable($data->table_id, $keys, $data->chart_type, $extra_data = [], $data->id);
        return $chart_data;
    }

    public function bubbleOrScatterChart($labels, $tableRows, $chart_type, $keys, $ninja_chart)
    {
        $data_sets = [];
        $data = [];

        $bg_color = $this->dynamicBackgroundColor($ninja_chart, $chart_type);
        $bd_color = $this->dynamicBorderColor($ninja_chart, $chart_type, $keys);
        $rows = $tableRows->map(function ($items) {
            return isset($items->value) ? json_decode($items->value) : '';
        });

        $keyss = [];
        foreach ($keys as $key) {
            if (Arr::get($key, 'data_type') == 'number') {
                $keyss[] = Arr::get($key, 'key');
            }
        }

        foreach ($rows as $value) {
            $x = $keyss[0] ?? null;
            $y = $keyss[1] ?? null;
            $r = $keyss[2] ?? null;
            $data[] = [
                'x' => $value->$x ?? null,
                'y' => $value->$y ?? null,
                'r' => $value->$r ?? null
            ];
        }

        $data_sets[] =
            [
                "label"                => ucwords($chart_type),
                "backgroundColor"      => isset($bg_color['bg_color'][0]) ? $bg_color['bg_color'][0] : $this->chartBackgroundColor($chart_type, $rows),
                "pointBackgroundColor" => 'white',
                "borderWidth"          => 1,
                "pointBorderColor"     => 'black',
                "pointHoverRadius"     => 4,
                "pointRadius"          => 3,
                "borderColor"          => isset($bd_color[0]) ? $bd_color[0] : $this->randomColor(),
//                  Data to be represented on y-axis
                "data"                 => $data
        ];

        return $data_sets;
    }

    public function otherChart($labels, $tableRows, $chart_type, $keys, $ninja_chart, $extra_data = [])
    {

        $data_sets = [];
        $rows = $tableRows->map(function ($items) {
            return json_decode($items->value);
        });
        $i = -1;
        foreach ($keys as $key) {
            $border_color = $this->borderColor($ninja_chart, $chart_type, $i);
            $label = $this->label($ninja_chart, $chart_type, $i);
            $background_color = $this->backgroundColor($ninja_chart, $chart_type, $i);
            $k = Arr::get($key, 'key');
            // $background_color[] = $this->chartBackgroundColor($chart_type, $rows);
            if ($k !== Arr::get($labels, 'label_key')) {
                $data_sets[] =
                    [
                        "label"                => $label ? $label : $k,
                        "backgroundColor"      => $background_color ? $background_color : $this->chartBackgroundColor($chart_type, $rows),
                        "pointBackgroundColor" => 'white',
                        "borderWidth"          => 1,
                        "pointBorderColor"     => 'black',
                        "pointHoverRadius"     => 4,
                        "pointRadius"          => 3,
                        "borderColor"          => $border_color ? $border_color : $this->randomColor(),
                        //Data to be represented on y-axis
                        "data"                 => $tableRows->map(function ($items) use ($k) {
                            if (isset($items->value) && isset(json_decode($items->value)->$k)) {
                                return json_decode($items->value)->$k;
                            }
                        })
                    ];
            }
            $i++;
        }
        return $data_sets;
    }
}
