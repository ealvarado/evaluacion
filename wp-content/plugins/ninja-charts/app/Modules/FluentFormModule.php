<?php

namespace NinjaCharts\Modules;

use NinjaCharts\Traits\Helper;
use NinjaCharts\Models\FluentForm;
use NinjaCharts\Models\NinjaCharts;
use NinjaCharts\Framework\Support\Arr;
use NinjaCharts\Models\FluentFormSubmission;

class FluentFormModule
{
    use Helper;

    public function getTableList()
    {
        $fluent_forms = FluentForm::select('id', 'title')->get();

        $list = [];

        foreach ($fluent_forms as $table) {
            $list[] = [
                'id'   => $table->id,
                'name' => $table->title
            ];
        }

        return $list;
    }

    public function getKeysByTable($table_id = null)
    {
        $type_keys = FluentForm::whereId($table_id)->select('form_fields')->first();

        $values = json_decode($type_keys->form_fields);

        foreach ($values->fields as $value) {

            if (isset($value->fields)) {
                foreach ($value->fields as $key => $value) {
                    $keys_types[] = [
                        'key'       => $value->attributes->name,
                        'data_type' => isset($value->attributes->type) ? $value->attributes->type : null
                    ];
                }
            } else {
                $keys_types[] = [
                    'key'       => $value->attributes->name,
                    'data_type' => isset($value->attributes->type) ? $value->attributes->type : null
                ];
            }
        }

        return $keys_types;
    }

    public function getAllDataByTable($table_id = null, $keys = null, $chart_type = null, $extra_data = [], $id = null)
    {
        $tableRows = FluentFormSubmission::whereFormId($table_id)->where('status', '!=', 'trashed')->select('response')->get();
        $ninja_chart = $id ? NinjaCharts::findOrFail($id) : null;
        // $labels = $this->labelFormat($keys, $tableRows);
        $labels = '';

        if ($chart_type === 'bubble' || $chart_type === 'scatter') {
            $data_sets = $this->bubbleOrScatterChart($labels, $tableRows, $chart_type, $keys, $ninja_chart);
        } else {
            $data_sets = $this->otherChart($labels, $tableRows, $chart_type, $keys, $ninja_chart);
        }

        $chart_data = [
            "labels"   => Arr::get($labels, 'labels'),
            "datasets" => $data_sets,
        ];

        return $chart_data;
    }


    public function labelFormat($keys, $tableRows)
    {

        $label_key = '';
        $i = 0;
        foreach ($keys as $key) {
            if (Arr::get($key, 'data_type') === 'text') {
                $label_key = Arr::get($key, 'key');
                break;
            } elseif ($label_key === '' && gettype(Arr::get($keys[$i], 'key')) === 'string') {
                $label_key = Arr::get($keys[$i], 'key');
                break;
            } else {
                $label_key = Arr::get($keys[0], 'key');
                break;
            }
            $i++;
        }
        $label = $tableRows->map(function ($items) use ($label_key) {
            return isset($items->response) ? json_decode($items->response)->$label_key : '';
        });

        return [
            'label_key' => $label_key,
            'labels'    => $label
        ];
    }

    public function renderChart($data)
    {
        if ($data->render_engine === 'chart_js') {
            return $this->chartJsChart($data);
        }
    }

    public function chartJsChart($data)
    {
        $keys = json_decode($data->final_keys, true);
        $chart_data = $this->getAllDataByTable($data->table_id, $keys, $data->chart_type, $extra_data = [], $data->id);
        return $chart_data;
    }

    public function bubbleOrScatterChart($labels, $tableRows, $chart_type, $keys, $ninja_chart)
    {
        $data_sets = [];
        $data = [];

        $rows = $tableRows->map(function ($items) {
            return isset($items->response) ? json_decode($items->response) : '';
        });
        $bg_color = $this->dynamicBackgroundColor($ninja_chart, $chart_type);
        $bd_color = $this->dynamicBorderColor($ninja_chart, $chart_type);
        $keyss = [];
        foreach ($keys as $key) {
            if (Arr::get($key, 'data_type') == 'number') {
                $keyss[] = Arr::get($key, 'key');
            }
        }

        foreach ($rows as $value) {
            $x = $keyss[0] ? $keyss[0] : null;
            $y = $keyss[1] ? $keyss[1] : null;
            $r = $keyss[2] ? $keyss[2] : null;
            $data[] = [
                'x' => $value->$x ? $value->$x : null,
                'y' => $value->$y ? $value->$y : null,
                'r' => $value->$r ? $value->$r : null
            ];
        }

        $data_sets[] =
            [
                "label"                => ucwords($chart_type),
                "backgroundColor"      => isset($bg_color['bg_color'][0]) ? $bg_color['bg_color'][0] : $this->chartBackgroundColor($chart_type, $rows),
                "pointBackgroundColor" => 'white',
                "borderWidth"          => 1,
                "pointBorderColor"     => 'black',
                "pointHoverRadius"     => 4,
                "pointRadius"          => 3,
                "borderColor"          => isset($bd_color[0]) ? $bd_color[0] : $this->randomColor(),
                // Data to be represented on y-axis
                "data"                 => $data
            ];

        return $data_sets;
    }

    public function otherChart($labels, $tableRows, $chart_type, $keys, $ninja_chart)
    {
        $data_sets = [];
        $rows = $tableRows->map(function ($items) {
            return json_decode($items->response);
        });
        $bg_color = $this->dynamicBackgroundColor($ninja_chart, $chart_type);
        $bd_color = $this->dynamicBorderColor($ninja_chart, $chart_type);

        $i = -1;
        foreach ($keys as $key) {
            $k = Arr::get($key, 'key');
            $background_color[] = $this->chartBackgroundColor($chart_type, $rows);
            if ($k !== Arr::get($labels, 'label_key')) {
                $data_sets[] =
                    [
                        "label"                => $k,
                        "backgroundColor"      => isset($bg_color['bg_color'][$i]) ? $bg_color['bg_color'][$i] : $this->chartBackgroundColor($chart_type, $rows),
                        "pointBackgroundColor" => 'white',
                        "borderWidth"          => 1,
                        "pointBorderColor"     => 'black',
                        "pointHoverRadius"     => 4,
                        "pointRadius"          => 3,
                        "borderColor"          => isset($bd_color[$i]) ? $bd_color[$i] : $this->randomColor(),
                        //Data to be represented on y-axis
                        "data"                 => $tableRows->map(function ($items) use ($k) {
                            if (isset($items->response)) {
                                if (isset(json_decode($items->response)->$k) && json_decode($items->response)->$k != null) {
                                    return json_decode($items->response)->$k;
                                } else {

                                    foreach (json_decode($items->response) as $key => $value) {
                                        if (gettype($value) === 'object') {
                                            return isset(json_decode($items->response)->$key->$k) ? json_decode($items->response)->$key->$k : '';
                                        }
                                    }
                                }
                            }
                        })
                    ];
            }
            $i++;
        }
        return $data_sets;
    }

}
