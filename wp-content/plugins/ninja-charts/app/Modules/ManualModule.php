<?php

namespace NinjaCharts\Modules;

use NinjaCharts\App;
use NinjaCharts\Traits\Helper;
use NinjaCharts\Models\NinjaCharts;
use NinjaCharts\Framework\Support\Arr;

class ManualModule
{
    protected $request;
    use Helper;

    public function getTableList()
    {
        return 0;
    }

    public function getKeysByTable($table_id = null)
    {
        return '';
    }

    public function getAllDataByTable($table_id = null, $keys = null, $chart_type = null, $extra_data = [], $id = null)
    {
        $data_sets = [];
        $labels = [];
        $data = [];
      
        $ninja_chart = $id ? NinjaCharts::findOrFail($id) : null;
        $manual_inputs = isset($extra_data['manual_inputs']) ? $extra_data['manual_inputs'] : $extra_data;

        $i = 0;
        $border_color = $this->borderColor($ninja_chart, $chart_type, $i);
        $label = $this->label($ninja_chart, $chart_type, $i);
        $background_color = $this->backgroundColor($ninja_chart, $chart_type, $i);
        
        if($manual_inputs === '') return '';
        if ($chart_type==='bubble' || $chart_type==='scatter') {
            foreach ($manual_inputs as $key => $value) {
                $data[] = [
                    'x' => Arr::get($value, 'x'),
                    'y' => Arr::get($value, 'y'),
                    'r' => Arr::get($value, 'r', '')
                ];
            }
        } else {
            foreach ($manual_inputs as $key => $value) {
                $data[] = Arr::get($value, 'number_input');
                $labels[] = Arr::get($value, 'text_input');
            }
        }

        $data_sets[] =
            [
                "label"                => $label ? $label : ucwords($chart_type),
                "backgroundColor"      => $background_color ? $background_color : $this->chartBackgroundColor($chart_type, $manual_inputs),
                "pointBackgroundColor" => 'white',
                "borderWidth"          => 1,
                "pointBorderColor"     => 'black',
                "pointHoverRadius"     => 4,
                "pointRadius"          => 3,
                "borderColor"          => $border_color ? $border_color : $this->randomColor(),
                //  Data to be represented on y-axis
                "data"                 => $data
            ];

        $chart_data = [
            "labels"   => $labels,
            "datasets" => $data_sets,
        ];

        return $chart_data;
    }

    public function renderChart($data)
    {
        if ($data->render_engine === 'chart_js') {
            return $this->chartJsChart($data);
        }
    }

    public function chartJsChart($data)
    {
        $chart_data = $this->getAllDataByTable($data->table_id, $keys='', $data->chart_type,json_decode($data->manual_inputs,true),$data->id);
        return $chart_data;
    }
}
