<?php

namespace NinjaCharts\Framework\Rest;

class Route
{
    protected $app = null;

    protected $prefix = null;

    protected $path = null;

    protected $handler = null;

    protected $method = null;

    protected $wheres = [];

    protected $policyHandler = null;

    protected $predefinedNamedRegx = [
        'int' => '[0-9]+',
        'alpha' => '[a-zA-Z]+',
        'alpha_num' => '[a-zA-Z0-9]+',
        'alpha_num_dash' => '[a-zA-Z0-9-_]+'
    ];


    public function __construct($app, $prefix, $path, $handler, $method)
    {
        $this->app = $app;
        $this->prefix = $prefix;
        $this->path = $path;
        $this->handler = $handler;
        $this->method = $method;
    }

    public function where($identifier, $value = null)
    {
        if (!is_null($value)) {
            $this->wheres[$identifier] = $this->get($value);
        } else {
            foreach ($identifier as $key => $value) {
                $this->wheres[$key] = $this->getValue($value);
            }
        }

        return $this;
    }

    public function int($identifiers)
    {
        $identifiers = is_array($identifiers) ? $identifiers : func_get_args();

        foreach ($identifiers as $identifier) {
            $this->wheres[$identifier] = '[0-9]+';
        }

        return $this;
    }

    public function alpha($identifiers)
    {
        $identifiers = is_array($identifiers) ? $identifiers : func_get_args();

        foreach ($identifiers as $identifier) {
            $this->wheres[$identifier] = '[a-zA-Z]+';
        }

        return $this;
    }

    public function alphaNum($identifiers)
    {
        $identifiers = is_array($identifiers) ? $identifiers : func_get_args();

        foreach ($identifiers as $identifier) {
            $this->wheres[$identifier] = '[a-zA-Z0-9]+';
        }

        return $this;
    }

    public function alphaNumDash($identifiers)
    {
        $identifiers = is_array($identifiers) ? $identifiers : func_get_args();

        foreach ($identifiers as $identifier) {
            $this->wheres[$identifier] = '[a-zA-Z0-9-_]+';
        }

        return $this;
    }

    public function withPolicy($handler)
    {
        $this->policyHandler = $handler;
    }

    public function register()
    {
        $path = $this->compilePath($this->path);

        $options = [
            'methods' => $this->method,
            'callback' => $this->app->parseRestHandler($this->handler)
        ];

        if ($this->policyHandler) {
            $options['permission_callback'] = $this->app->parsePolicyHandler(
                $this->getPolicyHandler($this->policyHandler)
            );
        }

        return register_rest_route($this->prefix, "/{$path}", $options);
    }

    protected function getValue($value)
    {
        if (array_key_exists($value, $this->predefinedNamedRegx)) {
            return $this->predefinedNamedRegx[$value];
        }

        return $value;
    }

    protected function getPolicyHandler($policyHandler)
    {
        if (strpos($policyHandler, '@') !== false) return $policyHandler;

        if (strpos($policyHandler, '::') !== false) return $policyHandler;
        
        if (!function_exists($policyHandler)) {
            if (is_string($this->handler) && strpos($this->handler, '@') !== false) {
                list($_, $method) = explode('@', $this->handler);
                $policyHandler = $policyHandler . '@' . $method;
            } else if (is_array($this->handler)) {
                $policyHandler = $policyHandler . '@' . $this->handler[1];
            }
        }

        return $policyHandler;
    }

    protected function compilePath($path)
    {
        return preg_replace_callback('/{(.*?)}/', function($match) use ($path) {
            if (isset($this->wheres[$match[1]])) {
                $reg = $this->wheres[$match[1]];
                return "(?P<" . $match[1] . ">" . $reg . ")";
            }

            // Allow anything when there is no type check
            return "(?P<" . $match[1] . ">[^\s]+)";
        }, $path);
    }
}
