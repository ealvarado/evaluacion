<?php

namespace NinjaCharts\Framework\Foundation;

abstract class Policy
{
    public static function __callStatic($method, $args)
    {
        return call_user_func_array([new static, $method], $args);
    }

    public function __call($method, $args)
    {
        if (!method_exists($this, $method)) {
            return true;
        }
    }
}
