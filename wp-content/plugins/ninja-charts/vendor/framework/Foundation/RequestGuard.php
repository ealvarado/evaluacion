<?php

namespace NinjaCharts\Framework\Foundation;

use NinjaCharts\Framework\Foundation\App;
use NinjaCharts\Framework\Validator\Validator;
use NinjaCharts\Framework\Validator\ValidationException;

abstract class RequestGuard
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }

    public function messages()
    {
        return [];
    }

    public function validate(Validator $validator = null)
    {
        try {
            $validator = $validator ?: App::getInstance('validator');

            $rules = (array) $this->rules();

            if (!$rules) return;

            $validator = $validator->make($this->all(), $rules, (array) $this->messages());

            if ($validator->validate()->fails()) {
                throw new ValidationException('Unprocessable Entity!', 422, null, $validator->errors());
            }
        } catch (ValidationException $e) {

            if (defined('REST_REQUEST') && REST_REQUEST) {
                throw $e;
            };

            App::getInstance()->doCustomAction('handle_exception', $e);
        }
    }

    public function __call($method, $params)
    {
        return call_user_func_array(
            [App::getInstance('request'), $method], $params
        );
    }
}
