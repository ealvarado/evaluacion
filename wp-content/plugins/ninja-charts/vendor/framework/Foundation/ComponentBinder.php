<?php

namespace NinjaCharts\Framework\Foundation;

use NinjaCharts\Framework\View\View;
use NinjaCharts\Framework\Rest\Rest;
use NinjaCharts\Framework\Request\Request;
use NinjaCharts\Framework\Response\Response;
use NinjaCharts\Framework\Database\Orm\Model;
use NinjaCharts\Framework\Validator\Validator;
use NinjaCharts\Framework\Foundation\Dispatcher;
use NinjaCharts\Framework\Foundation\RequestGuard;
use NinjaCharts\Framework\Database\ConnectionResolver;
use NinjaCharts\Framework\Database\Query\WPDBConnection;
use NinjaCharts\Framework\Foundation\UnAuthorizedException;

class ComponentBinder
{
    protected $app = null;

    public function __construct($app)
    {
        $this->app = $app;
    }

    public function bindComponents()
    {
        $this->bindRequest();
        $this->bindResponse();
        $this->bindValidator();
        $this->bindView();
        $this->bindEvents();
        $this->bindDataBase();
        $this->bindRest();
        $this->extendBindings();
        $this->registerRequestGuard();
    }

    protected function bindRequest()
    {
        $this->app->instance('request', new Request(
            $this->app, $_GET, $_POST, $_FILES
        ));
    }

    protected function bindResponse()
    {
        $this->app->bind('response', function($app) {
            return new Response($app);
        });
    }

    protected function bindValidator()
    {
        $this->app->bind('validator', function($app) {
            return new Validator;
        });
    }

    protected function bindView()
    {
        $this->app->bind('view', function($app) {
            return new View($app);
        });
    }

    protected function bindEvents()
    {
        $this->app->singleton('events', function($app) {
            return new Dispatcher($app);
        });
    }

    protected function bindDataBase()
    {
        $this->app->bindShared('db', function($app) {
            return new WPDBConnection(
                $GLOBALS['wpdb'], $app->config->get('database')
            );
        });

        Model::setEventDispatcher($this->app['events']);
        
        Model::setConnectionResolver(new ConnectionResolver);
    }

    protected function bindRest()
    {
        $this->app->singleton('rest', function($app) {
            return new Rest($app);
        });
    }

    protected function extendBindings()
    {
        $bindings = $this->app['path'] . 'boot/bindings.php';

        if (is_readable($bindings)) {
            require $bindings;
        }
    }

    protected function registerRequestGuard()
    {
        $this->app->resolving(RequestGuard::class, function($object) {
            try {
                if(!$this->app->call([$object, 'authorize'])) {
                    throw new UnAuthorizedException('Unauthorized!', 401);
                }
            } catch (UnAuthorizedException $e) {
                return $this->app->doCustomAction('handle_exception', $e);
            }

            return $this->app->call([$object, 'validate']);
        });
    }
}
