<?php

namespace NinjaCharts\Framework\Foundation;

class ExceptionHandler
{
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function register()
    {
        set_exception_handler([$this, 'handleException']);
    }

    public function handleException($e)
    {
        $this->app->doCustomAction('handle_exception', $e);
    }
}
