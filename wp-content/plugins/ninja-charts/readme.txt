=== Ninja Charts - Best WP Charts Plugin for WordPress ===

Contributors: wpmanageninja, adreastrian
Tags: charts, chart, graphs, graph, table, Data Tables, WP Charts, WordPress Chart Plugin, Grid, csv data, Tablular Data, spreadsheet data
Requires at least: 4.5
Requires PHP: 5.6 or greater
Tested up to: 5.4.2
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

Ninja Charts comes with amazing features that will help you to create unlimited online charts & graphs on your web pages.

Do you want to create different online charts on your website? Well, no, worries! Ninja Charts is a powerful chart generator that allows you to create and use numerous types of charts in WordPress. Just by following a few simple steps, you can generate amazing charts in different formats. Its JavaScript-based chart rendering engine makes the chart creation process easier than ever.

### WHY Ninja Charts?

Ninja Charts is the most lightweight WordPress chart plugin that simplifies the online chart creation process with ease of use. Currently, the plugin offers 9 different styles of charts that you can adjust with your required data in a visual representation. The charts offered include:

- Line charts
- Pie charts
- Bar charts
- Bubble charts
- Doughnut charts
- Radar charts
- Polar charts
- Scatter charts
- Area charts

One of the most prominent features of [Ninja Charts](https://wordpress.org/plugins/ninja-tables/) is its ability to retrieve data from popular [Ninja Tables](https://wordpress.org/plugins/ninja-tables/) and show them
 in the form of different charts. If you’re a Ninja Tables user and want to show your data in a data chart format, then this WordPress chart plugin is for you.

### HOW DOES IT WORK?

It is pretty simple to use, you just need to specify the titles and the chart type, then you will find the option to put a value manually or you can retrieve data from Ninja Tables plugin directly. After completing the above-mentioned process the plugin will automatically generate a short-code for a single chart, which you can embed on any page or post to show the respective data.

### Ninja Charts FEATURES

- Easy to use interface
- Simple and handy steps to get started
- Responsive, mobile-friendly WordPress chart plugin
- Chart generator
- Shortcode-friendly charts
- Live preview on admin
- Drag & drop friendly
- Multiple chart types available
- 9 types of charts
- Multiple data sources
- Versatile rendering engines
- Special formatting and preview option
- Amazing dedicated support facility

### INTEGRATED WITH NINJA TABLES

One of the cool aspects of the Ninja Charts is its default functionality with [Ninja Charts](https://wordpress.org/plugins/ninja-tables/). With this WordPress chart plugin, you can create charts retrieving the data from the Ninja Tables plugin. To create charts from Ninja Tables, take a look at the simple steps as follows-

**Step 1:** After installing the Ninja Charts, click on the Add New button
**Step 2:** Put your chart name in the title ➟ select chart render engine ➟ choose your preferred chart type from the dashboard ➟ and click on the **Next** button
**Step 3:** From the Data Source select Ninja Tables ➟ Click on **Next** button [In this step, you will also have an option for manual chart creation.]
**Step 4:** Now, choose the expected table that you want to create a chart from. [For manual chart creation, you’ll get an option to define it manually]
**Step 5:** Then, you’ll get the option to drag & drop the desired columns and click on the **Next** button.
**Step 6:** Next, here you will get the prepared chart with customization options in the **Formating & preview**. Once you’re done with the necessary customization, click on the **Next** button.
**Step 7:** Once you click on the **Next** button, you’re done with the process and you will see a short-code based chart has been created.

The steps are easy-going and less time-consuming.

### Unlimited Customization

Ninja Charts allows you to customize your data chart in the best possible way. While creating the chart, in the **Formating and preview** step, you will get a bunch of advanced options to customize your online chart
. Here, you can customize-
- Chart width
- Responsive chart width
- Chart height
- Background color
- Border width
- Border color
- Border radius
- Font size
- Font style
- Font color
Moreover, you’ll get more options like **AXES**, **TITLE**, **TOOLTIP**, **LEGEND** for making your online chart more personalized.

###AMAZING SUPPORT

We care about your needs and would like to serve your purposes in the best possible way. Our dedicated support ninjas and developers are always ready to cooperate with your queries.

If you have any queries, feel free to open a [ticket](https://wpmanageninja.com/support-tickets/). We would love to get back to you shortly.

== Screenshots ==

1. Fresh Ninja Charts admin panel
2. Chart creation interface
3. Ninja Charts support multiple data source e.g. Ninja Tables, Fluent Forms (upcoming)
4. Manual data source interface
5. Numerous Chart customization
6. Renderable shortcode
7. Admin dashboard to manage various charts
8. Rendered chart in the frontend

== Changelog ==

= 1.0 (Date: Jul 08, 2020)
* Init first version
