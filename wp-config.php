<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'marcolegal' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ']tj4%mM+lV#$IWl2j|#h(VJ;?q)qj=2hJ6,%L$qg;rr^YcDRt1VpxhYt;!rPq?$H' );
define( 'SECURE_AUTH_KEY',  ':DuFD,9*`l4ME/3G|}(Ao3{vI#28^+toO.m)YeqZ]H<S2}x}}m&gEVVV:NS=1Pgz' );
define( 'LOGGED_IN_KEY',    '0~U$-N8v|?J;f,g]H]|DgLw5`1B,~~IK?$,r% -p%{%[ne_A&zO1EttSH-e6aJg{' );
define( 'NONCE_KEY',        'xu#3uO j}-^S=cVjIR.Z>(; DR+`ml*(ZGQ>UFj{gJ{66v{3RTNHVC3?ZSpKJfu ' );
define( 'AUTH_SALT',        'OuK~qIG#d&%CNw$6dluvSnxXZ+/<)g`-O[&`TYp4uwWZX`;T/OD(x?^<Rpty9 kX' );
define( 'SECURE_AUTH_SALT', '#j.:q5awZq4d,<FkU:UPg.i_}%MI,EMpUG[6;UAMO-+ZrL-|IorYLBzECl:4hKLA' );
define( 'LOGGED_IN_SALT',   'TU^vH,4LbnBg52oHa:f/Ozf2Rm2=Y)rZa_U~TjE`2 .5lGPyIZ:Rk@Ir@!R-X?-[' );
define( 'NONCE_SALT',       '{!NSi/gKe$0NFe<nY.!=ypdfmJI58/$7]K{s!L4bj[cO%S_O*9|FuL+B~?PW#3.~' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'marcolegal';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
